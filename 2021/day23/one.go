package day23

import (
	"container/heap"
	"errors"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	amphs := make([]*amph, 0)
	for y := 2; y < 4; y++ {
		for _, x := range []int{3, 5, 7, 9} {
			amphs = append(amphs, &amph{
				Type: amphTypes[lines[y][x]],
				Pos:  tools.Pos2D{X: x - 1, Y: y - 1},
			})
		}
	}
	return solve(amphs, 2)
}

func solve(amphs []*amph, maxY int) (string, error) {
	// set done
	grid := createGrid(amphs, maxY)
	for _, x := range []int{amphA.HomeX, amphB.HomeX, amphC.HomeX, amphD.HomeX} {
		for y := maxY; y > 0; y-- {
			if grid[x][y] != x {
				break
			}
			for _, a := range amphs {
				if a.Pos.X == x && a.Pos.Y == y {
					a.Done = true
					break
				}
			}
		}
	}
	seen := make(map[int64]bool)
	queue := make(tools.PriorityQueue, 0)
	heap.Push(&queue, &tools.PriorityQueueItem{
		Value:    amphs,
		Priority: 0,
	})

mainLoop:
	for queue.Len() > 0 {
		entry := heap.Pop(&queue).(*tools.PriorityQueueItem)
		amphs := entry.Value.([]*amph)
		key := getSeenID(amphs)
		if _, ok := seen[key]; ok {
			continue
		}
		seen[key] = true
		grid := createGrid(amphs, maxY)

		posMoves := false
		for i, a := range amphs {
			if a.Done {
				continue
			}
			moves := a.getMoves(grid, maxY)
			for _, m := range moves {
				posMoves = true
				weight := entry.Priority + (a.Pos.Y+m.Y+tools.Abs(a.Pos.X-m.X))*a.Type.Enegery
				copied := make([]*amph, len(amphs))
				copy(copied, amphs)
				copied[i] = &amph{
					Type: a.Type,
					Pos:  m,
					Done: a.Type.HomeX == m.X,
				}
				heap.Push(&queue, &tools.PriorityQueueItem{
					Value:    copied,
					Priority: weight,
				})
			}
		}

		if !posMoves {
			// check if win
			for _, a := range amphs {
				if a.Pos.X != a.Type.HomeX {
					continue mainLoop
				}
			}
			return strconv.Itoa(entry.Priority), nil
		}

	}

	return "", errors.New("failed to find answer")
}

type amph struct {
	Type amphType
	Pos  tools.Pos2D
	Done bool
}

func (a *amph) inRoom() bool {
	return a.Pos.Y != 0
}

func (a *amph) canGoHome(grid [][]int, maxY int) *tools.Pos2D {
	var option *tools.Pos2D

	for y := maxY; y > 0; y-- {
		v := grid[a.Type.HomeX][y]
		if v == 0 {
			option = &tools.Pos2D{X: a.Type.HomeX, Y: y}
			break
		} else if v != a.Type.HomeX {
			return nil
		}
	}

	if option == nil {
		return nil
	}

	// can move out
	if a.Pos.Y != 0 {
		for y := a.Pos.Y - 1; y > 0; y-- {
			if grid[a.Pos.X][y] != 0 {
				return nil
			}
		}
	}

	// Can move to col
	if a.Pos.X > a.Type.HomeX {
		for x := a.Pos.X - 1; x > a.Type.HomeX; x-- {
			if grid[x][0] != 0 {
				return nil
			}
		}
	} else {
		for x := a.Pos.X + 1; x < a.Type.HomeX; x++ {
			if grid[x][0] != 0 {
				return nil
			}
		}
	}

	return option
}

func (a *amph) getMoves(grid [][]int, maxY int) []tools.Pos2D {

	options := make([]tools.Pos2D, 0)
	homeOpt := a.canGoHome(grid, maxY)
	if homeOpt != nil {
		options = append(options, *homeOpt)
	}

	// Ones not in rooms can not go anywhere but in the right room
	if !a.inRoom() {
		return options
	}

	// Else in room

	// If blocked in room, can't move
	if a.Pos.Y > 1 && grid[a.Pos.X][a.Pos.Y-1] != 0 {
		return options
	}

	// Check to see what stores it can move to
	for x := a.Pos.X - 1; x > -1; x-- {
		if grid[x][0] != 0 {
			break
		}
		if _, ok := storesX[x]; ok {
			options = append(options, tools.Pos2D{X: x, Y: 0})
		}
	}

	for x := a.Pos.X + 1; x < 11; x++ {
		if grid[x][0] != 0 {
			break
		}
		if _, ok := storesX[x]; ok {
			options = append(options, tools.Pos2D{X: x, Y: 0})
		}
	}

	return options
}

func getSeenID(amphs []*amph) int64 {
	val := int64(0)
	for _, a := range amphs {
		val <<= 4
		val |= int64(a.Pos.X)
	}
	return val
}
func createGrid(amphs []*amph, maxY int) [][]int {
	grid := make([][]int, 11)
	for x := 0; x < 11; x++ {
		grid[x] = make([]int, maxY+1)
	}
	for _, a := range amphs {
		grid[a.Pos.X][a.Pos.Y] = a.Type.HomeX
	}
	return grid
}

type amphType struct {
	Enegery int
	HomeX   int
}

var (
	amphA = amphType{
		Enegery: 1,
		HomeX:   2,
	}
	amphB = amphType{
		Enegery: 10,
		HomeX:   4,
	}
	amphC = amphType{
		Enegery: 100,
		HomeX:   6,
	}
	amphD = amphType{
		Enegery: 1000,
		HomeX:   8,
	}
	amphTypes = map[byte]amphType{'A': amphA, 'B': amphB, 'C': amphC, 'D': amphD}
	storesX   = map[int]bool{0: true, 1: true, 3: true, 5: true, 7: true, 9: true, 10: true}
)

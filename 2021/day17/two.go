package day17

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	targetMin, targetMax, volMin, volMax, err := load(lines)
	if err != nil {
		return "", err
	}

	var hits = true
	answer := 0
	for y := volMin.Y; y <= volMax.Y; y++ {
		for x := volMin.X; x <= volMax.X; x++ {
			hits, _ = hitsTarget(targetMin, targetMax, tools.Pos2D{X: x, Y: y})
			if hits {
				answer++
			}
		}
	}

	return strconv.Itoa(answer), nil // Return with the answer
}

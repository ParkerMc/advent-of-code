// Package day12 2018
package day12

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "3576"
const testAnswerTwo = "84271"

var testFilename = "../test/private/" + tools.GetPackageName()

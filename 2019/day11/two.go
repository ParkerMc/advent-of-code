package day11

import (
	"github.com/golang-collections/collections/set"
	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

const (
	black2 = 0
	white2 = 1
)

// func paint2(area map[int]map[int]int, pos tools.Pos2D, color int) {
// 	if _, ok := area[pos.X]; !ok {
// 		if color == white2 {
// 			return
// 		}
// 		area[pos.X] = make(map[int]int, 0)
// 	}
// 	if color == 1 {
// 		area[pos.X][pos.Y] = 0
// 	} else if color == 0 {
// 		area[pos.X][pos.Y] = 1
// 	}
// }

func minMax(area map[int]map[int]int) (int, int, int, int) {
	minX := 10000000000
	maxX := -10000000000
	minY := 10000000000
	maxY := -10000000000
	for x, line := range area {
		if x < minX {
			minX = x
		}
		if x > maxX {
			maxX = x
		}
		for y := range line {
			if y < minY {
				minY = y
			}
			if y > maxY {
				maxY = y
			}
		}
	}
	return minX, maxX, minY, maxY
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	program, err := tools.NewProgram(lines[0])
	if err != nil {
		return "", err
	}

	painted := set.New()
	area := make(map[int]map[int]int, 0)
	robot := robot{pos: tools.Pos2D{X: 0, Y: 0}, dir: up}

	first := true
	err = program.Run()
	if err != nil {
		return "", err
	}
	for !program.GetStopped() {
		if !first {
			color, err := program.RemoveOutput()
			if err != nil {
				return "", err
			}
			move, err := program.RemoveOutput()
			if err != nil {
				return "", err
			}
			paint(area, robot.pos, color)
			painted.Insert(tools.Pos2D{X: robot.pos.X, Y: robot.pos.Y})
			if (robot.dir == up && move == right) || (robot.dir == down && move == left) {
				robot.dir = right
			} else if (robot.dir == down && move == right) || (robot.dir == up && move == left) {
				robot.dir = left
			} else if (robot.dir == left && move == left) || (robot.dir == right && move == right) {
				robot.dir = down
			} else if (robot.dir == right && move == left) || (robot.dir == left && move == right) {
				robot.dir = up
			}
			switch robot.dir {
			case up:
				robot.pos.Y++
			case down:
				robot.pos.Y--
			case left:
				robot.pos.X--
			case right:
				robot.pos.X++
			}
		} else {
			first = false
		}
		color := 1
		if val, ok := area[robot.pos.X]; ok && val[robot.pos.Y] == 1 {
			color = 0
		}
		program.AddInput(color)
		err = program.Run()
		if err != nil {
			return "", err
		}
	}

	out := ""
	minX, maxX, minY, maxY := minMax(area)
	for y := maxY; y >= minY; y-- {
		out += "\n"
		for x := minX; x <= maxX; x++ {
			if _, ok := area[x]; ok && area[x][y] == white2 {
				out += "#"
				continue
			}
			out += " "
		}
	}

	return out, nil // Return with the answer
}

package day19

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	accepted, err := getScannersAndOffsets(lines)
	if err != nil {
		return "", err
	}

	largestDistance := 0
	for _, scanner := range accepted {
		for _, scanner2 := range accepted {
			dist := scanner.offset.MDist(&scanner2.offset)
			if dist > largestDistance {
				largestDistance = dist
			}
		}
	}

	return strconv.Itoa(largestDistance), nil // Return with the answer
}

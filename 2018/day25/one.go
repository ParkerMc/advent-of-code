package day25

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

type star struct {
	x       int
	y       int
	z       int
	a       int
	constid int
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func dist(star1 *star, star2 *star) int {
	return abs(star1.x-star2.x) + abs(star1.y-star2.y) + abs(star1.z-star2.z) + abs(star1.a-star2.a)
}

func setConst(stars []*star, sstar *star, constid int) {
	sstar.constid = constid
	for _, cstar := range stars {
		if cstar.constid == 0 && dist(sstar, cstar) <= 3 {
			setConst(stars, cstar, constid)
		}
	}
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	stars := make([]*star, 0)
	for _, line := range lines {
		pos := strings.Split(line, ",")
		nstar := &star{}
		nstar.x, err = strconv.Atoi(pos[0])
		if err != nil {
			return "", err
		}
		nstar.y, err = strconv.Atoi(pos[1])
		if err != nil {
			return "", err
		}
		nstar.z, err = strconv.Atoi(pos[2])
		if err != nil {
			return "", err
		}
		nstar.a, err = strconv.Atoi(pos[3])
		if err != nil {
			return "", err
		}
		stars = append(stars, nstar)
	}
	cconst := 1
	for _, cstar := range stars {
		if cstar.constid == 0 {
			setConst(stars, cstar, cconst)
			cconst++
		}
	}
	return strconv.Itoa(cconst - 1), nil // Return with the answer
}

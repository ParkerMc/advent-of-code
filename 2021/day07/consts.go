// Package day07 2018
package day07

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "352707"
const testAnswerTwo = "95519693"

var testFilename = "../test/private/" + tools.GetPackageName()

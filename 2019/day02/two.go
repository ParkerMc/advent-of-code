package day02

import (
	"errors"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	data, err := tools.CreateIntcodeData(lines[0])
	if err != nil {
		return "", err
	}

	for n := 0; n < 100; n++ {
		for v := 0; v < 100; v++ {

			program := tools.NewProgramFromData(data)
			program.SetData(1, n)
			program.SetData(2, v)

			err = program.Run()
			if err != nil {
				return "", err
			}

			if !program.GetStopped() {
				return "", errors.New("program not stopped")
			}
			ret := program.GetData(0)

			if ret == 19690720 {
				return strconv.Itoa(100*n + v), nil
			}
		}
	}

	return "", errors.New("this should never be reached") // Will never get here
}

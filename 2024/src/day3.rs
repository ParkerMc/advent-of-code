use crate::utils;
use regex::Regex;
use std::time::Duration;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    solve(input.into_iter().map(|l| l.as_str()))
}

fn part2(input: &Vec<String>) -> String {
    // HIGH 101681733
    solve(
        input
            .join("")
            .split("do()")
            .map(|s| s.split_once("don't()").map(|t| t.0).unwrap_or(s))
            .into_iter(),
    )
}

fn solve<'a>(input: impl Iterator<Item = &'a str>) -> String {
    let re = Regex::new(r"mul\((\d+),(\d+)\)").unwrap();
    input
        .into_iter()
        .map(|l| {
            re.captures_iter(l)
                .map(|c| {
                    c.get(1)
                        .unwrap()
                        .as_str()
                        .parse::<i64>()
                        .expect("Expected int")
                        * c.get(2)
                            .unwrap()
                            .as_str()
                            .parse::<i64>()
                            .expect("Expected int")
                })
                .sum::<i64>()
        })
        .sum::<i64>()
        .to_string()
}

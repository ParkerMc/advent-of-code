package day19

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

type scannerData struct {
	id      int
	results []tools.Pos3D
	offset  tools.Pos3D
}

func (s scannerData) findPermutations() []scannerData {
	out := make([]scannerData, 24)
	out[0] = s
	for i := 1; i < len(out); i++ {
		out[i] = scannerData{id: s.id, results: make([]tools.Pos3D, len(s.results))}
	}
	for i, result := range s.results {
		out[1].results[i] = tools.Pos3D{X: result.X, Y: result.Z, Z: -result.Y}
		out[2].results[i] = tools.Pos3D{X: result.X, Y: -result.Y, Z: -result.Z}
		out[3].results[i] = tools.Pos3D{X: result.X, Y: -result.Z, Z: result.Y}
		out[4].results[i] = tools.Pos3D{X: -result.X, Y: -result.Y, Z: result.Z}
		out[5].results[i] = tools.Pos3D{X: -result.X, Y: result.Z, Z: result.Y}
		out[6].results[i] = tools.Pos3D{X: -result.X, Y: result.Y, Z: -result.Z}
		out[7].results[i] = tools.Pos3D{X: -result.X, Y: -result.Z, Z: -result.Y}
		out[8].results[i] = tools.Pos3D{X: result.Y, Y: -result.X, Z: result.Z}
		out[9].results[i] = tools.Pos3D{X: result.Y, Y: result.Z, Z: result.X}
		out[10].results[i] = tools.Pos3D{X: result.Y, Y: result.X, Z: -result.Z}
		out[11].results[i] = tools.Pos3D{X: result.Y, Y: -result.Z, Z: -result.X}
		out[12].results[i] = tools.Pos3D{X: -result.Y, Y: result.X, Z: result.Z}
		out[13].results[i] = tools.Pos3D{X: -result.Y, Y: result.Z, Z: -result.X}
		out[14].results[i] = tools.Pos3D{X: -result.Y, Y: -result.X, Z: -result.Z}
		out[15].results[i] = tools.Pos3D{X: -result.Y, Y: -result.Z, Z: result.X}
		out[16].results[i] = tools.Pos3D{X: result.Z, Y: result.Y, Z: -result.X}
		out[17].results[i] = tools.Pos3D{X: result.Z, Y: -result.X, Z: -result.Y}
		out[18].results[i] = tools.Pos3D{X: result.Z, Y: -result.Y, Z: result.X}
		out[19].results[i] = tools.Pos3D{X: result.Z, Y: result.X, Z: result.Y}
		out[20].results[i] = tools.Pos3D{X: -result.Z, Y: -result.Y, Z: -result.X}
		out[21].results[i] = tools.Pos3D{X: -result.Z, Y: -result.X, Z: result.Y}
		out[22].results[i] = tools.Pos3D{X: -result.Z, Y: result.Y, Z: result.X}
		out[23].results[i] = tools.Pos3D{X: -result.Z, Y: result.X, Z: -result.Y}

	}
	return out
}

func (s scannerData) countMatches(data scannerData) (int, tools.Pos3D) {
	diff := make(map[tools.Pos3D]int)

	for _, pos1 := range s.results {
		for _, pos2 := range data.results {
			diff[pos1.Subtract(pos2)]++
		}
	}

	highestVal := 0
	highestKey := tools.Pos3D{}
	for key, val := range diff {
		if val > highestVal {
			highestVal = val
			highestKey = key
		}
	}

	return highestVal, highestKey
}

func (s scannerData) findMostMatched(dataList []scannerData) (int, tools.Pos3D, scannerData) {

	highestVal := 0
	highestData := scannerData{}
	highestOffset := tools.Pos3D{}
	for _, data := range dataList {
		val, offset := s.countMatches(data)
		if val > highestVal {
			highestVal = val
			highestOffset = offset
			highestData = data
		}
	}

	return highestVal, highestOffset, highestData
}

func applyToAll(offset tools.Pos3D, poses []tools.Pos3D) []tools.Pos3D {
	out := make([]tools.Pos3D, len(poses))
	for i, pos := range poses {
		out[i] = pos.Add(offset)
	}
	return out
}

func unique(slice []tools.Pos3D) []tools.Pos3D {
	keys := make(map[tools.Pos3D]bool)
	list := []tools.Pos3D{}
	for _, entry := range slice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func getScannersAndOffsets(lines []string) ([]scannerData, error) {
	var err error
	scanner := 0
	scanners := make([]scannerData, 0)
	for _, line := range lines {
		if line == "" {
			continue
		}
		if line[0:3] == "---" {
			scanner, err = strconv.Atoi(line[12 : len(line)-4])
			if err != nil {
				return nil, err
			}
			scanners = append(scanners, scannerData{id: scanner, results: make([]tools.Pos3D, 0)})
			continue
		}
		split := strings.Split(line, ",")
		x, err := strconv.Atoi(split[0])
		if err != nil {
			return nil, err
		}
		y, err := strconv.Atoi(split[1])
		if err != nil {
			return nil, err
		}
		z, err := strconv.Atoi(split[2])
		if err != nil {
			return nil, err
		}
		scanners[scanner].results = append(scanners[scanner].results, tools.Pos3D{X: x, Y: y, Z: z})
	}

	// Accept the first scanner and say it is true
	accepted := scanners[0:1]

	allScannerPermutations := make([][]scannerData, len(scanners)-1)
	for i, scanner := range scanners[1:] {
		allScannerPermutations[i] = scanner.findPermutations()
	}

	for i := 0; i < len(accepted); i++ {
		for j := 0; j < len(allScannerPermutations); j++ {
			matchCount, offset, data := accepted[i].findMostMatched(allScannerPermutations[j])
			if matchCount >= 12 {
				data.offset = offset.Add(accepted[i].offset)
				accepted = append(accepted, data)
				allScannerPermutations[j] = allScannerPermutations[len(allScannerPermutations)-1] // remove from array
				allScannerPermutations = allScannerPermutations[:len(allScannerPermutations)-1]
				j--
			}
		}
	}

	return accepted, nil
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	accepted, err := getScannersAndOffsets(lines)
	if err != nil {
		return "", err
	}

	posses := []tools.Pos3D{}
	for _, scanner := range accepted {
		posses = append(posses, applyToAll(scanner.offset, scanner.results)...)
	}

	posses = unique(posses)

	return strconv.Itoa(len(posses)), nil // Return with the answer
}

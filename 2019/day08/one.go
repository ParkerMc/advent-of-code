package day08

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	layers := make([][]int, len(lines[0])/150)
	for i := 0; i < len(lines[0])/150; i++ {
		layers[i] = make([]int, 150)
		for j := 0; j < 150; j++ {
			val, err := strconv.Atoi(string(lines[0][i*150+j]))
			if err != nil {
				return "", err
			}
			layers[i][j] = val
		}
	}

	least := 10000000
	leastI := -1
	for i, val := range layers {
		zeros := 0
		for _, num := range val {
			if num == 0 {
				zeros++
			}
		}
		if zeros < least {
			least = zeros
			leastI = i
		}
	}

	ones := 0
	twos := 0
	for _, num := range layers[leastI] {
		if num == 1 {
			ones++
		}
		if num == 2 {
			twos++
		}
	}

	return strconv.Itoa(ones * twos), nil // Return with the answer
}

package day19

import (
	"errors"
	"math"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

var ruleReg = regexp.MustCompile(`^(\d+): (?:\"(\w+)\"|([ \d\|]+))$`)

type ruleT struct {
	isStr bool
	str   string
	nums  [][]int
	regex string
	depth int
	done  bool
}

func (r ruleT) getPart(rules map[int]ruleT, depth, maxDepth int) (string, int, bool) { // minlen
	if r.done {
		return r.regex, r.depth, true
	}
	if depth > maxDepth {
		return "", 0, false
	}
	r.done = true
	if r.isStr {
		r.regex = r.str
		r.depth = len(r.str)
		r.done = true
	} else {
		if len(r.nums) > 1 {
			r.regex = "(?:"
		}
		minAdd := math.MaxInt64
		for i, list := range r.nums {
			if i != 0 {
				r.regex += "|"
			}
			partDepth := 0
			for j, list2 := range list {
				add, pDepth, done := rules[list2].getPart(rules, depth+j, maxDepth)
				partDepth += pDepth
				r.regex += add
				if !done {
					r.done = false
				}
			}
			if partDepth < minAdd {
				minAdd = partDepth
			}
		}
		if len(r.nums) > 1 {
			r.regex += ")"
		}
		r.depth = minAdd
		r.done = true
	}
	return r.regex, r.depth, r.done
}

func load(lines []string) (map[int]ruleT, []string, error) {
	rules := make(map[int]ruleT, 0)
	for i, line := range lines {
		if line == "" {
			return rules, lines[i:], nil
		}
		ruleNum, err := strconv.Atoi(ruleReg.FindStringSubmatch(line)[1])
		if err != nil {
			return nil, nil, err
		}
		if val := ruleReg.FindStringSubmatch(line)[2]; val != "" {
			rules[ruleNum] = ruleT{isStr: true, str: val}
		} else {
			split := strings.Split(ruleReg.FindStringSubmatch(line)[3], " | ")
			intList := make([][]int, len(split))
			for i, part := range split {
				split2 := strings.Split(part, " ")
				intList[i] = make([]int, len(split2))
				for j, part2 := range split2 {
					intList[i][j], err = strconv.Atoi(part2)
					if err != nil {
						return nil, nil, err
					}
				}
			}
			rules[ruleNum] = ruleT{isStr: false, nums: intList}
		}
	}
	return nil, nil, errors.New("invalid input")
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	rules, strs, err := load(lines)
	if err != nil {
		return "", err
	}

	regstr, _, _ := rules[0].getPart(rules, 0, math.MaxInt64)
	regex, err := regexp.Compile("^" + regstr + "$")
	if err != nil {
		return "", err
	}

	matched := 0
	for _, str := range strs {
		if regex.MatchString(str) {
			matched++
		}
	}
	return strconv.Itoa(matched), nil // Return with the answer
}

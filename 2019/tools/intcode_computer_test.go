package tools

import "testing"

func TestIntcodeProgram(t *testing.T) {
	pro1, err := NewProgram("88,0,4,0,99") // 88 will be 1 in real run
	if err != nil {
		t.Fatal(err)
	}
	pro1.SetData(0, 1)
	err = pro1.Run()
	if err != nil {
		t.Fatal(err)
	}
	if (!pro1.GetStopped()) || pro1.GetData(0) != 100 {
		t.Fatal("Error while running program 1 testing addition and SetData")
	}

	pro2 := NewProgramFromData([]int{2, 3, 0, 3, 99})
	if err != nil {
		t.Fatal(err)
	}
	err = pro2.Run()
	if err != nil {
		t.Fatal(err)
	}
	if (!pro2.GetStopped()) || pro2.GetData(3) != 6 {
		t.Fatal("Error while running program 2 testing multiplication")
	}

	pro3 := NewProgramFromData([]int{1102, 3, 2, 3, 99})
	if err != nil {
		t.Fatal(err)
	}
	err = pro3.Run()
	if err != nil {
		t.Fatal(err)
	}
	if (!pro3.GetStopped()) || pro3.GetData(3) != 6 {
		t.Fatal("Error while running program 3 testing immediate mode")
	}

	pro4 := NewProgramFromData([]int{3, 0, 99})
	if err != nil {
		t.Fatal(err)
	}
	err = pro4.Run()
	if err != nil {
		t.Fatal(err)
	}
	if pro4.GetStopped() {
		t.Fatal("Error while running program 3 testing input")
	}
	pro4.AddInput(15)
	err = pro4.Run()
	if err != nil {
		t.Fatal(err)
	}
	if (!pro4.GetStopped()) || pro4.GetData(0) != 15 {
		t.Fatal("Error while running program 4 testing input")
	}

	pro5 := NewProgramFromData([]int{4, 2, 99})
	if err != nil {
		t.Fatal(err)
	}
	err = pro5.Run()
	if err != nil {
		t.Fatal(err)
	}
	if val, err := pro5.RemoveOutput(); (!pro5.GetStopped()) || val != 99 || err != nil {
		t.Fatal("Error while running program 5 testing output")
	}

	pro6 := NewProgramFromData([]int{1105, 0, 6, 1105, 1, 7, 99, 104, 6, 99})
	if err != nil {
		t.Fatal(err)
	}
	err = pro6.Run()
	if err != nil {
		t.Fatal(err)
	}
	if val, err := pro6.RemoveOutput(); (!pro6.GetStopped()) || val != 6 || err != nil {
		t.Fatal("Error while running program 6 testing jump-if-true")
	}

	pro7 := NewProgramFromData([]int{1106, 1, 6, 1106, 0, 7, 99, 104, 7, 99})
	if err != nil {
		t.Fatal(err)
	}
	err = pro7.Run()
	if err != nil {
		t.Fatal(err)
	}
	if val, err := pro7.RemoveOutput(); (!pro7.GetStopped()) || val != 7 || err != nil {
		t.Fatal("Error while running program 7 testing jump-if-false")
	}

	pro8 := NewProgramFromData([]int{1107, 2, 1, 0, 1107, 1, 2, 1, 99})
	if err != nil {
		t.Fatal(err)
	}
	err = pro8.Run()
	if err != nil {
		t.Fatal(err)
	}
	if (!pro8.GetStopped()) || pro8.GetData(0) != 0 || pro8.GetData(1) != 1 {
		t.Fatal("Error while running program 8 testing less than")
	}

	pro9 := NewProgramFromData([]int{1108, 2, 1, 0, 1108, 1, 1, 1, 99})
	if err != nil {
		t.Fatal(err)
	}
	err = pro9.Run()
	if err != nil {
		t.Fatal(err)
	}
	if (!pro9.GetStopped()) || pro9.GetData(0) != 0 || pro9.GetData(1) != 1 {
		t.Fatal("Error while running program 9 testing equals")
	}

	pro10 := NewProgramFromData([]int{109, 7, 2108, 7, -4, 0, 99})
	if err != nil {
		t.Fatal(err)
	}
	err = pro10.Run()
	if err != nil {
		t.Fatal(err)
	}
	if (!pro10.GetStopped()) || pro10.GetData(0) != 1 {
		t.Fatal("Error while running program 10 testing relative mode")
	}

}

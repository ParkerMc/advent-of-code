package day05

import (
	"strconv"
	"strings"
	"unicode"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// Two the first puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	data := react(lines[0])
	types := make(map[rune]int)
	for _, letter := range "qwertyuiopasdfghjklzxcvbnm" {
		types[unicode.ToLower(letter)] = len(react(strings.Replace(strings.Replace(string(data), string(unicode.ToLower(letter)), "", -1), string(unicode.ToUpper(letter)), "", -1)))
	}

	smallest := 10000000000
	for _, amt := range types {
		if amt < smallest {
			smallest = amt
		}
	}
	return strconv.Itoa(smallest), nil // Return with the answer
}

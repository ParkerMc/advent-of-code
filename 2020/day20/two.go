package day20

// NOTE: Some things are generated but not used, as the point of this was just to show the puzzle can be solved and it's running fast enough I have not bothered to remove those parts.
//       In a real world senerio this code would be optimized.

import (
	"errors"
	"fmt"
	"math"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

func arrangeGrid(possibleTilesPerTile map[int][][][]tileRotT, tiles map[int]tileGroupT, tileNumbers []int) (tileT, error) {
	corners := make([]int, 0)
	edges := make([]int, 0)
	middle := make([]int, 0)

	for tileNum, rots := range possibleTilesPerTile {
		count := 0
		for _, posCombos := range rots[0] {
			if len(posCombos) > 0 {
				count++
			}
		}
		switch count {
		case 2:
			corners = append(corners, tileNum)
		case 3:
			edges = append(edges, tileNum)
		case 4:
			middle = append(middle, tileNum)
		default:
			return nil, fmt.Errorf("count isn't valid: %d", count)
		}
	}

	bigGridSize := int(math.Round(math.Sqrt(float64(len(tileNumbers)))))

	if len(corners) != 4 {
		return nil, errors.New("wrong number of corners")
	}
	if len(edges) != bigGridSize*4-8 {
		return nil, errors.New("wrong number of edges")
	}
	if len(middle) != bigGridSize*bigGridSize-(bigGridSize*4-4) {
		return nil, errors.New("too many middle parts")
	}

	gridTileMap := make([][]tileRotT, bigGridSize)
	for i := range gridTileMap {
		gridTileMap[i] = make([]tileRotT, bigGridSize)
	}

	// possibleTilesPerTile: [tile num][tile rot][dir] = []possible combos
	// Get the first corner and set it to the first
	firstCornerNum := 1453 // TODO remove hardcoded fix
	//firstCornerNum := corners[0]
	//firstCornerNum := 2417
	//corners = corners[1:]
	firstRot := -1
	for posRot, postiles := range possibleTilesPerTile[firstCornerNum] {
		if len(postiles[dirRight]) > 0 && len(postiles[dirDown]) > 0 {
			firstRot = posRot
			break
		}
	}
	if firstRot == -1 {
		return nil, errors.New("couldn't find rotation for first corner")
	}
	firstCorner := tileRotT{tile: firstCornerNum, rot: firstRot}
	lastTile := firstCorner
	gridTileMap[0][0] = lastTile
	for i := 1; i < bigGridSize; i++ { // Fill the rest of the top
		list := possibleTilesPerTile[lastTile.tile][lastTile.rot][dirRight]
		if len(list) > 1 {
			return nil, errors.New("more than one valid pice for this case")
		}
		lastTile = list[0]
		gridTileMap[i][0] = lastTile
	}
	for i := 1; i < bigGridSize; i++ { // Fill the rest of the right side
		list := possibleTilesPerTile[lastTile.tile][lastTile.rot][dirDown]
		if len(list) > 1 {
			return nil, errors.New("more than one valid pice for this case")
		}
		lastTile = list[0]

		gridTileMap[bigGridSize-1][i] = lastTile
	}
	for i := bigGridSize - 2; i > -1; i-- { // Fill the rest of the bottom
		list := possibleTilesPerTile[lastTile.tile][lastTile.rot][dirLeft]
		if len(list) > 1 {
			return nil, errors.New("more than one valid pice for this case")
		}
		lastTile = list[0]

		gridTileMap[i][bigGridSize-1] = lastTile
	}
	for i := bigGridSize - 2; i > 0; i-- { // Fill the rest of the left
		list := possibleTilesPerTile[lastTile.tile][lastTile.rot][dirUp]
		if len(list) > 1 {
			return nil, errors.New("more than one valid pice for this case")
		}
		lastTile = list[0]

		gridTileMap[0][i] = lastTile
	}

	// Double check the first cornor
	list := possibleTilesPerTile[lastTile.tile][lastTile.rot][dirUp]
	if len(list) > 1 {
		return nil, errors.New("more than one valid pice for this case")
	}
	lastTile = list[0]
	if lastTile.tile != firstCorner.tile || lastTile.rot != firstCorner.rot {
		return nil, errors.New("first and last corners are different")
	}

	// Fill middle
	for x := 1; x < bigGridSize-1; x++ {
		for y := 1; y < bigGridSize-1; y++ {
			posTilePerTile := make([][]tileRotT, 4)
			if tile := gridTileMap[x][y-1]; tile.tile != 0 {
				list := possibleTilesPerTile[tile.tile][tile.rot][dirDown]
				posTilePerTile[0] = make([]tileRotT, len(list))
				copy(posTilePerTile[0], list)
			}
			if tile := gridTileMap[x][y+1]; tile.tile != 0 {
				list := possibleTilesPerTile[tile.tile][tile.rot][dirUp]
				posTilePerTile[1] = make([]tileRotT, len(list))
				copy(posTilePerTile[1], list)
			}
			if tile := gridTileMap[x-1][y]; tile.tile != 0 {
				list := possibleTilesPerTile[tile.tile][tile.rot][dirRight]
				posTilePerTile[2] = make([]tileRotT, len(list))
				copy(posTilePerTile[2], list)
			}
			if tile := gridTileMap[x+1][y]; tile.tile != 0 {
				list := possibleTilesPerTile[tile.tile][tile.rot][dirLeft]
				posTilePerTile[3] = make([]tileRotT, len(list))
				copy(posTilePerTile[3], list)
			}

			set := false
			posTiles := make([]tileRotT, 0)
			for _, posTileForTile := range posTilePerTile {
				if len(posTileForTile) > 0 {
					if !set {
						posTiles = posTileForTile
						set = true
					} else {
						toRemove := make([]int, 0)
						for i, tile := range posTiles {
							contained := false
							for _, tile2 := range posTileForTile {
								if tile2.tile == tile.tile && tile2.rot == tile.rot {
									contained = true
								}
							}
							if !contained {
								toRemove = append(toRemove, i)
							}
						}
						for i, remove := range toRemove {
							posTiles = append(posTiles[:remove-i], posTiles[remove-i+1:]...)
						}
					}
				}
			}
			if len(posTiles) != 1 {
				return nil, fmt.Errorf("had %d possible tiles indead of 1", len(posTiles))
			}
			gridTileMap[x][y] = posTiles[0]
		}
	}

	realTileSize := tileSize - 2
	realGridSize := realTileSize * bigGridSize
	grid := make(tileT, realGridSize)
	for i := range grid {
		grid[i] = make([]bool, realGridSize)
	}
	for xTile, col := range gridTileMap {
		for yTile, tile := range col {
			if tile.tile == 0 {
				continue
			}
			for x := 0; x < realTileSize; x++ {
				for y := 0; y < realTileSize; y++ {
					grid[realTileSize*xTile+x][realTileSize*yTile+y] = tiles[tile.tile][tile.rot][x+1][y+1]
				}
			}
		}
	}

	return grid, nil
}

func (t tileT) replaceSeaMonsters() int {
	gridSize := len(t)
	xMaxSearch := gridSize - 19
	yMaxSearch := gridSize - 2
	count := 0
	for x := 0; x < xMaxSearch; x++ {
		for y := 0; y < yMaxSearch; y++ {
			if t[x+0][y+1] &&
				t[x+1][y+2] &&
				t[x+4][y+2] &&
				t[x+5][y+1] &&
				t[x+6][y+1] &&
				t[x+7][y+2] &&
				t[x+10][y+2] &&
				t[x+11][y+1] &&
				t[x+12][y+1] &&
				t[x+13][y+2] &&
				t[x+16][y+2] &&
				t[x+17][y+1] &&
				t[x+18][y+1] &&
				t[x+18][y+0] &&
				t[x+19][y+1] {
				t[x+0][y+1] = false
				t[x+1][y+2] = false
				t[x+4][y+2] = false
				t[x+5][y+1] = false
				t[x+6][y+1] = false
				t[x+7][y+2] = false
				t[x+10][y+2] = false
				t[x+11][y+1] = false
				t[x+12][y+1] = false
				t[x+13][y+2] = false
				t[x+16][y+2] = false
				t[x+17][y+1] = false
				t[x+18][y+1] = false
				t[x+18][y+0] = false
				t[x+19][y+1] = false
				count++
			}
		}
	}
	return count
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {
	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	tiles, tileNumbers, err := loadTiles(lines)
	if err != nil {
		return "", err
	}

	possibleTilesPerTile := getPossibleTiles(tiles, tileNumbers)

	grid, err := arrangeGrid(possibleTilesPerTile, tiles, tileNumbers)
	if err != nil {
		return "", err
	}

	for time := 0; grid.replaceSeaMonsters() == 0; time++ {
		if time == 4 {
			grid = grid.rotate90()
		} else if time > -1 && time < 8 {
			grid = grid.flip()
		} else {
			return "", errors.New("couldn't find any sea monsters")
		}
	}

	out := 0
	for _, col := range grid {
		for _, val := range col {
			if val {
				out++
			}
		}
	}
	return strconv.Itoa(out), nil // Return with the answer
}

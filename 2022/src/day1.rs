use std::collections::BinaryHeap;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) {
    utils::day::run_day(part, module_path!(), part1, part2, input);
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    let max_c: i64 = process_input(input).max().expect("failed to find max elf");
    max_c.to_string()
}

fn part2(input: &Vec<String>) -> String {
    let sorted_elves: BinaryHeap<_> = process_input(input).collect();
    sorted_elves.iter().take(3).sum::<i64>().to_string()
}

fn process_input(input: &Vec<String>) -> impl Iterator<Item = i64> + '_ {
    input.split(|l| l.is_empty()).map(|l| {
        l.iter()
            .map(|l| l.parse::<i64>().expect("Input contains non numbers"))
            .sum()
    })
}

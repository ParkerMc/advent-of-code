package day01

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	windows := make([]int, len(lines)-2)
	for i := range lines {
		if i+3 > len(lines) {
			break
		}
		windowVal := 0
		for j := 0; j < 3; j++ {
			intVal, err := strconv.Atoi(lines[i+j])
			if err != nil {
				return "", err
			}
			windowVal += intVal
		}
		windows[i] = windowVal
	}

	answer := 0
	last := 999999999999
	for _, val := range windows {
		if val > last {
			answer++
		}
		last = val
	}

	return strconv.Itoa(answer), nil // Return with the answer
}

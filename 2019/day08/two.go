package day08

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	layers := make([][]int, len(lines[0])/150)
	for i := 0; i < len(lines[0])/150; i++ {
		layers[i] = make([]int, 150)
		for j := 0; j < 150; j++ {
			val, err := strconv.Atoi(string(lines[0][i*150+j]))
			if err != nil {
				return "", err
			}
			layers[i][j] = val
		}
	}

	img := "\n"
	for y := 0; y < 6; y++ {
		for x := 0; x < 25; x++ {
			i := x + y*25
			for _, layer := range layers {
				if layer[i] == 0 {
					img += " "
					break
				} else if layer[i] == 1 {
					img += "#"
					break
				}
			}
		}
		img += "\n"
	}

	return img, nil // Will never get here
}

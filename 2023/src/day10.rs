use std::{collections::VecDeque, time::Duration};

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn find_loop(map: &Vec<Vec<char>>) -> (i64, usize, usize, Vec<Vec<i64>>) {
    let x_len = map[0].len();
    let start = map
        .iter()
        .enumerate()
        .find_map(|(y, l)| {
            if let Some(x) = l.iter().find_position(|c| **c == 'S') {
                return Some((y, x.0, 0));
            }
            None
        })
        .expect("Failed to find start");

    let mut dist_map = (0..map.len()).map(|_| vec![-1; map[0].len()]).collect_vec();
    let mut queue = VecDeque::from([start]);
    while let Some((y, x, depth)) = queue.pop_front() {
        if dist_map[y][x] > -1 {
            // Detect loop
            if dist_map[y][x] == depth {
                return (depth, y, x, dist_map);
            }
            continue;
        }
        dist_map[y][x] = depth;

        match map[y][x] {
            '|' => {
                if y > 0 && (map[y - 1][x] == '|' || map[y - 1][x] == '7' || map[y - 1][x] == 'F') {
                    queue.push_back((y - 1, x, depth + 1))
                }
                if y < map.len() - 1
                    && (map[y + 1][x] == '|' || map[y + 1][x] == 'L' || map[y + 1][x] == 'J')
                {
                    queue.push_back((y + 1, x, depth + 1))
                }
            }
            '-' => {
                if x > 0 && (map[y][x - 1] == '-' || map[y][x - 1] == 'L' || map[y][x - 1] == 'F') {
                    queue.push_back((y, x - 1, depth + 1))
                }
                if x < x_len - 1
                    && (map[y][x + 1] == '-' || map[y][x + 1] == '7' || map[y][x + 1] == 'J')
                {
                    queue.push_back((y, x + 1, depth + 1));
                }
            }
            'L' => {
                if y > 0 && (map[y - 1][x] == '|' || map[y - 1][x] == '7' || map[y - 1][x] == 'F') {
                    queue.push_back((y - 1, x, depth + 1))
                }
                if x < x_len - 1
                    && (map[y][x + 1] == '-' || map[y][x + 1] == '7' || map[y][x + 1] == 'J')
                {
                    queue.push_back((y, x + 1, depth + 1));
                }
            }
            'J' => {
                if y > 0 && (map[y - 1][x] == '|' || map[y - 1][x] == '7' || map[y - 1][x] == 'F') {
                    queue.push_back((y - 1, x, depth + 1))
                }
                if x > 0 && (map[y][x - 1] == '-' || map[y][x - 1] == 'L' || map[y][x - 1] == 'F') {
                    queue.push_back((y, x - 1, depth + 1))
                }
            }
            '7' => {
                if y < map.len() - 1
                    && (map[y + 1][x] == '|' || map[y + 1][x] == 'L' || map[y + 1][x] == 'J')
                {
                    queue.push_back((y + 1, x, depth + 1))
                }
                if x > 0 && (map[y][x - 1] == '-' || map[y][x - 1] == 'L' || map[y][x - 1] == 'F') {
                    queue.push_back((y, x - 1, depth + 1))
                }
            }
            'F' => {
                if y < map.len() - 1
                    && (map[y + 1][x] == '|' || map[y + 1][x] == 'L' || map[y + 1][x] == 'J')
                {
                    queue.push_back((y + 1, x, depth + 1))
                }
                if x < x_len - 1
                    && (map[y][x + 1] == '-' || map[y][x + 1] == '7' || map[y][x + 1] == 'J')
                {
                    queue.push_back((y, x + 1, depth + 1));
                }
            }
            'S' => {
                if y > 0 && (map[y - 1][x] == '|' || map[y - 1][x] == '7' || map[y - 1][x] == 'F') {
                    queue.push_back((y - 1, x, depth + 1))
                }
                if y < map.len() - 1
                    && (map[y + 1][x] == '|' || map[y + 1][x] == 'L' || map[y + 1][x] == 'J')
                {
                    queue.push_back((y + 1, x, depth + 1))
                }
                if x > 0 && (map[y][x - 1] == '-' || map[y][x - 1] == 'L' || map[y][x - 1] == 'F') {
                    queue.push_back((y, x - 1, depth + 1))
                }
                if x < x_len - 1
                    && (map[y][x + 1] == '-' || map[y][x + 1] == '7' || map[y][x + 1] == 'J')
                {
                    queue.push_back((y, x + 1, depth + 1));
                }
            }
            '.' => continue,
            _ => panic!("Unknown map char {}", map[y][x]),
        }
    }
    panic!("Ran out of pipes")
}

fn part1(input: &Vec<String>) -> String {
    let map = input.iter().map(|l| l.chars().collect_vec()).collect_vec();
    let (high, _, _, _) = find_loop(&map);
    high.to_string()
}

fn part2(input: &Vec<String>) -> String {
    let map = input.iter().map(|l| l.chars().collect_vec()).collect_vec();
    let (_, y, x, dist_map) = find_loop(&map);

    // Go in reverse to find exact loop
    let start = map
        .iter()
        .enumerate()
        .find_map(|(y, l)| {
            if let Some(x) = l.iter().find_position(|c| **c == 'S') {
                return Some((y, x.0));
            }
            None
        })
        .expect("Failed to find start");
    let mut loop_map = (0..map.len())
        .map(|_| vec![LoopEntry::Unknown; map[0].len()])
        .collect_vec();
    let mut queue = VecDeque::from([(y, x)]);
    let x_len = map[0].len();

    loop_map[start.0][start.1] = LoopEntry::Loop;
    while let Some((y, x)) = queue.pop_front() {
        if loop_map[y][x] != LoopEntry::Unknown {
            continue;
        }
        let depth = dist_map[y][x];
        loop_map[y][x] = LoopEntry::Loop;

        match map[y][x] {
            '|' => {
                if y > 0
                    && dist_map[y - 1][x] == depth - 1
                    && (map[y - 1][x] == '|' || map[y - 1][x] == '7' || map[y - 1][x] == 'F')
                {
                    queue.push_back((y - 1, x))
                }
                if y < map.len() - 1
                    && dist_map[y + 1][x] == depth - 1
                    && (map[y + 1][x] == '|' || map[y + 1][x] == 'L' || map[y + 1][x] == 'J')
                {
                    queue.push_back((y + 1, x))
                }
            }
            '-' => {
                if x > 0
                    && dist_map[y][x - 1] == depth - 1
                    && (map[y][x - 1] == '-' || map[y][x - 1] == 'L' || map[y][x - 1] == 'F')
                {
                    queue.push_back((y, x - 1))
                }
                if x < x_len - 1
                    && dist_map[y][x + 1] == depth - 1
                    && (map[y][x + 1] == '-' || map[y][x + 1] == '7' || map[y][x + 1] == 'J')
                {
                    queue.push_back((y, x + 1));
                }
            }
            'L' => {
                if y > 0
                    && dist_map[y - 1][x] == depth - 1
                    && (map[y - 1][x] == '|' || map[y - 1][x] == '7' || map[y - 1][x] == 'F')
                {
                    queue.push_back((y - 1, x))
                }
                if x < x_len - 1
                    && dist_map[y][x + 1] == depth - 1
                    && (map[y][x + 1] == '-' || map[y][x + 1] == '7' || map[y][x + 1] == 'J')
                {
                    queue.push_back((y, x + 1));
                }
            }
            'J' => {
                if y > 0
                    && dist_map[y - 1][x] == depth - 1
                    && (map[y - 1][x] == '|' || map[y - 1][x] == '7' || map[y - 1][x] == 'F')
                {
                    queue.push_back((y - 1, x))
                }
                if x > 0
                    && dist_map[y][x - 1] == depth - 1
                    && (map[y][x - 1] == '-' || map[y][x - 1] == 'L' || map[y][x - 1] == 'F')
                {
                    queue.push_back((y, x - 1))
                }
            }
            '7' => {
                if y < map.len() - 1
                    && dist_map[y + 1][x] == depth - 1
                    && (map[y + 1][x] == '|' || map[y + 1][x] == 'L' || map[y + 1][x] == 'J')
                {
                    queue.push_back((y + 1, x))
                }
                if x > 0
                    && dist_map[y][x - 1] == depth - 1
                    && (map[y][x - 1] == '-' || map[y][x - 1] == 'L' || map[y][x - 1] == 'F')
                {
                    queue.push_back((y, x - 1))
                }
            }
            'F' => {
                if y < map.len() - 1
                    && dist_map[y + 1][x] == depth - 1
                    && (map[y + 1][x] == '|' || map[y + 1][x] == 'L' || map[y + 1][x] == 'J')
                {
                    queue.push_back((y + 1, x))
                }
                if x < x_len - 1
                    && dist_map[y][x + 1] == depth - 1
                    && (map[y][x + 1] == '-' || map[y][x + 1] == '7' || map[y][x + 1] == 'J')
                {
                    queue.push_back((y, x + 1));
                }
            }
            'S' | '.' => continue,
            _ => panic!("Unknown map char {}", map[y][x]),
        }
    }

    // Clean up on y axis
    for y in 0..loop_map.len() {
        let first = loop_map[y]
            .iter()
            .find_position(|c| **c == LoopEntry::Loop)
            .map(|(i, _)| i)
            .unwrap_or(x_len);

        let last = loop_map[y]
            .iter()
            .enumerate()
            .rev()
            .find(|(_, c)| **c == LoopEntry::Loop)
            .map(|(i, _)| i)
            .unwrap_or(x_len);

        for x in (0..first).chain(last..x_len) {
            if loop_map[y][x] == LoopEntry::Unknown {
                loop_map[y][x] = LoopEntry::OutsideLoop;
            }
        }
    }
    // Clean up on x axis
    for x in 0..x_len {
        let first = (0..loop_map.len())
            .find(|y| loop_map[*y][x] == LoopEntry::Loop)
            .unwrap_or(loop_map.len());

        let last = (0..loop_map.len())
            .rev()
            .find(|y| loop_map[*y][x] == LoopEntry::Loop)
            .unwrap_or(loop_map.len());

        for y in (0..first).chain(last..loop_map.len()) {
            if loop_map[y][x] == LoopEntry::Unknown {
                loop_map[y][x] = LoopEntry::OutsideLoop;
            }
        }
    }

    // Fill in pipes
    for (y, row) in map.iter().enumerate() {
        for (x, c) in row.iter().enumerate() {
            if *c != '.' && loop_map[y][x] == LoopEntry::Unknown {
                loop_map[y][x] = LoopEntry::Pipe;
            }
        }
    }

    // expand
    let mut enlarged = loop_map
        .iter()
        .enumerate()
        .flat_map(|(y, r)| {
            let mut o1 = vec![LoopEntry::Unknown; r.len() * 3];
            let mut o2 = vec![LoopEntry::Unknown; r.len() * 3];
            let mut o3 = vec![LoopEntry::Unknown; r.len() * 3];
            for (x, e) in r.iter().enumerate() {
                let start_x = x * 3;
                match *e {
                    LoopEntry::OutsideLoop => {
                        for i in 0..3 {
                            o1[start_x + i] = LoopEntry::OutsideLoop;
                            o2[start_x + i] = LoopEntry::OutsideLoop;
                            o3[start_x + i] = LoopEntry::OutsideLoop;
                        }
                    }
                    LoopEntry::Loop | LoopEntry::Pipe => {
                        let fill_with = *e;
                        match map[y][x] {
                            '|' => {
                                o1[start_x + 1] = fill_with;
                                o2[start_x + 1] = fill_with;
                                o3[start_x + 1] = fill_with;
                            }
                            '-' => {
                                for i in 0..3 {
                                    o2[start_x + i] = fill_with;
                                }
                            }
                            'L' => {
                                o1[start_x + 1] = fill_with;
                                o2[start_x + 1] = fill_with;
                                o2[start_x + 2] = fill_with;
                            }
                            'J' => {
                                o1[start_x + 1] = fill_with;
                                o2[start_x + 1] = fill_with;
                                o2[start_x] = fill_with;
                            }
                            '7' => {
                                o2[start_x] = fill_with;
                                o2[start_x + 1] = fill_with;
                                o3[start_x + 1] = fill_with;
                            }
                            'F' => {
                                o2[start_x + 2] = fill_with;
                                o2[start_x + 1] = fill_with;
                                o3[start_x + 1] = fill_with;
                            }
                            'S' => {
                                o2[start_x + 1] = fill_with;
                                if y < x_len - 1
                                    && (map[y][x + 1] == '-'
                                        || map[y][x + 1] == '7'
                                        || map[y][x + 1] == 'J')
                                {
                                    o2[start_x + 2] = fill_with;
                                }
                                if x > 0
                                    && (map[y][x - 1] == '-'
                                        || map[y][x - 1] == 'L'
                                        || map[y][x - 1] == 'F')
                                {
                                    o2[start_x] = fill_with;
                                }
                                if y < loop_map.len() - 1
                                    && (map[y + 1][x] == '|'
                                        || map[y + 1][x] == 'L'
                                        || map[y + 1][x] == 'J')
                                {
                                    o3[start_x + 1] = fill_with;
                                }
                                if y > 0
                                    && (map[y - 1][x] == '|'
                                        || map[y - 1][x] == '7'
                                        || map[y - 1][x] == 'F')
                                {
                                    o1[start_x + 1] = fill_with;
                                }
                            }
                            _ => panic!("Unknown map char {}", map[y][x]),
                        }
                    }
                    _ => (),
                }
            }
            [o1, o2, o3]
        })
        .collect_vec();

    let enlarged_x_len = enlarged[0].len();
    // Cleanup
    let mut queue = VecDeque::new();
    for y in 0..enlarged.len() {
        for x in 0..enlarged_x_len {
            if enlarged[y][x] == LoopEntry::OutsideLoop {
                if y > 0 {
                    queue.push_back((y - 1, x));
                }
                if y < enlarged.len() - 1 {
                    queue.push_back((y + 1, x));
                }
                if x > 0 {
                    queue.push_back((y, x - 1));
                }
                if x < enlarged_x_len - 1 {
                    queue.push_back((y, x + 1));
                }
                while let Some((y, x)) = queue.pop_front() {
                    if enlarged[y][x] != LoopEntry::Unknown {
                        continue;
                    }
                    enlarged[y][x] = LoopEntry::OutsideLoop;
                    if y > 0 {
                        queue.push_back((y - 1, x));
                    }
                    if y < enlarged.len() - 1 {
                        queue.push_back((y + 1, x));
                    }
                    if x > 0 {
                        queue.push_back((y, x - 1));
                    }
                    if x < enlarged_x_len - 1 {
                        queue.push_back((y, x + 1));
                    }
                }
            }
        }
    }

    for y in 0..loop_map.len() {
        for x in 0..x_len {
            let start_y = y * 3;
            let start_x = x * 3;
            if loop_map[y][x] == LoopEntry::Unknown
                && enlarged[start_y + 1][start_x + 1] != LoopEntry::Unknown
            {
                loop_map[y][x] = LoopEntry::OutsideLoop;
                continue;
            }

            if loop_map[y][x] == LoopEntry::Pipe
                && !(0..2).any(|py| {
                    (0..2).any(|px| enlarged[start_y + py][start_x + px] == LoopEntry::OutsideLoop)
                })
            {
                loop_map[y][x] = LoopEntry::Unknown;
            }
        }
    }

    // Count
    loop_map
        .iter()
        .flat_map(|r| r.iter().filter(|c| **c == LoopEntry::Unknown))
        .count()
        .to_string()
}

#[derive(Clone, Copy, PartialEq, Eq)]
enum LoopEntry {
    Unknown,
    Loop,
    OutsideLoop,
    Pipe,
}

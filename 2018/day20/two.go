package day20

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	root, _, err := readInput(lines[0][1:])
	if err != nil {
		return "", err
	}

	grid := make(map[int]map[int]*cellObj)
	grid[0] = map[int]*cellObj{0: {}}

	drawGrid(grid, tools.Pos2D{}, root)
	// gridPrint(grid)

	countMap := generateCountMap(grid)

	answer := 1
	for _, col := range countMap {
		for _, val := range col {
			if val > 1000 {
				answer++
			}
		}
	}

	return strconv.Itoa(answer), nil // Return with the answer
}

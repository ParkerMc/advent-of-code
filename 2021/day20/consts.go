// Package day20 2018
package day20

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "5391"
const testAnswerTwo = "16383"

var testFilename = "../test/private/" + tools.GetPackageName()

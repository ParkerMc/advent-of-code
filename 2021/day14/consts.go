// Package day14 2018
package day14

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "2010"
const testAnswerTwo = "2437698971143"

var testFilename = "../test/private/" + tools.GetPackageName()

// Package day01 2020
package day01

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "786811"
const testAnswerTwo = "199068980"

var testFilename = "../test/private/" + tools.GetPackageName()

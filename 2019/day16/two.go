package day16

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

// Thanks to the interwebs for this I was looking in the wrong place
func runFTT2(msg []int) []int {
	for k := 0; k < 100; k++ {
		last := 0
		for i := len(msg) - 1; i > -1; i-- {
			last = (last + msg[i]) % 10
			msg[i] = last
		}
	}
	return msg
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	msgPart, err := praseInput(lines[0])
	if err != nil {
		return "", err
	}

	offset := 0
	for i := 0; i < 7; i++ {
		offset *= 10
		offset += msgPart[i]
	}
	partlen := len(msgPart)
	legnth := (partlen * 10000) - offset
	msg := make([]int, legnth)
	pull := offset % partlen
	for i := 0; i < legnth; i++ {
		msg[i] = msgPart[pull]
		pull++
		if pull == partlen {
			pull = 0
		}
	}
	msg = runFTT2(msg)

	out := 0
	for i := 0; i < 8; i++ {
		out *= 10
		out += msg[i]
	}

	return strconv.Itoa(out), nil // Return with the answer
}

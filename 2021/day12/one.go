package day12

import (
	"strconv"
	"strings"
	"unicode"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

func isBigCave(name string) bool {
	return unicode.IsUpper(rune(name[0]))
}

func alreadyVisted(visitedSmall []string, name string) bool {
	for _, cave := range visitedSmall {
		if cave == name {
			return true
		}
	}
	return false
}

func countPaths(paths map[string][]string, cur string, visitedSmall []string) int {
	out := 0
	if cur == "end" {
		return 1
	}
	if !isBigCave(cur) {
		visitedSmall = append(visitedSmall, cur)
	}
	for _, next := range paths[cur] {
		if !isBigCave(next) && alreadyVisted(visitedSmall, next) {
			continue
		}
		out += countPaths(paths, next, visitedSmall)
	}
	return out

}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	paths := make(map[string][]string, 0)
	for _, strPath := range lines {
		split := strings.Split(strPath, "-")
		paths[split[0]] = append(paths[split[0]], split[1])
		paths[split[1]] = append(paths[split[1]], split[0])
	}

	return strconv.Itoa(countPaths(paths, "start", make([]string, 0))), nil // Return with the answer
}

package day01

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	frequency := 0
	hits := []int{frequency}
	for {
		for _, line := range lines { // Loop though the lines
			number, err := strconv.Atoi(line) // Get the number
			if err != nil {
				return "", err
			}
			frequency += number

			for _, i := range hits {
				if i == frequency {
					return strconv.Itoa(frequency), nil // Return with the answer
				}
			}

			hits = append(hits, frequency)
		}
	}
}

package day19

// Two the second puzzle for the day
func Two(filename string) (string, error) {
	return run(filename, []int{1})
}

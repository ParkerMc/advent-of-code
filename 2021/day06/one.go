package day06

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// globalMap[Days left]
var globalMap = make(map[int]int, 0)

func getDay(days int) int {
	if val, ok := globalMap[days]; ok {
		return val
	}
	if days-7 < 0 {
		return 1
	} else if days-7 == 0 {
		return 2
	}
	val := getDay(days-7) + getDay(days-9)
	globalMap[days] = val
	return val
}

func runForDays(line string, days int) (int, error) {
	var err error
	split := strings.Split(line, ",")
	fish := make([]int, len(split))
	for i, val := range split {
		fish[i], err = strconv.Atoi(val)
		if err != nil {
			return 0, err
		}
	}

	answer := 0
	for _, val := range fish {
		answer += getDay(days + 6 - val)
	}
	return answer, nil
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	answer, err := runForDays(lines[0], 80)
	if err != nil {
		return "", err
	}

	return strconv.Itoa(answer), nil // Return with the answer
}

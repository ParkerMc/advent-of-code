package day18

import (
	"container/list"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

func solve2(equation *list.List) (int, error) {
	current := equation.Front()
	for current != nil {
		if current.Next() == nil {
			break
		}
		if current.Next().Value.(*store).op == opAdd {
			err := solvePT(current, equation, 2)
			if err != nil {
				return 0, err
			}
			continue
		}
		current = current.Next()
		if current != nil {
			current = current.Next()
		}
	}

	for equation.Len() > 1 {
		err := solvePT(equation.Front(), equation, 2)
		if err != nil {
			return 0, err
		}
	}
	return equation.Front().Value.(*store).num, nil
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	out := 0

	for _, line := range lines {
		equation, err := load(line)
		if err != nil {
			return "", err
		}

		val, err := solve2(equation)
		if err != nil {
			return "", err
		}

		out += val
	}

	return strconv.Itoa(out), nil // Return with the answer
}

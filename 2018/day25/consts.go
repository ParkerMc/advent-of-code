// Package day25 2018
package day25

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "375"
const testAnswerTwo = ""

var testFilename = "../test/private/" + tools.GetPackageName()

// Package day23 2018
package day23

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "499"
const testAnswerTwo = "80162663"

var testFilename = "../test/private/" + tools.GetPackageName()

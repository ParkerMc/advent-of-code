use std::time::Duration;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    let directions = input.iter().map(|l| {
        let mut split = l.split(' ');
        (
            split
                .next()
                .expect("Expected direction")
                .chars()
                .next()
                .expect("Expected one"),
            split
                .next()
                .expect("Expected amount")
                .parse()
                .expect("Expected num"),
        )
    });

    solve(directions).to_string()
}

fn part2(input: &Vec<String>) -> String {
    let directions = input.iter().map(|l| {
        let hex = l.split(' ').last().expect("Expected last");
        // (#70c710)
        (
            hex.chars().skip(7).next().expect("Expected one"),
            i64::from_str_radix(&hex[2..7], 16).expect("Expected hex num"),
        )
    });

    solve(directions).to_string()
}

fn solve(directions: impl Iterator<Item = (char, i64)>) -> i64 {
    let mut x = 0;
    let mut y = 0;
    let mut len = 0;
    let mut sum = 0;
    for (dir, amt) in directions {
        let (nx, ny) = match dir {
            'R' | '0' => (x + amt, y),
            'D' | '1' => (x, y + amt),
            'L' | '2' => (x - amt, y),
            'U' | '3' => (x, y - amt),
            _ => panic!("Unknown: {}", dir),
        };

        sum += x * ny - y * nx;
        len += amt;
        x = nx;
        y = ny;
    }

    sum.abs() / 2 + len / 2 + 1
}

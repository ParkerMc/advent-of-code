// Package day06 2019
package day06

import "gitlab.com/parkermc/advent-of-code/2019/tools"

const testAnswerOne = "292387"
const testAnswerTwo = "433"

var testFilename = "../test/private/" + tools.GetPackageName()

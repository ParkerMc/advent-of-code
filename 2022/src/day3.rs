use std::collections::HashSet;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) {
    utils::day::run_day(part, module_path!(), part1, part2, input);
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    let sum: u32 = input
        .iter()
        .map(|l| {
            let split = l.split_at(l.len() / 2);
            let set: HashSet<_> = split.0.chars().collect();
            let shared = split
                .1
                .chars()
                .find(|c| set.contains(&c))
                .expect("Failed to find common char") as u32;
            if shared > 96 {
                return shared - 96;
            }
            return shared - 38;
        })
        .sum();
    sum.to_string()
}

fn part2(input: &Vec<String>) -> String {
    let sum: u32 = input
        .chunks(3)
        .map(|l| {
            let set1: HashSet<_> = l[0].chars().collect();
            let set2: HashSet<_> = l[1].chars().collect();
            let shared = l[2]
                .chars()
                .find(|c| set1.contains(&c) && set2.contains(&c))
                .expect("Failed to find common char") as u32;
            if shared > 96 {
                return shared - 96;
            }
            return shared - 38;
        })
        .sum();
    sum.to_string()
}

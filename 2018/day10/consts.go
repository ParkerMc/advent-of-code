// Package day10 2018
package day10

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "\n" +
	"#####     ##    #    #  #       #####     ##    #####   ##### \n" +
	"#    #   #  #   ##   #  #       #    #   #  #   #    #  #    #\n" +
	"#    #  #    #  ##   #  #       #    #  #    #  #    #  #    #\n" +
	"#    #  #    #  # #  #  #       #    #  #    #  #    #  #    #\n" +
	"#####   #    #  # #  #  #       #####   #    #  #####   ##### \n" +
	"#       ######  #  # #  #       #       ######  #       #  #  \n" +
	"#       #    #  #  # #  #       #       #    #  #       #   # \n" +
	"#       #    #  #   ##  #       #       #    #  #       #   # \n" +
	"#       #    #  #   ##  #       #       #    #  #       #    #\n" +
	"#       #    #  #    #  ######  #       #    #  #       #    #"
const testAnswerTwo = "10304"

var testFilename = "../test/private/" + tools.GetPackageName()

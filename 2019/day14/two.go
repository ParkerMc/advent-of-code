package day14

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

const trillion = 1000000000000

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	reactions, err := parseReactions(lines)
	if err != nil {
		return "", err
	}

	// basiclly brute force it intelligently because I'm lazy still under 0.00s

	// Get the lower guess
	extra := make(map[string]int64, 0)
	var amtUsed int64
	var amtMade int64
	var amtToMake int64 = 100000000
	for {
		var oreForAmt int64
		oreForAmt, extra, err = getOreNeeded(reactions, extra, amtToMake)
		if err != nil {
			return "", err
		}
		if oreForAmt+amtUsed > trillion {
			if amtToMake == 1 {
				break
			}
			amtToMake /= 10
		} else {
			amtUsed += oreForAmt
			amtMade += amtToMake
		}
	}

	// Get exact
	realAmt := amtMade
	for {
		realAmt++
		oreForAmt, _, err := getOreNeeded(reactions, nil, realAmt)
		if err != nil {
			return "", err
		}
		if oreForAmt > trillion {
			realAmt--
			break
		}
	}

	return strconv.FormatInt(realAmt, 10), nil // Will never get here
}

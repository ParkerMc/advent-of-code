// Package day16 2020
package day16

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "29851"
const testAnswerTwo = "3029180675981"

var testFilename = "../test/private/" + tools.GetPackageName()

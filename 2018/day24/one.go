package day24

import (
	"errors"
	"log"
	"math"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

type dmgType int
type armyName string

const (
	fire        dmgType  = 1
	radiation   dmgType  = 2
	bludgeoning dmgType  = 3
	slashing    dmgType  = 4
	cold        dmgType  = 5
	none        armyName = "None"
	immune      armyName = "Immune System"
	infection   armyName = "Infection"
)

var debug = false

type army struct {
	name             armyName
	groups           []*group
	totalAttackDmg   int
	minimumHitPoints int
}

func (a *army) updateMinimumHitPoints() {
	a.minimumHitPoints = math.MaxInt
	for _, g := range a.groups {
		if g.hitPoints < a.minimumHitPoints {
			a.minimumHitPoints = g.hitPoints
		}
	}
}

func (a *army) findTargets(enemy *army) {
	groupsCopy := append([]*group(nil), a.groups...)
	sort.Slice(groupsCopy, func(i, j int) bool {
		iv := groupsCopy[i].getEffectivePower()
		jv := groupsCopy[j].getEffectivePower()
		if iv == jv {
			return groupsCopy[i].initiative > groupsCopy[j].initiative
		}
		return iv > jv
	})

	taken := make([]int, len(groupsCopy))
	a.totalAttackDmg = 0
	for i, g := range groupsCopy {
		dmg := g.findTarget(enemy, taken[:i])
		taken[i] = g.target
		a.totalAttackDmg += dmg
	}
}

type group struct {
	armyName   armyName
	i          int
	count      int
	hitPoints  int
	attackType dmgType
	attackDmg  int
	initiative int
	immuneTo   []dmgType
	weakTo     []dmgType
	target     int
}

func (g *group) getEffectivePower() int {
	return g.count * g.attackDmg
}

func (g *group) findTarget(enemy *army, taken []int) int {
	damageMap := make([]int, len(enemy.groups))
OUTER:
	for i, egroup := range enemy.groups {
		for _, t := range taken {
			if egroup.i == t {
				damageMap[i] = -1
				continue OUTER
			}
		}

		damageMap[i] = g.calcDamage(egroup)
		if debug {
			log.Printf("%s group %d would deal defending group %d %d damage", g.armyName, g.i+1, egroup.i+1, damageMap[i])
		}
	}

	maxI := -1
	maxV := 0
	maxEp := 0
	maxInitiative := 0
	for i, v := range damageMap {
		egroup := enemy.groups[i]
		if v > maxV {
			maxI = egroup.i
			maxEp = egroup.getEffectivePower()
			maxV = v
			maxInitiative = egroup.initiative
		} else if v == maxV && v != 0 {
			iep := egroup.getEffectivePower()
			if iep > maxEp || (iep == maxEp && egroup.initiative > maxInitiative) {
				maxI = egroup.i
				maxEp = egroup.getEffectivePower()
				maxV = v
				maxInitiative = egroup.initiative
			}
		}
	}
	g.target = maxI

	return maxV
}

func (g *group) calcDamage(enemy *group) int {
	for _, dtype := range enemy.immuneTo {
		if dtype == g.attackType {
			return 0
		}
	}
	dam := g.getEffectivePower()
	for _, dtype := range enemy.weakTo {
		if dtype == g.attackType {
			return dam * 2
		}
	}
	return dam
}

func (g *group) attack(armies []*army) {
	if g.target < 0 {
		return
	}
	enemyArmy := armies[0]
	if g.armyName == immune {
		enemyArmy = armies[1]
	}
	targetI := -1
	var enemy *group
	for i, eg := range enemyArmy.groups {
		if eg.i == g.target {
			targetI = i
			enemy = eg
			break
		}
	}
	if enemy == nil {
		return // enemy was killed
	}

	if enemyArmy.totalAttackDmg < g.hitPoints { // If there's no change of them killing us then kill them as that's the only possible outcome
		enemy.count = 0
	}
	killUnits := min(g.calcDamage(enemy)/enemy.hitPoints, enemy.count)
	enemy.count -= killUnits
	if enemy.count == 0 {
		if len(enemyArmy.groups) == 1 {
			enemyArmy.groups = make([]*group, 0)
		} else if len(enemyArmy.groups) == 2 {
			remainingI := 0
			if remainingI == targetI {
				remainingI = 1
			}
			enemyArmy.groups = []*group{enemyArmy.groups[remainingI]}
		} else {
			enemyArmy.groups = append(enemyArmy.groups[:targetI], enemyArmy.groups[targetI+1:]...)
		}
		enemyArmy.updateMinimumHitPoints()
	}
	if debug {
		log.Printf("%s group %d attacks defending group %d, killing %d units", g.armyName, g.i+1, g.target+1, killUnits)
	}

}

func getType(str string) (dmgType, error) {
	switch str {
	case "fire":
		return fire, nil
	case "radiation":
		return radiation, nil
	case "bludgeoning":
		return bludgeoning, nil
	case "slashing":
		return slashing, nil
	case "cold":
		return cold, nil
	default:
		return 0, errors.New("Unknown dmg type " + str)
	}
}

func (d dmgType) getName() string {
	switch d {
	case fire:
		return "fire"
	case radiation:
		return "radiation"
	case bludgeoning:
		return "bludgeoning"
	case slashing:
		return "slashing"
	case cold:
		return "cold"
	default:
		return ""
	}
}

func loadData(lines []string) ([]*army, error) {
	armies := make([]*army, 2)
	armies[0] = &army{name: immune, groups: make([]*group, 0)}
	armies[1] = &army{name: infection, groups: make([]*group, 0)}

	skip := 0
	armyI := 0
	armyName := immune
	i := 0
	for _, line := range lines[1:] {
		if skip > 0 {
			skip--
			continue
		}
		if line == "" {
			skip = 1
			i = 0
			armyI++
			armyName = infection
			continue
		}
		inside := ""
		outside := line
		if strings.Contains(line, "(") {
			split := strings.Split(line, " (")
			split2 := strings.Split(split[1], ")")
			outside = split[0] + split2[1]
			inside = split2[0]
		}

		outsideSplit := strings.Split(outside, " ")
		count, err := strconv.Atoi(outsideSplit[0])
		if err != nil {
			return nil, err
		}
		hitPoints, err := strconv.Atoi(outsideSplit[4])
		if err != nil {
			return nil, err
		}
		attackDmg, err := strconv.Atoi(outsideSplit[12])
		if err != nil {
			return nil, err
		}
		attackType, err := getType(outsideSplit[13])
		if err != nil {
			return nil, err
		}
		initiative, err := strconv.Atoi(outsideSplit[17])
		if err != nil {
			return nil, err
		}

		immuneTo := make([]dmgType, 0)
		weakTo := make([]dmgType, 0)

		if inside != "" {
			insideSplit := strings.Split(inside, "; ")
			for _, part := range insideSplit {
				partSplit := strings.Split(strings.ReplaceAll(part, ",", ""), " ")
				dmgTypes := make([]dmgType, 0)
				for _, dmgType := range partSplit[2:] {
					t, err := getType(dmgType)
					if err != nil {
						return nil, err
					}
					dmgTypes = append(dmgTypes, t)
				}
				switch partSplit[0] {
				case "weak":
					weakTo = append(weakTo, dmgTypes...)
				case "immune":
					immuneTo = append(immuneTo, dmgTypes...)
				default:
					return nil, errors.New("Unknown inner type: " + partSplit[0])

				}
			}
		}
		target := -1
		g := group{
			armyName,
			i,
			count,
			hitPoints,
			attackType,
			attackDmg,
			initiative,
			immuneTo,
			weakTo,
			target,
		}
		armies[armyI].groups = append(armies[armyI].groups, &g)
		i++
	}
	armies[0].updateMinimumHitPoints()
	armies[1].updateMinimumHitPoints()
	return armies, nil
}

func runSim(armiesIn []*army, boost int) (armyName, int) {
	armies := make([]*army, 2)
	for i, a := range armiesIn {
		armyBoost := 0
		if a.name == immune {
			armyBoost = boost
		}
		groups := make([]*group, len(a.groups))
		for j, g := range a.groups {
			groups[j] = &group{
				armyName:   g.armyName,
				i:          g.i,
				count:      g.count,
				hitPoints:  g.hitPoints,
				attackType: g.attackType,
				attackDmg:  g.attackDmg + armyBoost,
				initiative: g.initiative,
				immuneTo:   g.immuneTo,
				weakTo:     g.weakTo,
				target:     g.target,
			}
		}
		armies[i] = &army{
			name:             a.name,
			groups:           groups,
			minimumHitPoints: a.minimumHitPoints,
		}
	}

	for len(armies[0].groups) > 0 && len(armies[1].groups) > 0 {
		if debug {
			log.Print("---------------------------------------------------------------")
			for _, army := range armies {
				log.Printf("%s:", army.name)
				for _, group := range army.groups {
					log.Printf("Group %d contains %d units", group.i+1, group.count)
				}
			}
		}

		// Target phase
		armies[1].findTargets(armies[0])
		armies[0].findTargets(armies[1])

		if debug {
			log.Print()
		}

		// Detect stale mate
		if armies[0].minimumHitPoints > armies[1].totalAttackDmg && armies[1].minimumHitPoints > armies[0].totalAttackDmg {
			break
		}

		// Attack phase
		combined := append([]*group(nil), armies[0].groups...)
		combined = append(combined, armies[1].groups...)
		sort.Slice(combined, func(i, j int) bool {
			return combined[i].initiative > combined[j].initiative
		})

		for _, group := range combined {
			group.attack(armies)
		}
	}

	total := 0
	// Count for stalemate
	if len(armies[0].groups) != 0 && len(armies[1].groups) != 0 {
		for _, group := range armies[1].groups {
			total += group.count
		}
		return none, total
	} else if len(armies[0].groups) == 0 { // Count for infection
		for _, group := range armies[1].groups {
			total += group.count
		}
		return infection, total
	}

	for _, group := range armies[0].groups {
		total += group.count
	}
	return immune, total
}

// One the first puzzle for the day
func One(filename string) (string, error) {
	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	armies, err := loadData(lines)
	if err != nil {
		return "", err
	}

	_, total := runSim(armies, 0)
	return strconv.Itoa(total), nil // Return with the answer
}

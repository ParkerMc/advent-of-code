package day09

import (
	"errors"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

const preambleLen = 25

func findInvalid(lines []int) (int, error) {
	preamble := lines[:preambleLen]

	for _, num := range lines[preambleLen:] {
		good := false
		for i, num1 := range preamble {
			if good {
				break
			}
			for _, num2 := range preamble[i+1:] {
				if num == num1+num2 {
					good = true
					break
				}
			}
		}
		if !good {
			return num, nil
		}
		preamble = append(preamble[1:], num)
	}
	return -1, errors.New("did not find any invalid number")
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	nums := make([]int, len(lines))
	for i, str := range lines {
		nums[i], err = strconv.Atoi(str)
		if err != nil {
			return "", err
		}
	}

	num, err := findInvalid(nums)
	if err != nil {
		return "", err
	}

	return strconv.Itoa(num), nil // Return with the answer
}

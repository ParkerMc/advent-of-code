use std::time::Duration;

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    let (a, b, c, is) = load(input);
    solve(a, b, c, &is)
        .into_iter()
        .map(|n| n.to_string())
        .join(",")
}

fn part2(input: &Vec<String>) -> String {
    let (_, b, c, is) = load(input);
    let mut a = 0;
    for i in (0..is.len()).rev() {
        a *= 8;
        let mut sol = solve(a, b, c, &is);
        while sol != is[i..] {
            a += 1;
            sol = solve(a, b, c, &is);
        }
    }
    a.to_string()
}

fn load(input: &Vec<String>) -> (u64, u64, u64, Vec<u64>) {
    let a = input[0].split(": ").last().unwrap().parse::<u64>().unwrap();
    let b = input[1].split(": ").last().unwrap().parse::<u64>().unwrap();
    let c = input[2].split(": ").last().unwrap().parse::<u64>().unwrap();
    let is = input[4]
        .split(": ")
        .last()
        .unwrap()
        .split(",")
        .map(|s| s.parse::<u64>().unwrap())
        .collect_vec();
    (a, b, c, is)
}

fn solve(a: u64, b: u64, c: u64, is: &Vec<u64>) -> Vec<u64> {
    let mut a = a;
    let mut b = b;
    let mut c = c;
    let mut ip = 0;
    let mut output = vec![];

    while (ip as usize) < is.len() {
        match is[ip] {
            0 => a = (a as f64 / 2_f64.powf(get_combo(is[ip + 1], a, b, c) as f64)) as u64,
            1 => b ^= is[ip + 1],
            2 => b = get_combo(is[ip + 1], a, b, c) % 8,
            3 => {
                if a != 0 {
                    ip = is[ip + 1] as usize;
                    continue;
                }
            }
            4 => b ^= c,
            5 => output.push(get_combo(is[ip + 1], a, b, c) % 8),
            6 => b = (a as f64 / 2_f64.powf(get_combo(is[ip + 1], a, b, c) as f64)) as u64,
            7 => c = (a as f64 / 2_f64.powf(get_combo(is[ip + 1], a, b, c) as f64)) as u64,
            _ => panic!("Not defined {}", is[ip]),
        }
        ip += 2;
    }

    output
}

fn get_combo(combo: u64, a: u64, b: u64, c: u64) -> u64 {
    match combo {
        0 => 0,
        1 => 1,
        2 => 2,
        3 => 3,
        4 => a,
        5 => b,
        6 => c,
        7 => panic!("Unheard of 7!"),
        _ => panic!("Out of change {}", combo),
    }
}

package day21

import (
	"log"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

type rolls struct {
	player1 int
	player2 int
}

var probMap = map[int]uint64{3: 1, 4: 3, 5: 6, 6: 7, 7: 6, 8: 3, 9: 1}
var cache map[int][]uint64

func getGameKey(player1 *player, player2 *player) int {
	// 32 bits
	return (((((player1.place << 5) | player1.score) << 4) | player2.place) << 5) | player2.score
}

func doTurn(player1 *player, player2 *player) (p1out, p2out uint64) {
	// Check cache
	key := getGameKey(player1, player2)
	if val, ok := cache[key]; ok {
		return val[0], val[1]
	}

	// Else we need to find the value
	for roll, times := range probMap {
		p1Clone := player1.clone()

		p1Clone.place = (p1Clone.place + roll) % 10
		p1Clone.score += p1Clone.place + 1
		if p1Clone.score >= 21 {
			p1out += times
			continue
		}

		p2wins, p1wins := doTurn(player2, p1Clone)
		p1out += p1wins * times
		p2out += p2wins * times
	}

	cache[key] = []uint64{p1out, p2out}
	return p1out, p2out

}

// Two the second puzzle for the day
func Two(filename string) (string, error) {
	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	players := make([]*player, 0)
	for i, line := range lines {
		split := strings.Split(line, " ")
		place, err := strconv.Atoi(split[len(split)-1])
		if err != nil {
			return "", err
		}
		players = append(players, &player{place: place - 1})
		if debug {
			log.Printf("Player %d starting position: %d", i+1, place)
		}
	}

	cache = make(map[int][]uint64)

	p1wins, p2wins := doTurn(players[0], players[1])

	return strconv.FormatUint(max(p1wins, p2wins), 10), nil // Return with the answer
}

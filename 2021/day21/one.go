package day21

import (
	"log"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

const debug = false

type die struct {
	lastVal   int
	rollCount int
}

func (d *die) roll3() (one, two, three int) {
	d.lastVal++
	if d.lastVal == 101 {
		d.lastVal = 1
	}
	one = d.lastVal
	d.lastVal++
	if d.lastVal == 101 {
		d.lastVal = 1
	}
	two = d.lastVal
	d.lastVal++
	if d.lastVal == 101 {
		d.lastVal = 1
	}
	d.rollCount += 3
	three = d.lastVal

	return one, two, three
}

type player struct {
	score int
	place int
}

func (p *player) clone() *player {
	return &player{
		p.score,
		p.place,
	}
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	players := make([]*player, 0)
	for i, line := range lines {
		split := strings.Split(line, " ")
		place, err := strconv.Atoi(split[len(split)-1])
		if err != nil {
			return "", err
		}
		players = append(players, &player{place: place - 1})
		if debug {
			log.Printf("Player %d starting position: %d", i+1, place)
		}
	}

	var dice = die{}
OUTTER:
	for {
		for i, p := range players {
			one, two, three := dice.roll3()
			p.place = (p.place + one + two + three) % 10
			p.score += p.place + 1
			if debug {
				log.Printf("Player %d rolls %d+%d+%d and moves to space %d for a total score of %d.", i+1, one, two, three, p.place+1, p.score)
			}
			if p.score >= 1000 {
				break OUTTER
			}
		}
	}

	return strconv.Itoa(min(players[0].score, players[1].score) * dice.rollCount), nil // Return with the answer
}

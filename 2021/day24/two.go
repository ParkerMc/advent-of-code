package day24

import "strconv"

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	rules, err := getRules(filename)
	if err != nil {
		return "", err
	}

	num := make([]int, 14)
	for _, r := range rules {
		if r.diff > 0 {
			num[r.fromI] = 1
			num[r.toI] = r.diff + 1
		} else {
			num[r.fromI] = 1 - r.diff
			num[r.toI] = 1
		}
	}

	answer := 0
	for _, n := range num {
		answer *= 10
		answer += n
	}

	return strconv.Itoa(answer), nil // Return with the answer
}

// Package day04 2020
package day04

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "204"
const testAnswerTwo = "179"

var testFilename = "../test/private/" + tools.GetPackageName()

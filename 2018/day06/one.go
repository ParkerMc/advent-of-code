package day06

import (
	"errors"
	"math"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// Point used to store the points
type Point struct {
	X        int
	Z        int
	Infinite bool
	Amt      int
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	points := make([]Point, 0)
	for _, line := range lines {
		lineSplit := strings.Split(line, ", ")
		x, err := strconv.Atoi(lineSplit[0])
		if err != nil {
			return "", err
		}
		z, err := strconv.Atoi(lineSplit[1])
		if err != nil {
			return "", err
		}
		if x < 0 || z < 0 {
			return "", errors.New("program does not support negative cords")
		}
		points = append(points, Point{x, z, false, 0})
	}

	biggestX := points[0].X
	biggestZ := points[0].Z
	for _, point := range points {
		if biggestX < point.X {
			biggestX = point.X
		}
		if biggestZ < point.Z {
			biggestZ = point.Z
		}
	}

	for x := 0; x <= biggestX; x++ {
		for z := 0; z <= biggestZ; z++ {
			closest := -1
			closestAmt := 10000000000000

			for pointID, point := range points {
				amt := int(math.Abs(float64(point.X-x))) + int(math.Abs(float64(point.Z-z)))
				if amt < closestAmt {
					closest = pointID
					closestAmt = amt
				} else if amt == closestAmt {
					closest = -1
				}
			}
			if closest != -1 {
				if x == 0 || z == 0 || x == biggestX || z == biggestZ {
					points[closest].Infinite = true
				}
				points[closest].Amt++
			}
		}
	}

	biggestArea := Point{0, 0, false, 0}
	for _, point := range points {
		if (!point.Infinite) && biggestArea.Amt < point.Amt {
			biggestArea = point
		}
	}
	return strconv.Itoa(biggestArea.Amt), nil // Return with the answer
}

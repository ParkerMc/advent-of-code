// Package day14 2018
package day14

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "1221283494"
const testAnswerTwo = "20261485"

var testFilename = "../test/private/" + tools.GetPackageName()

package day05

import (
	"errors"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	program, err := tools.NewProgram(lines[0])
	if err != nil {
		return "", err
	}

	program.AddInput(5)

	err = program.Run()
	if err != nil {
		return "", err
	}

	if !program.GetStopped() {
		return "", errors.New("program not stopped")
	}

	data := 0
	tdata, err := program.RemoveOutput()
	for err == nil {
		data = tdata
		tdata, err = program.RemoveOutput()
	}
	return strconv.Itoa(data), nil // Return with the answer
}

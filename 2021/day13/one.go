package day13

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

func doFolding(lines []string, all bool) (map[int]map[int]int, error) {
	grid := make(map[int]map[int]int, 0)
	readMode := 0
	for _, val := range lines {
		if val == "" {
			readMode++
			continue
		}
		if readMode == 0 {
			split := strings.Split(val, ",")
			x, err := strconv.Atoi(split[0])
			if err != nil {
				return nil, err
			}
			y, err := strconv.Atoi(split[1])
			if err != nil {
				return nil, err
			}
			tools.GridSet(grid, tools.Pos2D{X: x, Y: y}, 1)
		} else {
			split := strings.Split(val[11:], "=")
			num, err := strconv.Atoi(split[1])
			if err != nil {
				return nil, err
			}
			if split[0] == "y" {
				for x, col := range grid {
					for y, val := range col {
						if y > num && val == 1 {
							delete(grid[x], y)
							tools.GridSet(grid, tools.Pos2D{X: x, Y: num - (y - num)}, 1)
						}
					}
				}
			} else {
				for x, col := range grid {
					if x > num {
						for y, val := range col {
							if val == 1 {
								grid[x][y] = 0
								tools.GridSet(grid, tools.Pos2D{X: num - (x - num), Y: y}, 1)
							}
						}
						delete(grid, x)
					}
				}
			}
			if !all {
				break
			}
		}
	}
	return grid, nil
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	grid, err := doFolding(lines, false)
	if err != nil {
		return "", nil
	}

	count := 0
	for _, col := range grid {
		for _, val := range col {
			if val == 1 {
				count++
			}
		}
	}

	return strconv.Itoa(count), nil // Return with the answer
}

// Package day17 2018
package day17

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "39162"
const testAnswerTwo = "32047"

var testFilename = "../test/private/" + tools.GetPackageName()

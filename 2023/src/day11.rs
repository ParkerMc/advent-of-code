use std::time::Duration;

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn solve(input: &Vec<String>, empty_add: usize) -> usize {
    let chars = input.iter().map(|l| l.chars().collect_vec()).collect_vec();
    let mut jumps_x = vec![];
    let x_len = chars[0].len();

    for x in 0..x_len {
        if !chars.iter().map(|row| row[x]).any(|c| c == '#') {
            // if empty
            jumps_x.push(x);
        }
    }

    let mut galaxies = vec![];

    let mut real_y = 0;
    for row in chars {
        let mut empty = true;
        for (x, c) in row.iter().enumerate() {
            if *c == '#' {
                galaxies.push(Galaxy {
                    x: x + jumps_x.iter().take_while(|j| **j < x).count() * empty_add,
                    y: real_y,
                });
                empty = false;
            }
        }
        if empty {
            real_y += empty_add;
        }
        real_y += 1;
    }

    galaxies
        .iter()
        .enumerate()
        .map(|(i, g)| {
            galaxies[(i + 1)..galaxies.len()]
                .iter()
                .map(|g2| g.dist(g2))
                .sum::<usize>()
        })
        .sum::<usize>()
}

fn part1(input: &Vec<String>) -> String {
    solve(input, 1).to_string()
}

fn part2(input: &Vec<String>) -> String {
    solve(input, 999999).to_string()
}

struct Galaxy {
    x: usize,
    y: usize,
}

impl Galaxy {
    fn dist(&self, other: &Self) -> usize {
        self.x.abs_diff(other.x) + self.y.abs_diff(other.y)
    }
}

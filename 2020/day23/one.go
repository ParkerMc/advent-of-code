package day23

import (
	"container/list"
	"log"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

const debug = false

func getNext(element *list.Element, list *list.List) *list.Element {
	next := element.Next()
	if next == nil {
		return list.Front()
	}
	return next
}

func printCups(cups *list.List, curNum int) {
	line := "cups: "
	curCup := cups.Front()
	for i := 0; curCup != nil; i++ {
		cupVal := curCup.Value.(int)
		if cupVal == curNum {
			line += "(" + strconv.Itoa(cupVal) + ")"
		} else {
			line += " " + strconv.Itoa(cupVal) + " "
		}
		curCup = curCup.Next()
	}
	log.Print(line)
}

func intJoin(arr []int, sep string) string {
	strArr := make([]string, len(arr))
	for i, val := range arr {
		strArr[i] = strconv.Itoa(val)
	}
	return strings.Join(strArr, sep)
}

func load(lines []string, pt2 bool) (*list.List, []*list.Element, int, error) {
	highest := 0
	cups := list.New()
	var cupMap []*list.Element
	if pt2 {
		cupMap = make([]*list.Element, 1000001)
	} else {
		cupMap = make([]*list.Element, 10)
	}
	for _, c := range lines[0] {
		val, err := strconv.Atoi(string(c))
		if err != nil {
			return nil, nil, 0, err
		}
		cupMap[val] = cups.PushBack(val)
		if val > highest {
			highest = val
		}
	}
	return cups, cupMap, highest, nil
}

func solve(cups *list.List, cupMap []*list.Element, highest, moves int) {
	cur := cups.Front()
	curNum := cur.Value.(int)

	for i := 1; i <= moves; i++ {
		if debug {
			log.Printf("-- move %d --", i)
			printCups(cups, curNum)
		}

		pickedUp := make([]int, 3)
		for i := range pickedUp {
			element := getNext(cur, cups)
			cups.Remove(element)
			pickedUp[i] = element.Value.(int)
		}

		if debug {
			log.Printf("pick up: %s", intJoin(pickedUp, ", "))
			log.Printf("destination: %d", cur.Value.(int)-1)
			log.Print()
		}

		destNum := cur.Value.(int)
		for valid := false; !valid; {
			destNum--
			if destNum < 1 {
				destNum = highest
			}
			valid = true
			for _, cup := range pickedUp {
				if cup == destNum {
					valid = false
					break
				}
			}
		}
		dest := cupMap[destNum]

		curAdd := dest
		for _, val := range pickedUp {
			curAdd = cups.InsertAfter(val, curAdd)
			cupMap[val] = curAdd
		}

		cur = getNext(cur, cups)
		curNum = cur.Value.(int)
	}

	if debug {
		log.Print("-- final --")
		printCups(cups, curNum)
	}
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	cups, cupMap, highest, err := load(lines, false)
	if err != nil {
		return "", err
	}

	solve(cups, cupMap, highest, 100)

	out := ""
	curCup := cups.Front()
	for curCup != nil {
		if curCup.Value.(int) == 1 {
			break
		}
		curCup = curCup.Next()
	}

	curCup = getNext(curCup, cups)
	for curCup.Value.(int) != 1 {
		out += strconv.Itoa(curCup.Value.(int))
		curCup = getNext(curCup, cups)
	}

	return out, nil // Return with the answer
}

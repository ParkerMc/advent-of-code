use std::time::Duration;

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    solve(input, solve_pt1)
}

fn part2(input: &Vec<String>) -> String {
    solve(input, solve_pt2)
}

fn solve(
    input: &Vec<String>,
    solver: fn(goal: u64, cur: u64, i: usize, data: &Vec<u64>) -> bool,
) -> String {
    input
        .iter()
        .map(|l| {
            let (v, ns) = l.split(": ").collect_tuple().unwrap();

            (
                v.parse::<u64>().expect("Expected number"),
                ns.split(" ")
                    .map(|s| s.parse::<u64>().expect("Expected number"))
                    .collect_vec(),
            )
        })
        .filter(|(v, n)| solver(*v, n[0], 1, n))
        .map(|(v, _)| v)
        .sum::<u64>()
        .to_string()
}

fn solve_pt1(goal: u64, cur: u64, i: usize, data: &Vec<u64>) -> bool {
    if cur > goal || i == data.len() {
        return cur == goal;
    }
    return solve_pt1(goal, cur + data[i], i + 1, data)
        || solve_pt1(goal, cur * data[i], i + 1, data);
}

fn solve_pt2(goal: u64, cur: u64, i: usize, data: &Vec<u64>) -> bool {
    if cur > goal || i == data.len() {
        return cur == goal;
    }
    return solve_pt2(goal, cur + data[i], i + 1, data)
        || solve_pt2(goal, cur * data[i], i + 1, data)
        || solve_pt2(goal, concat(cur, data[i]), i + 1, data);
}

fn concat(x: u64, y: u64) -> u64 {
    let mut pow = 10;
    while y >= pow {
        pow *= 10;
    }
    return x * pow + y;
}

// Package day11 2020
package day11

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "2108"
const testAnswerTwo = "1897"

var testFilename = "../test/private/" + tools.GetPackageName()

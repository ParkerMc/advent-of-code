// Package day11 2018
package day11

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "1735"
const testAnswerTwo = "400"

var testFilename = "../test/private/" + tools.GetPackageName()

package day08

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

func inSegment(letter rune, segment string) bool {
	for _, r := range segment {
		if r == letter {
			return true
		}
	}

	return false
}

func areSame(m map[rune]bool, m2 map[rune]bool) bool {
	for key, val := range m {
		if val != m2[key] {
			return false
		}
	}
	return true
}

func countTrue(m map[rune]bool) int {
	count := 0
	for _, val := range m {
		if val {
			count++
		}
	}

	return count
}

func getTrues(m map[rune]bool) []rune {
	list := make([]rune, 0)
	for key, val := range m {
		if val {
			list = append(list, key)
		}
	}
	return list
}

func reverseMap(m map[rune]map[rune]bool, str string) string {
	out := ""
	for _, re := range str {
		out += string(getTrues(m[re])[0])
	}
	return out
}

func readSegment(segment string) int {
	length := len(segment)
	if length == 2 {
		return 1
	}
	if length == 3 {
		return 7
	}
	if length == 4 {
		return 4
	}
	if length == 7 {
		return 8
	}
	if length == 5 { // 2, 3, or 5
		if strings.Contains(segment, "b") {
			return 5
		}
		if strings.Contains(segment, "e") {
			return 2
		}
		return 3
	}
	if length == 6 { // 0, 6, 9
		if !strings.Contains(segment, "d") {
			return 0
		}
		if strings.Contains(segment, "e") {
			return 6
		}
		return 9
	}
	return 0
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	answer := 0
	for _, line := range lines {
		split := strings.Split(line, " | ")
		split2 := strings.Split(split[0], " ")

		possibleMaps := make(map[rune]map[rune]bool, 7)
		for _, r := range []rune{'a', 'b', 'c', 'd', 'e', 'f', 'g'} {
			possibleMaps[r] = make(map[rune]bool, 7)
			for _, r2 := range []rune{'a', 'b', 'c', 'd', 'e', 'f', 'g'} {
				possibleMaps[r][r2] = true
			}
		}

		// First loop thorugh get the easy ones
		for _, segment := range split2 {
			length := len(segment)
			if length == 2 { // 1
				for _, r := range []rune{'a', 'b', 'c', 'd', 'e', 'f', 'g'} {
					if inSegment(r, segment) {
						continue
					}
					possibleMaps[r]['c'] = false
					possibleMaps[r]['f'] = false
				}
			} else if length == 3 { // 7
				for _, r := range []rune{'a', 'b', 'c', 'd', 'e', 'f', 'g'} {
					if inSegment(r, segment) {
						continue
					}
					possibleMaps[r]['a'] = false
					possibleMaps[r]['c'] = false
					possibleMaps[r]['f'] = false
				}
			} else if length == 4 { // 4
				for _, r := range []rune{'a', 'b', 'c', 'd', 'e', 'f', 'g'} {
					if inSegment(r, segment) {
						continue
					}
					possibleMaps[r]['b'] = false
					possibleMaps[r]['c'] = false
					possibleMaps[r]['d'] = false
					possibleMaps[r]['f'] = false
				}
			} else if length == 5 { // 2, 3, or 5
				for _, r := range []rune{'a', 'b', 'c', 'd', 'e', 'f', 'g'} {
					if inSegment(r, segment) {
						continue
					}
					possibleMaps[r]['a'] = false
					possibleMaps[r]['d'] = false
					possibleMaps[r]['g'] = false
				}
			} else if length == 6 { // 0, 6, 9
				for _, r := range []rune{'a', 'b', 'c', 'd', 'e', 'f', 'g'} {
					if inSegment(r, segment) {
						continue
					}
					possibleMaps[r]['a'] = false
					possibleMaps[r]['b'] = false
					possibleMaps[r]['f'] = false
					possibleMaps[r]['g'] = false
				}
			}
		}

		changeMade := true
		for changeMade {
			changeMade = false
			for i, m := range possibleMaps {
				trueCount := countTrue(m)
				if trueCount == 1 { // If there is just one possible map set the others to false
					trues := getTrues(m)
					for k := range possibleMaps {
						if i == k {
							continue
						}
						if !changeMade && possibleMaps[k][trues[0]] {
							changeMade = true
						}
						possibleMaps[k][trues[0]] = false
					}
				} else if trueCount == 2 {
					// See if there is another match
					for j, m2 := range possibleMaps {
						if i == j || !areSame(m, m2) {
							continue
						}
						// If there is another match remove those two from the others
						trues := getTrues(m)
						for k := range possibleMaps {
							if i == k || j == k {
								continue
							}
							for _, t := range trues {
								if !changeMade && possibleMaps[k][t] {
									changeMade = true
								}
								possibleMaps[k][t] = false
							}
						}
					}
				}
			}
		}
		out := 0
		split3 := strings.Split(split[1], " ")
		for _, segment := range split3 {
			out *= 10
			out += readSegment(reverseMap(possibleMaps, segment))
		}
		answer += out
	}

	return strconv.Itoa(answer), nil // Return with the answer
}

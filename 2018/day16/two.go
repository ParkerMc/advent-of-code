package day16

import (
	"errors"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

type codeType int
type possCodes []codeType

const (
	typeNil codeType = 0
	addr    codeType = 1
	addi    codeType = 2
	mulr    codeType = 3
	muli    codeType = 4
	banr    codeType = 5
	bani    codeType = 6
	borr    codeType = 7
	bori    codeType = 8
	setr    codeType = 9
	seti    codeType = 10
	gtir    codeType = 11
	gtri    codeType = 12
	gtrr    codeType = 13
	eqir    codeType = 14
	eqri    codeType = 15
	eqrr    codeType = 16
)

func (poss possCodes) removeCode(removeCode codeType) possCodes {
	for i, code := range poss {
		if code == removeCode {
			poss[i] = poss[len(poss)-1]
			return poss[:len(poss)-1]
		}
	}
	return poss
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	possible := make([]possCodes, 0)
	for i := 0; i < 16; i++ {
		possible = append(possible, possCodes{addr, addi, mulr, muli, banr, bani, borr, bori, setr, seti, gtir, gtri, gtrr, eqir, eqri, eqrr})
	}
	for i, line := range lines {
		if strings.Contains(line, "Before") {
			beforeStr := strings.Split(strings.Split(strings.Split(line, "[")[1], "]")[0], ", ")
			before := make([]int, 0)
			for _, numStr := range beforeStr {
				num, err := strconv.Atoi(numStr)
				if err != nil {
					return "", err
				}
				before = append(before, num)
			}
			opcode, err := strconv.Atoi(strings.Split(lines[i+1], " ")[0])
			if err != nil {
				return "", err
			}
			a, err := strconv.Atoi(strings.Split(lines[i+1], " ")[1])
			if err != nil {
				return "", err
			}
			b, err := strconv.Atoi(strings.Split(lines[i+1], " ")[2])
			if err != nil {
				return "", err
			}
			c, err := strconv.Atoi(strings.Split(lines[i+1], " ")[3])
			if err != nil {
				return "", err
			}
			afterStr := strings.Split(strings.Split(strings.Split(lines[i+2], "[")[1], "]")[0], ", ")
			after := make([]int, 0)
			for _, numStr := range afterStr {
				num, err := strconv.Atoi(numStr)
				if err != nil {
					return "", err
				}
				after = append(after, num)
			}

			// addr
			if before[a]+before[b] != after[c] {
				possible[opcode] = possible[opcode].removeCode(addr)
			}
			//addi
			if before[a]+b != after[c] {
				possible[opcode] = possible[opcode].removeCode(addi)
			}

			// mulr
			if before[a]*before[b] != after[c] {
				possible[opcode] = possible[opcode].removeCode(mulr)
			}
			//muli
			if before[a]*b != after[c] {
				possible[opcode] = possible[opcode].removeCode(muli)
			}

			// banr
			if before[a]&before[b] != after[c] {
				possible[opcode] = possible[opcode].removeCode(banr)
			}
			//bani
			if before[a]&b != after[c] {
				possible[opcode] = possible[opcode].removeCode(bani)
			}

			// borr
			if before[a]|before[b] != after[c] {
				possible[opcode] = possible[opcode].removeCode(borr)
			}
			//bori
			if before[a]|b != after[c] {
				possible[opcode] = possible[opcode].removeCode(bori)
			}

			// setr
			if before[a] != after[c] {
				possible[opcode] = possible[opcode].removeCode(setr)
			}
			//seti
			if a != after[c] {
				possible[opcode] = possible[opcode].removeCode(seti)
			}

			//gtir
			if (after[c] == 0 && a > before[b]) || (after[c] == 1 && a <= before[b]) {
				possible[opcode] = possible[opcode].removeCode(gtir)
			}
			//gtri
			if (after[c] == 0 && before[a] > b) || (after[c] == 1 && before[a] <= b) {
				possible[opcode] = possible[opcode].removeCode(gtri)
			}
			//gtrr
			if (after[c] == 0 && before[a] > before[b]) || (after[c] == 1 && before[a] <= before[b]) {
				possible[opcode] = possible[opcode].removeCode(gtrr)
			}

			//eqir
			if (after[c] == 0 && a == before[b]) || (after[c] == 1 && a != before[b]) {
				possible[opcode] = possible[opcode].removeCode(eqir)
			}
			//eqri
			if (after[c] == 0 && before[a] == b) || (after[c] == 1 && before[a] != b) {
				possible[opcode] = possible[opcode].removeCode(eqri)
			}
			//eqrr
			if (after[c] == 0 && before[a] == before[b]) || (after[c] == 1 && before[a] != before[b]) {
				possible[opcode] = possible[opcode].removeCode(eqrr)
			}
		}
	}
	codeConverter := make([]codeType, 16)
	for i := 0; i < 16; i++ {
		for opcode, types := range possible {
			if len(types) == 1 {
				codeConverter[opcode] = types[0]
				for j := range possible {
					possible[j] = possible[j].removeCode(types[0])
				}
			}
		}
	}

	for _, optype := range codeConverter {
		if optype == typeNil {
			return "", errors.New("couldn't find all opcodes")
		}
	}
	registers := make([]int, 4)
	for i, line := range lines {
		if i > 0 && !strings.Contains(lines[i-1], "Before") && len(strings.Split(line, " ")) == 4 {
			opcode, err := strconv.Atoi(strings.Split(line, " ")[0])
			if err != nil {
				return "", err
			}
			a, err := strconv.Atoi(strings.Split(line, " ")[1])
			if err != nil {
				return "", err
			}
			b, err := strconv.Atoi(strings.Split(line, " ")[2])
			if err != nil {
				return "", err
			}
			c, err := strconv.Atoi(strings.Split(line, " ")[3])
			if err != nil {
				return "", err
			}

			if codeConverter[opcode] == addr {
				registers[c] = registers[a] + registers[b]
			}
			if codeConverter[opcode] == addi {
				registers[c] = registers[a] + b
			}

			if codeConverter[opcode] == mulr {
				registers[c] = registers[a] * registers[b]
			}
			if codeConverter[opcode] == muli {
				registers[c] = registers[a] * b
			}

			if codeConverter[opcode] == banr {
				registers[c] = registers[a] & registers[b]
			}
			if codeConverter[opcode] == bani {
				registers[c] = registers[a] & b
			}

			if codeConverter[opcode] == borr {
				registers[c] = registers[a] | registers[b]
			}
			if codeConverter[opcode] == bori {
				registers[c] = registers[a] | b
			}

			if codeConverter[opcode] == setr {
				registers[c] = registers[a]
			}
			if codeConverter[opcode] == seti {
				registers[c] = a
			}

			if codeConverter[opcode] == gtir {
				if a > registers[b] {
					registers[c] = 1
				} else {
					registers[c] = 0
				}
			}
			if codeConverter[opcode] == gtri {
				if registers[a] > b {
					registers[c] = 1
				} else {
					registers[c] = 0
				}
			}
			if codeConverter[opcode] == gtrr {
				if registers[a] > registers[b] {
					registers[c] = 1
				} else {
					registers[c] = 0
				}
			}

			if codeConverter[opcode] == eqir {
				if a == registers[b] {
					registers[c] = 1
				} else {
					registers[c] = 0
				}
			}
			if codeConverter[opcode] == eqri {
				if registers[a] == b {
					registers[c] = 1
				} else {
					registers[c] = 0
				}
			}
			if codeConverter[opcode] == eqrr {
				if registers[a] == registers[b] {
					registers[c] = 1
				} else {
					registers[c] = 0
				}
			}
		}
	}
	return strconv.Itoa(registers[0]), nil // Return with the answer
}

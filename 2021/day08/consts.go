// Package day08 2018
package day08

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "301"
const testAnswerTwo = "908067"

var testFilename = "../test/private/" + tools.GetPackageName()

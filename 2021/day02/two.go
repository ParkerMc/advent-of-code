package day02

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	pos := tools.Pos3D{X: 0, Y: 0, Z: 0}
	for _, line := range lines {
		split := strings.Split(line, " ")
		amount, err := strconv.Atoi(split[1])
		if err != nil {
			return "", err
		}
		switch split[0] {
		case "forward":
			pos.X += amount
			pos.Y += pos.Z * amount
		case "down":
			pos.Z -= amount
		case "up":
			pos.Z += amount
		}
	}

	return strconv.Itoa(pos.X * -pos.Y), nil // Return with the answer
}

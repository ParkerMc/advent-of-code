// Package day04 2018
package day04

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "101194"
const testAnswerTwo = "102095"

var testFilename = "../test/private/" + tools.GetPackageName()

package day15

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	split := strings.Split(lines[0], ",")
	nums := make([]int, len(split))
	for i, strNum := range split {
		nums[i], err = strconv.Atoi(strNum)
		if err != nil {
			return "", err
		}
	}

	legnth := 100000000
	lastI := make([]int, legnth)
	for i, num := range nums[:len(nums)-1] {
		lastI[num] = i + 1
	}
	lastNum := nums[len(nums)-1]

	for i := len(nums); i < 30000000; i++ {
		if val := lastI[lastNum]; val != 0 {
			lastI[lastNum] = i
			lastNum = i - val
		} else {
			lastI[lastNum] = i
			lastNum = 0
		}
	}

	return strconv.Itoa(lastNum), nil // Return with the answer
}

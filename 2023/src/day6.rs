use std::time::Duration;

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn find_ways_to_beat(times: Vec<u64>, dists: Vec<u64>) -> usize {
    times
        .into_iter()
        .zip(dists.into_iter())
        .map(|(t, d)| {
            ((d / t)..t)
                .map(|h| h * (t - h))
                .filter(|d2| *d2 > d)
                .count()
        })
        .reduce(|acc, n| acc * n)
        .expect("Expected one")
}

fn part1(input: &Vec<String>) -> String {
    let (times, dists) = input
        .iter()
        .map(|l| {
            l.split(':')
                .last()
                .expect("Expected last")
                .split(' ')
                .filter(|s| !s.is_empty())
                .map(|s| s.parse::<u64>().expect("Expected number"))
                .collect_vec()
        })
        .collect_tuple()
        .expect("Expected only two lines");

    find_ways_to_beat(times, dists).to_string()
}

fn part2(input: &Vec<String>) -> String {
    let (times, dists) = input
        .iter()
        .map(|l| {
            vec![l
                .split(':')
                .last()
                .expect("Expected last")
                .replace(" ", "")
                .parse::<u64>()
                .expect("Expected number")]
        })
        .collect_tuple()
        .expect("Expected only two lines");

    find_ways_to_beat(times, dists).to_string()
}

package day09

import (
	"sort"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	grid := make([][]int, len(lines[0]))
	for x := 0; x < len(lines[0]); x++ {
		grid[x] = make([]int, len(lines))
	}

	for y, line := range lines {
		for x, val := range line {
			grid[x][y] = int(val - 48)
		}
	}

	lowPoints := make([]tools.Pos2D, 0)
	for x, col := range grid {
		for y, val := range col {
			if (y-1 < 0 || grid[x][y-1] > val) &&
				(y+1 >= len(grid[0]) || grid[x][y+1] > val) &&
				(x-1 < 0 || grid[x-1][y] > val) &&
				(x+1 >= len(grid) || grid[x+1][y] > val) {
				lowPoints = append(lowPoints, tools.Pos2D{X: x, Y: y})
			}
		}
	}

	top3 := make([]int, 3)
	for _, point := range lowPoints {
		basinGrid := make([][]bool, len(grid))
		for x := 0; x < len(grid); x++ {
			basinGrid[x] = make([]bool, len(grid[0]))
		}

		size := 0
		toSearch := []tools.Pos2D{point}

		for len(toSearch) > 0 {
			pos := toSearch[0]
			toSearch = toSearch[1:]
			if basinGrid[pos.X][pos.Y] {
				continue
			}
			size++
			basinGrid[pos.X][pos.Y] = true
			if pos.Y-1 >= 0 && grid[pos.X][pos.Y-1] != 9 && !basinGrid[pos.X][pos.Y-1] {
				toSearch = append(toSearch, pos.Add(tools.Pos2D{X: 0, Y: -1}))
			}
			if pos.Y+1 < len(grid[0]) && grid[pos.X][pos.Y+1] != 9 && !basinGrid[pos.X][pos.Y+1] {
				toSearch = append(toSearch, pos.Add(tools.Pos2D{X: 0, Y: 1}))
			}
			if pos.X-1 >= 0 && grid[pos.X-1][pos.Y] != 9 && !basinGrid[pos.X-1][pos.Y] {
				toSearch = append(toSearch, pos.Add(tools.Pos2D{X: -1, Y: 0}))
			}
			if pos.X+1 < len(grid) && grid[pos.X+1][pos.Y] != 9 && !basinGrid[pos.X+1][pos.Y] {
				toSearch = append(toSearch, pos.Add(tools.Pos2D{X: 1, Y: 0}))
			}
		}
		if size > top3[0] {
			top3 = append(top3[1:], size)
			sort.Ints(top3)
		}
	}

	return strconv.Itoa(top3[0] * top3[1] * top3[2]), nil // Return with the answer
}

use std::{collections::HashMap, time::Duration};

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    solve(input, false)
}

fn part2(input: &Vec<String>) -> String {
    solve(input, true)
}

fn solve(input: &Vec<String>, full_total: bool) -> String {
    let map = input
        .iter()
        .map(|l| {
            l.chars()
                .map(|c| c.to_string().parse::<u32>().unwrap())
                .collect_vec()
        })
        .collect_vec();
    let y_len = map.len();
    let x_len = map[0].len();
    let trail_heads = map.iter().enumerate().flat_map(|(y, r)| {
        r.iter()
            .enumerate()
            .filter(|(_, v)| **v == 0)
            .map(|(x, _)| (y, x))
            .collect_vec()
    });

    trail_heads
        .map(|th| {
            let mut ends = HashMap::new();
            let mut queue = vec![th];
            while let Some(cur_pos) = queue.pop() {
                let looking_value = map[cur_pos.0][cur_pos.1] + 1;
                if looking_value == 10 {
                    *ends.entry(cur_pos).or_insert(0) += 1;
                    continue;
                }
                // y-1
                if cur_pos.0 > 0 && map[cur_pos.0 - 1][cur_pos.1] == looking_value {
                    queue.push((cur_pos.0 - 1, cur_pos.1));
                }

                // y
                if cur_pos.1 > 0 && map[cur_pos.0][cur_pos.1 - 1] == looking_value {
                    queue.push((cur_pos.0, cur_pos.1 - 1));
                }
                if cur_pos.1 < x_len - 1 && map[cur_pos.0][cur_pos.1 + 1] == looking_value {
                    queue.push((cur_pos.0, cur_pos.1 + 1));
                }
                // y+1
                if cur_pos.0 < y_len - 1 && map[cur_pos.0 + 1][cur_pos.1] == looking_value {
                    queue.push((cur_pos.0 + 1, cur_pos.1));
                }
            }
            if full_total {
                ends.values().sum()
            } else {
                ends.len()
            }
        })
        .sum::<usize>()
        .to_string()
}

package day13

import (
	"errors"
	"strconv"
	"time"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

// Slows the program down and displays the game
const progress = false

// func print(area map[int]map[int]int, score int) {
// 	tools.ClearOutput()
// 	minX, maxX, minY, maxY := tools.GridMinMax(area)
// 	for y := minY; y < maxY; y++ {
// 		for x := minX; x < maxX; x++ {
// 			if row, ok := area[x]; ok {
// 				switch row[y] {
// 				case empty:
// 					fmt.Print(" ")
// 				case wall:
// 					fmt.Print("█")
// 				case block:
// 					fmt.Print("#")
// 				case paddle:
// 					fmt.Print("_")
// 				case ball:
// 					fmt.Print(".")

// 				}
// 			} else {
// 				fmt.Print(" ")
// 			}
// 		}
// 		fmt.Println("█")
// 	}
// 	fmt.Print(" Score: ")
// 	fmt.Println(score)
// 	fmt.Println()
// 	fmt.Println()
// }

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	p, err := tools.NewProgram(lines[0])
	if err != nil {
		return "", err
	}
	p.SetData(0, 2)

	bcount := 0
	score := -1
	ballPos := tools.Pos2D{X: -1, Y: -1}
	paddlePos := tools.Pos2D{X: -1, Y: -1}
	area := make(map[int]map[int]int, 0)

	err = p.Run()
	if err != nil {
		return "", err
	}

	onePlaced := false
	for bcount > 0 || !onePlaced {
		if progress {
			time.Sleep(time.Second / 40)
		}
		for done := false; !done; {
			x, err := p.RemoveOutput()
			if err != nil {
				done = true
			} else {
				y, err := p.RemoveOutput()
				if err != nil {
					return "", err
				}
				val, err := p.RemoveOutput()
				if err != nil {
					return "", err
				}
				if x == -1 {
					score = val
				} else {
					if _, ok := area[x]; !ok {
						area[x] = make(map[int]int, 0)
					}
					if val == ball {
						ballPos = tools.Pos2D{X: x, Y: y}
					}
					if val == paddle {
						paddlePos = tools.Pos2D{X: x, Y: y}
					}
					if val == block {
						bcount++
						onePlaced = true
					}
					if area[x][y] == block {
						bcount--
					}
					area[x][y] = val
				}
			}
		}
		if !p.GetStopped() {
			if paddlePos.X > ballPos.X {
				p.AddInput(-1)
			} else if paddlePos.X < ballPos.X {
				p.AddInput(1)
			} else {
				p.AddInput(0)
			}
		} else if bcount > 0 {
			return "", errors.New("program ended early")
		}

		if progress {
			print(area, score)
		}

		err := p.Run()
		if err != nil {
			return "", err
		}
	}
	return strconv.Itoa(score), nil // Will never get here
}

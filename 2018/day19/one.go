package day19

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

func run(filename string, registerStart []int) (string, error) {
	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	ipI, err := strconv.Atoi(strings.TrimLeft(lines[0], "#ip "))
	if err != nil {
		return "", err
	}
	ip := 0
	registers := make([]int, 6)
	pastRegs := make([][]int, 0)
	copy(registers, registerStart)
	registers[ipI] = ip
	for time := 1; true; time++ {
		opcode := strings.Split(lines[ip+1], " ")[0]
		a, err := strconv.Atoi(strings.Split(lines[ip+1], " ")[1])
		if err != nil {
			return "", err
		}

		b, err := strconv.Atoi(strings.Split(lines[ip+1], " ")[2])
		if err != nil {
			return "", err
		}

		c, err := strconv.Atoi(strings.Split(lines[ip+1], " ")[3])
		if err != nil {
			return "", err
		}

		if opcode == "addr" {
			registers[c] = registers[a] + registers[b]
		} else if opcode == "addi" {
			registers[c] = registers[a] + b
		} else if opcode == "mulr" {
			registers[c] = registers[a] * registers[b]
		} else if opcode == "muli" {
			registers[c] = registers[a] * b
		} else if opcode == "banr" {
			registers[c] = registers[a] & registers[b]
		} else if opcode == "bani" {
			registers[c] = registers[a] & b
		} else if opcode == "borr" {
			registers[c] = registers[a] | registers[b]
		} else if opcode == "bori" {
			registers[c] = registers[a] | b
		} else if opcode == "seti" {
			registers[c] = a
		} else if opcode == "setr" {
			registers[c] = registers[a]
		} else if opcode == "gtir" {
			if a > registers[b] {
				registers[c] = 1
			} else {
				registers[c] = 0
			}
		} else if opcode == "gtri" {
			if registers[a] > b {
				registers[c] = 1
			} else {
				registers[c] = 0
			}
		} else if opcode == "gtrr" {
			if registers[a] > registers[b] {
				registers[c] = 1
			} else {
				registers[c] = 0
			}
		} else if opcode == "eqir" {
			if a == registers[b] {
				registers[c] = 1
			} else {
				registers[c] = 0
			}
		} else if opcode == "eqri" {
			if registers[a] == b {
				registers[c] = 1
			} else {
				registers[c] = 0
			}
		} else if opcode == "eqrr" {
			if registers[a] == registers[b] {
				registers[c] = 1
			} else {
				registers[c] = 0
			}
		} else {
			return "", fmt.Errorf("%s is not programed in", opcode)
		}
		registers[ipI]++
		ip = registers[ipI]

		if ip > len(lines)-2 {
			return strconv.Itoa(registers[0]), nil
		}

		add := true
		for _, testReg := range pastRegs {
			if testReg[0] == ip {
				answer := 0
				for x := 1; x < registers[5]+1; x++ {
					if registers[5]%x == 0 {
						answer += x
					}
				}
				add = false
				return strconv.Itoa(answer), nil
			}
		}
		if add {
			pastRegs = append(pastRegs, append([]int{ip}, registers...))
		}
	}
	return "", nil // Return with the answer
}

// One the first puzzle for the day
func One(filename string) (string, error) {
	return run(filename, nil)
}

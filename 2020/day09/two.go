package day09

import (
	"errors"
	"sort"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	nums := make([]int, len(lines))
	for i, str := range lines {
		nums[i], err = strconv.Atoi(str)
		if err != nil {
			return "", err
		}
	}

	invaidNum, err := findInvalid(nums)
	if err != nil {
		return "", err
	}

	runningSum := 0
	firstSum := 0
	for i, addNum := range nums {
		runningSum += addNum
		for runningSum > invaidNum {
			runningSum -= nums[firstSum]
			firstSum++
		}
		if runningSum == invaidNum {
			usedNums := nums[firstSum : i+1]
			sort.Ints(usedNums)
			return strconv.Itoa(usedNums[0] + usedNums[len(usedNums)-1]), nil
		}
	}

	return "", errors.New("invalid data") // Show never get here
}

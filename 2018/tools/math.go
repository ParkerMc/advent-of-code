package tools

// Abs returns the absoulute value
func Abs(num int) int {
	if num < 0 {
		return -num
	}
	return num
}

// Gcd finds the greatest common factor
func Gcd(num, num2 int) int {
	for num2 != 0 {
		t := num2
		num2 = num % num2
		num = t
	}
	return num
}

// Lcm returns the least commone multiple
func Lcm(num, num2 int) int {
	return num * num2 / Gcd(num, num2)
}

// Min gets the min of two number
func Min(num, num2 int) int {
	if num < num2 {
		return num
	}
	return num2
}

// Max gets the max of two numbers
func Max(num, num2 int) int {
	if num > num2 {
		return num
	}
	return num2
}

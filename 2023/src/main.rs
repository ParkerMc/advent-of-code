use std::{
    collections::BTreeMap,
    env,
    path::Path,
    process::{self, exit, ExitCode},
    time::Duration,
};

#[macro_use]
extern crate rulinalg;

mod day1;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day18;
mod day19;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;
mod utils;

fn create_test_map() -> BTreeMap<u8, fn(Option<u8>, &Vec<String>) -> Duration> {
    let mut test_map: BTreeMap<u8, fn(Option<u8>, &Vec<String>) -> Duration> = BTreeMap::new();
    test_map.insert(1, day1::run);
    test_map.insert(2, day2::run);
    test_map.insert(3, day3::run);
    test_map.insert(4, day4::run);
    test_map.insert(5, day5::run);
    test_map.insert(6, day6::run);
    test_map.insert(7, day7::run);
    test_map.insert(8, day8::run);
    test_map.insert(9, day9::run);
    test_map.insert(10, day10::run);
    test_map.insert(11, day11::run);
    test_map.insert(12, day12::run);
    test_map.insert(13, day13::run);
    test_map.insert(14, day14::run);
    test_map.insert(15, day15::run);
    test_map.insert(16, day16::run);
    test_map.insert(17, day17::run);
    test_map.insert(18, day18::run);
    test_map.insert(19, day19::run);
    test_map
}

fn main() -> ExitCode {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 || args.iter().any(|f| f == "-h" || f == "--help") {
        eprintln!("Useage: aoc2022 input_file_or_folder [day_number] [part_number]\n");
        eprintln!("input_file_or_folder: must be a text file with any name or a folder with files named in the following format \"day#\"");
        eprintln!("E.x. \"day5\" or \"day14\"\n");
        eprintln!("day_number: is an optional argument to speifiy what day(1-25) should be ran\n");
        eprintln!("part_number: is an optional argument to speifiy what part(1-2) should be ran");
        return ExitCode::FAILURE;
    }

    let file_path = &args[1];
    let day = args.get(2).map(|a| match a.parse::<u8>() {
        Ok(n) if n < 26 && n > 0 => n,
        _ => {
            eprintln!("Day argument should be a number between 1 and 25 inclusive");
            process::exit(1);
        }
    });
    let part = args.get(3).map(|a| match a.parse::<u8>() {
        Ok(n) if n < 3 && n > 0 => n,
        _ => {
            eprintln!("Part argument should be a number between 1 and 2 inclusive");
            process::exit(1);
        }
    });

    run(day, part, file_path);
    return ExitCode::SUCCESS;
}

fn run(day: Option<u8>, part: Option<u8>, file_path: &str) {
    let test_map = create_test_map();

    match day {
        Some(d) => {
            match test_map.get(&d) {
                Some(runner) => runner(part, &get_test_input(d, file_path)),
                None => {
                    println!("Day {} does not exist.", d);
                    exit(1);
                }
            };
        }
        None => {
            let duration: Duration = test_map
                .iter()
                .map(|(day, runner)| runner(None, &get_test_input(*day, file_path)))
                .sum();
            println!(
                "\nTotal Duration: {:.2} ms",
                duration.as_micros() as f64 / 1000.0
            );
        }
    }
}

fn get_test_input(day: u8, file_path: &str) -> Vec<String> {
    let path = Path::new(file_path);
    if path.is_file() {
        return utils::io::read_input(file_path);
    }
    let in_folder_path = path.join(String::from("day") + &day.to_string());
    utils::io::read_input(in_folder_path.to_str().expect("Failed to join path"))
}

package day23

import (
	"log"
	"math"
	"sort"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

type weightedRange struct {
	weight int
	min    tools.Pos3D
	max    tools.Pos3D
}

func rangeMDist(minPos tools.Pos3D, maxPos tools.Pos3D, pos tools.Pos3D) int {
	dist := 0
	if pos.X < minPos.X {
		dist += minPos.X - pos.X
	} else if pos.X > maxPos.X {
		dist += pos.X - maxPos.X
	}
	if pos.Y < minPos.Y {
		dist += minPos.Y - pos.Y
	} else if pos.Y > maxPos.Y {
		dist += pos.Y - maxPos.Y
	}
	if pos.Z < minPos.Z {
		dist += minPos.Z - pos.Z
	} else if pos.Z > maxPos.Z {
		dist += pos.Z - maxPos.Z
	}
	return dist
}

func search(minPos tools.Pos3D, maxPos tools.Pos3D, nanobots []*nanobot, maxC int) (tools.Pos3D, int) {
	size := maxPos.Subtract(minPos)
	middle := minPos.Add(tools.Pos3D{X: size.X / 2, Y: size.Y / 2, Z: size.Z / 2})
	boxes := make([]weightedRange, 8)
	i := 0
	for _, x := range [][]int{{minPos.X, middle.X}, {middle.X + 1, maxPos.X}} {
		for _, y := range [][]int{{minPos.Y, middle.Y}, {middle.Y + 1, maxPos.Y}} {
		ZLOOP:
			for _, z := range [][]int{{minPos.Z, middle.Z}, {middle.Z + 1, maxPos.Z}} {
				minV := tools.Pos3D{X: x[0], Y: y[0], Z: z[0]}
				maxV := tools.Pos3D{X: x[1], Y: y[1], Z: z[1]}
				for _, box := range boxes {
					if box.min.X == minV.X && box.min.Y == minV.Y && box.min.Z == minV.Z &&
						box.max.X == minV.X && box.max.Y == maxV.Y && box.max.Z == maxV.Z {
						break ZLOOP
					}
				}
				count := 0
				for _, bot := range nanobots {
					dist := rangeMDist(minV, maxV, bot.pos)
					if dist <= bot.rangen {
						count++
					}
				}
				boxes[i] = weightedRange{weight: count, min: minV, max: maxV}
				i++
			}
		}
	}

	if minPos.MDist(&maxPos) == 0 {
		return minPos, boxes[0].weight
	}
	sort.Slice(boxes, func(i, j int) bool {
		return boxes[i].weight > boxes[j].weight
	})
	mostBots := 0
	var mostPos tools.Pos3D
	for _, box := range boxes {
		maxCC := max(maxC, mostBots)
		if maxCC >= box.weight {
			break
		}
		pos, subCount := search(box.min, box.max, nanobots, max(mostBots, maxCC))
		if subCount > mostBots {
			mostBots = subCount
			mostPos = pos
		}
	}

	return mostPos, mostBots
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {
	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	nanobots, err := loadData(lines)
	if err != nil {
		return "", err
	}

	minPos := tools.Pos3D{X: math.MaxInt, Y: math.MaxInt, Z: math.MaxInt}
	maxPos := tools.Pos3D{X: math.MinInt, Y: math.MinInt, Z: math.MinInt}
	for _, bot := range nanobots {
		if bot.pos.X < minPos.X {
			minPos.X = bot.pos.X
		}
		if bot.pos.Y < minPos.Y {
			minPos.Y = bot.pos.Y
		}
		if bot.pos.Z < minPos.Z {
			minPos.Z = bot.pos.Z
		}
		if bot.pos.X > maxPos.X {
			maxPos.X = bot.pos.X
		}
		if bot.pos.Y > maxPos.Y {
			maxPos.Y = bot.pos.Y
		}
		if bot.pos.Z > maxPos.Z {
			maxPos.Z = bot.pos.Z
		}
	}

	pos, c := search(minPos, maxPos, nanobots, 0)
	log.Print(c)

	return strconv.Itoa((&tools.Pos3D{X: 0, Y: 0, Z: 0}).MDist(&pos)), nil
}

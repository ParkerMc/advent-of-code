package day20

import (
	"log"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

func printGrid(grid [][]bool) {
	for y := 0; y < len(grid); y++ {
		line := ""
		for _, col := range grid {
			val := col[y]
			if val {
				line += "#"
			} else {
				line += "."
			}
		}
		log.Print(line)
	}
}

func solve(lines []string, steps int) int {
	key := lines[0]
	area := make([][]bool, len(lines[2]))
	for x := range area {
		area[x] = make([]bool, len(lines)-2)
	}
	for y, row := range lines[2:] {
		for x, val := range row {
			if val == '#' {
				area[x][y] = true
			}
		}
	}

	for step := 0; step < steps; step++ {
		// is expanded out by one each direction each time
		newArea := make([][]bool, len(area)+2)
		for x := range newArea {
			col := make([]bool, len(area[0])+2)
			newArea[x] = col
			oldX := x - 1
			var oldColLeft []bool
			var oldCol []bool
			var oldColRight []bool
			if oldX > 0 {
				oldColLeft = area[oldX-1]
			}
			if oldX >= 0 && oldX < len(area) {
				oldCol = area[oldX]
			}
			if oldX >= -1 && oldX+1 < len(area) {
				oldColRight = area[oldX+1]
			}

			for y := range col {
				oldY := y - 1
				// Get value
				i := 0
				// Read row above
				if oldY > 0 {
					if (oldColLeft != nil && oldColLeft[oldY-1]) || (oldColLeft == nil && step%2 == 1) {
						i++
					}

					i <<= 1
					if (oldCol != nil && oldCol[oldY-1]) || (oldCol == nil && step%2 == 1) {
						i++
					}

					i <<= 1
					if (oldColRight != nil && oldColRight[oldY-1]) || (oldColRight == nil && step%2 == 1) {
						i++
					}
				} else if step%2 == 1 {
					i += 7
				}

				// Read current row
				if oldY >= 0 && oldY < len(area[0]) {
					i <<= 1
					if (oldColLeft != nil && oldColLeft[oldY]) || (oldColLeft == nil && step%2 == 1) {
						i++
					}

					i <<= 1
					if (oldCol != nil && oldCol[oldY]) || (oldCol == nil && step%2 == 1) {
						i++
					}

					i <<= 1
					if (oldColRight != nil && oldColRight[oldY]) || (oldColRight == nil && step%2 == 1) {
						i++
					}
				} else {
					i <<= 3
					if step%2 == 1 {
						i += 7
					}
				}

				// Read row below
				if oldY >= -1 && oldY+1 < len(area[0]) {
					i <<= 1
					if (oldColLeft != nil && oldColLeft[oldY+1]) || (oldColLeft == nil && step%2 == 1) {
						i++
					}

					i <<= 1
					if (oldCol != nil && oldCol[oldY+1]) || (oldCol == nil && step%2 == 1) {
						i++
					}

					i <<= 1
					if (oldColRight != nil && oldColRight[oldY+1]) || (oldColRight == nil && step%2 == 1) {
						i++
					}
				} else {
					i <<= 3
					if step%2 == 1 {
						i += 7
					}
				}
				col[y] = key[i] == '#'
			}
		}
		area = newArea
	}

	answer := 0
	for _, col := range area {
		for _, val := range col {
			if val {
				answer++
			}
		}
	}
	return answer
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	answer := solve(lines, 2)

	return strconv.Itoa(answer), nil // Return with the answer
}

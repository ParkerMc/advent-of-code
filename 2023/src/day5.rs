use std::time::Duration;

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    let maps = input
        .split(|l| l.is_empty())
        .skip(1)
        .map(|g| {
            g.iter()
                .skip(1)
                .map(|l| {
                    let split = l.split(" ").collect_vec();
                    let start = split[1].parse().expect("Expected start to be a number");
                    let len = split[2]
                        .parse::<u64>()
                        .expect("Expected len to be a number");
                    Range {
                        dest: split[0].parse().expect("Exected dest to be a number"),
                        start: start,
                        end: start + len,
                        len: len,
                    }
                })
                .collect_vec()
        })
        .collect_vec();

    let seeds: Vec<u64> = input[0]
        .split(": ")
        .last()
        .expect("Expected last")
        .split(" ")
        .map(|n| n.parse().expect("Expected seed number"))
        .collect_vec();

    seeds
        .iter()
        .map(|s| {
            maps.iter().fold(s.clone(), |x, map| {
                map.iter()
                    .find(|r| r.start < x && r.end > x)
                    .map(|r| x - r.start + r.dest)
                    .unwrap_or(x)
            })
        })
        .min()
        .expect("Expected a minimum")
        .to_string()
}

fn part2(input: &Vec<String>) -> String {
    let maps = input
        .split(|l| l.is_empty())
        .skip(1)
        .map(|g| {
            g.iter()
                .skip(1)
                .map(|l| {
                    let split = l.split(" ").collect_vec();
                    let start = split[1].parse().expect("Expected start to be a number");
                    let len = split[2]
                        .parse::<u64>()
                        .expect("Expected len to be a number");
                    Range {
                        dest: split[0].parse().expect("Exected dest to be a number"),
                        start: start,
                        end: start + len,
                        len: len,
                    }
                })
                .collect_vec()
        })
        .collect_vec();
    let raw_seeds = input[0]
        .split(": ")
        .last()
        .expect("Expected last")
        .split(" ")
        .map(|n| n.parse::<u64>().expect("Expected seed number"))
        .collect_vec();
    let seed_chunks = raw_seeds.chunks(2);
    let mut states = seed_chunks
        .map(|n: &[u64]| SeedRange {
            start: n[0],
            end: n[1] + n[0],
        })
        .collect_vec();

    for map in maps.iter() {
        let mut i = 0;
        while i < states.len() {
            let s = SeedRange {
                start: states[i].start,
                end: states[i].end,
            };

            for r in map {
                if r.start <= s.start && r.end >= s.end {
                    states[i] = SeedRange {
                        start: s.start - r.start + r.dest,
                        end: s.end - r.start + r.dest,
                    };
                    break;
                }

                if r.start > s.start && r.end < s.end {
                    states.push(SeedRange {
                        start: s.start,
                        end: r.start,
                    });
                    states.push(SeedRange {
                        start: r.end,
                        end: s.end,
                    });
                    states[i] = SeedRange {
                        start: r.dest,
                        end: r.dest + r.len,
                    };
                    break;
                }

                if r.start <= s.start && r.end > s.start {
                    states.push(SeedRange {
                        start: r.end,
                        end: s.end,
                    });
                    states[i] = SeedRange {
                        start: s.start - r.start + r.dest,
                        end: r.len + r.dest,
                    };
                    break;
                }

                if r.end >= s.end && r.start < s.end {
                    states.push(SeedRange {
                        start: s.start,
                        end: r.start,
                    });
                    states[i] = SeedRange {
                        start: r.dest,
                        end: s.end - r.start + r.dest,
                    };
                    break;
                }
            }

            i += 1;
        }
    }

    states
        .into_iter()
        .map(|s| s.start)
        .min()
        .expect("Expected a minimum")
        .to_string()
}

struct Range {
    dest: u64,
    start: u64,
    end: u64,
    len: u64,
}

struct SeedRange {
    start: u64,
    end: u64,
}

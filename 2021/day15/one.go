package day15

import (
	"container/heap"
	"errors"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

type queueEntry struct {
	pos tools.Pos2D
	val int
}

func solve(grid [][]int, goal tools.Pos2D) (int, error) {
	outGrid := make([][]int, goal.X+1)
	for x := range outGrid {
		outGrid[x] = make([]int, goal.Y+1)
	}

	queue := make(tools.PriorityQueue, 0)
	heap.Init(&queue)
	heap.Push(&queue, &tools.PriorityQueueItem{
		Value:    queueEntry{pos: tools.Pos2D{X: 0, Y: 0}, val: 0},
		Priority: 0,
	})
	outGrid[0][0] = 1
	for queue.Len() > 0 {
		item := heap.Pop(&queue).(*tools.PriorityQueueItem).Value.(queueEntry)
		outGrid[item.pos.X][item.pos.Y] = item.val
		if item.pos.X == goal.X && item.pos.Y == goal.Y {
			return item.val, nil
		}
		for _, posAdd := range []tools.Pos2D{{X: 1, Y: 0}, {X: -1, Y: 0}, {X: 0, Y: 1}, {X: 0, Y: -1}} {
			nextPos := item.pos.Add(posAdd)
			if nextPos.X < 0 || nextPos.Y < 0 || nextPos.X >= len(grid) || nextPos.Y >= len(grid[0]) || outGrid[nextPos.X][nextPos.Y] != 0 {
				continue
			}
			newVal := item.val + grid[nextPos.X][nextPos.Y]
			heap.Push(&queue, &tools.PriorityQueueItem{
				Value:    queueEntry{pos: nextPos, val: newVal},
				Priority: newVal + goal.MDist(&nextPos),
			})
		}
	}
	return 0, errors.New("queue ran out")
}

// One the first puzzle for the day
func One(filename string) (string, error) {
	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	grid := make([][]int, len(lines[0]))

	for x := range grid {
		grid[x] = make([]int, len(lines))
	}
	for y, row := range lines {
		for x, val := range row {
			grid[x][y] = int(val) - 48
		}
	}
	goal := tools.Pos2D{X: len(grid) - 1, Y: len(grid[0]) - 1}

	answer, err := solve(grid, goal)
	if err != nil {
		return "", err
	}

	return strconv.Itoa(answer), nil // return answer
}

use std::{
    cmp,
    collections::{BinaryHeap, HashSet},
    time::Duration,
    u32,
};

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    let map = input.iter().map(|l| l.chars().collect_vec()).collect_vec();
    let end = find_char(&map, 'E');
    let seen_map = solve(map);
    let end_cell = &seen_map[end.0][end.1];
    end_cell[0]
        .min(end_cell[1])
        .min(end_cell[2])
        .min(end_cell[3])
        .to_string()
}

fn part2(input: &Vec<String>) -> String {
    let map = input.iter().map(|l| l.chars().collect_vec()).collect_vec();
    let start = find_char(&map, 'S');

    let end = find_char(&map, 'E');
    let mut out_map = map.iter().map(|r| vec![false; r.len()]).collect_vec();
    let seen_map = solve(map);
    let end_cell = &seen_map[end.0][end.1];
    let min = end_cell[0]
        .min(end_cell[1])
        .min(end_cell[2])
        .min(end_cell[3]);
    let mut queue: HashSet<_> = end_cell
        .into_iter()
        .enumerate()
        .filter(|(_, v)| **v == min)
        .map(|(d, v)| ((end.0, end.1), Dir::from_index(d), *v))
        .collect();

    while let Some(entry) = queue.iter().next().cloned() {
        queue.remove(&entry);
        let (pos, d, s) = entry;
        out_map[pos.0][pos.1] = true;
        if pos == start {
            continue;
        }
        let backstep_pos = d.back_step(pos);
        let next_score = seen_map[backstep_pos.0][backstep_pos.1][d.index()];
        if next_score < s {
            //same as <= s-1
            queue.insert((backstep_pos, d, next_score));
        }

        if s < 1000 {
            continue;
        }
        let next_dir = d.rotate_c();
        let next_score = seen_map[pos.0][pos.1][next_dir.index()];
        if next_score <= s - 1000 {
            queue.insert((pos, next_dir, next_score));
        }
        let next_dir = d.rotate_cc();
        let next_score = seen_map[pos.0][pos.1][next_dir.index()];
        if next_score <= s - 1000 {
            queue.insert((pos, next_dir, next_score));
        }
        let next_dir = d.rotate_180();
        let next_score = seen_map[pos.0][pos.1][next_dir.index()];
        if s >= 2000 && next_score <= s - 2000 {
            queue.insert((pos, next_dir, next_score));
        }
    }

    out_map
        .into_iter()
        .flat_map(|r| r)
        .filter(|b| *b)
        .count()
        .to_string()
}

fn solve(map: Vec<Vec<char>>) -> Vec<Vec<Vec<u32>>> {
    let mut seen_map = map
        .iter()
        .map(|r| r.iter().map(|_| vec![u32::MAX; 4]).collect_vec())
        .collect_vec();
    let start = find_char(&map, 'S');

    let mut queue = BinaryHeap::new();
    if let Some(step_pos) = Dir::E.should_step(start, &map, &seen_map, 0) {
        queue.push(cmp::Reverse(Entry {
            score: 1,
            pos: step_pos,
            facing: Dir::E,
        }));
        seen_map[start.0][start.1][Dir::E.index()] = 0;
        seen_map[step_pos.0][step_pos.1][Dir::E.index()] = 1;
    }
    if let Some(step_pos) = Dir::N.should_step(start, &map, &seen_map, 1000) {
        queue.push(cmp::Reverse(Entry {
            score: 1001,
            pos: step_pos,
            facing: Dir::N,
        }));
        seen_map[start.0][start.1][Dir::N.index()] = 1000;
        seen_map[step_pos.0][step_pos.1][Dir::N.index()] = 1001;
    }
    if let Some(step_pos) = Dir::S.should_step(start, &map, &seen_map, 1000) {
        queue.push(cmp::Reverse(Entry {
            score: 1001,
            pos: step_pos,
            facing: Dir::S,
        }));
        seen_map[start.0][start.1][Dir::S.index()] = 1000;
        seen_map[step_pos.0][step_pos.1][Dir::S.index()] = 1001;
    }

    if let Some(step_pos) = Dir::W.should_step(start, &map, &seen_map, 2000) {
        queue.push(cmp::Reverse(Entry {
            score: 2001,
            pos: step_pos,
            facing: Dir::W,
        }));
        seen_map[start.0][start.1][Dir::W.index()] = 2000;
        seen_map[step_pos.0][step_pos.1][Dir::W.index()] = 2001;
    }

    while let Some(entry) = queue.pop() {
        let entry = entry.0;
        if map[entry.pos.0][entry.pos.1] == 'E' {
            return seen_map;
        }
        let dir = entry.facing.clone();
        if let Some(step_pos) = dir.should_step(entry.pos, &map, &seen_map, entry.score) {
            seen_map[step_pos.0][step_pos.1][dir.index()] = entry.score + 1;
            queue.push(cmp::Reverse(Entry {
                score: entry.score + 1,
                pos: step_pos,
                facing: dir,
            }));
        }
        let dir = entry.facing.rotate_c();
        if let Some(step_pos) = dir.should_step(entry.pos, &map, &seen_map, entry.score + 1000) {
            seen_map[entry.pos.0][entry.pos.1][dir.index()] = entry.score + 1000;
            seen_map[step_pos.0][step_pos.1][dir.index()] = entry.score + 1001;
            queue.push(cmp::Reverse(Entry {
                score: entry.score + 1001,
                pos: step_pos,
                facing: dir,
            }));
        }
        let dir = entry.facing.rotate_cc();
        if let Some(step_pos) = dir.should_step(entry.pos, &map, &seen_map, entry.score + 1000) {
            seen_map[entry.pos.0][entry.pos.1][dir.index()] = entry.score + 1000;
            seen_map[step_pos.0][step_pos.1][dir.index()] = entry.score + 1001;
            queue.push(cmp::Reverse(Entry {
                score: entry.score + 1001,
                pos: step_pos,
                facing: dir,
            }));
        }
        let dir = entry.facing.rotate_180();
        if let Some(step_pos) = dir.should_step(entry.pos, &map, &seen_map, entry.score + 2000) {
            seen_map[entry.pos.0][entry.pos.1][dir.index()] = entry.score + 2000;
            seen_map[step_pos.0][step_pos.1][dir.index()] = entry.score + 2001;
            queue.push(cmp::Reverse(Entry {
                score: entry.score + 2001,
                pos: step_pos,
                facing: dir,
            }));
        }
    }
    panic!("Not found")
}

fn find_char(map: &Vec<Vec<char>>, ch: char) -> (usize, usize) {
    map.iter()
        .enumerate()
        .filter_map(|(y, r)| {
            r.iter()
                .enumerate()
                .filter(|(_, c)| **c == ch)
                .map(|(x, _)| (y, x))
                .next()
        })
        .next()
        .unwrap()
}

#[derive(PartialEq, Eq, Clone, Copy, Hash)]
enum Dir {
    N,
    S,
    E,
    W,
}
impl Dir {
    fn from_index(i: usize) -> Self {
        match i {
            0 => Dir::N,
            1 => Dir::E,
            2 => Dir::S,
            3 => Dir::W,
            _ => panic!("Unknown index for dir {}", i),
        }
    }

    fn index(&self) -> usize {
        match self {
            Dir::N => 0,
            Dir::E => 1,
            Dir::S => 2,
            Dir::W => 3,
        }
    }

    fn step(&self, pos: (usize, usize)) -> (usize, usize) {
        match self {
            Dir::N => (pos.0 - 1, pos.1),
            Dir::E => (pos.0, pos.1 + 1),
            Dir::S => (pos.0 + 1, pos.1),
            Dir::W => (pos.0, pos.1 - 1),
        }
    }

    fn back_step(&self, pos: (usize, usize)) -> (usize, usize) {
        match self {
            Dir::N => (pos.0 + 1, pos.1),
            Dir::E => (pos.0, pos.1 - 1),
            Dir::S => (pos.0 - 1, pos.1),
            Dir::W => (pos.0, pos.1 + 1),
        }
    }

    fn rotate_c(&self) -> Self {
        match self {
            Dir::N => Dir::E,
            Dir::E => Dir::S,
            Dir::S => Dir::W,
            Dir::W => Dir::N,
        }
    }

    fn rotate_cc(&self) -> Self {
        match self {
            Dir::N => Dir::W,
            Dir::E => Dir::N,
            Dir::S => Dir::E,
            Dir::W => Dir::S,
        }
    }
    fn rotate_180(&self) -> Self {
        match self {
            Dir::N => Dir::S,
            Dir::E => Dir::W,
            Dir::S => Dir::N,
            Dir::W => Dir::E,
        }
    }

    fn should_step(
        &self,
        pos: (usize, usize),
        map: &Vec<Vec<char>>,
        seen_map: &Vec<Vec<Vec<u32>>>,
        score: u32,
    ) -> Option<(usize, usize)> {
        let pos = self.step(pos);
        if map[pos.0][pos.1] == '#' || seen_map[pos.0][pos.1][self.index()] <= score + 1 {
            return None;
        }
        Some(pos)
    }
}

#[derive(PartialEq, Eq)]
struct Entry {
    score: u32,
    pos: (usize, usize),
    facing: Dir,
}

impl Ord for Entry {
    fn cmp(&self, other: &Self) -> cmp::Ordering {
        self.score.cmp(&other.score)
    }
}

impl PartialOrd for Entry {
    fn partial_cmp(&self, other: &Self) -> Option<cmp::Ordering> {
        Some(self.cmp(other))
    }
}

package day17

import (
	"errors"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// Block used to store kinds of blocks
type Block int

// Set the block types
const (
	Sand      Block = 0
	Clay      Block = 1
	Spring    Block = 2
	Water     Block = 3
	RestWater Block = 4
)

// Direction used to store directions
type Direction int

// Set the directions
const (
	NX Direction = 0
	NY Direction = 1
	PX Direction = 2
	PY Direction = 3
)

// Square store a grid square
type Square struct {
	Type  Block
	Final bool
}

var springPos = []int{500, 0}

// func draw(grid [][]*Square) {
// 	out := make([]string, len(grid[0]))
// 	for _, xWall := range grid {
// 		for y, block := range xWall {
// 			if block.Type == Sand {
// 				out[y] += "."
// 			} else if block.Type == Clay {
// 				out[y] += "#"
// 			} else if block.Type == Spring {
// 				out[y] += "+"
// 			} else if block.Type == RestWater {
// 				out[y] += "~"
// 			} else {
// 				out[y] += "|"
// 			}
// 		}
// 	}
// 	for _, line := range out {
// 		log.Print(line)
// 	}
// }

func getGrid(filename string) ([][]*Square, error) {
	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return nil, err
	}
	biggestX := 500
	biggestY := 0
	for _, line := range lines {
		splitLine := strings.Split(line, ", ")
		num1, err := strconv.Atoi(strings.Split(splitLine[0], "=")[1])
		if err != nil {
			return nil, err
		}
		range2, err := strconv.Atoi(strings.Split(splitLine[1], "..")[1])
		if err != nil {
			return nil, err
		}
		if strings.Contains(splitLine[1], "x=") {
			if range2 > biggestX {
				biggestX = range2
			}
			if num1 > biggestY {
				biggestY = num1
			}
		} else {
			if range2 > biggestY {
				biggestY = range2
			}
			if num1 > biggestX {
				biggestX = num1
			}
		}
	}

	grid := make([][]*Square, biggestX+5)
	for x := 0; x < biggestX+5; x++ {
		for y := 0; y <= biggestY; y++ {
			grid[x] = append(grid[x], new(Square))
		}
	}
	grid[springPos[0]][springPos[1]].Type = Spring
	for _, line := range lines {
		splitLine := strings.Split(line, ", ")
		rangeX := false
		if strings.Contains(splitLine[1], "x=") {
			rangeX = true
		}
		num1, err := strconv.Atoi(strings.Split(splitLine[0], "=")[1])
		if err != nil {
			return nil, err
		}
		range1, err := strconv.Atoi(strings.Split(strings.Split(splitLine[1], "..")[0], "=")[1])
		if err != nil {
			return nil, err
		}
		range2, err := strconv.Atoi(strings.Split(splitLine[1], "..")[1])
		if err != nil {
			return nil, err
		}

		for i := range1; i <= range2; i++ {
			if rangeX {
				grid[i][num1].Type = Clay
				grid[i][num1].Final = true
			} else {
				grid[num1][i].Type = Clay
				grid[num1][i].Final = true
			}
		}
	}

	clayAreas := make([][]int, 0)

	for y := 0; y < len(grid[0]); y++ {
		startX := -1
		for x := 0; x < len(grid); x++ {
			if grid[x][y].Type == Clay {
				if startX != -1 && startX+1 < x {
					clayAreas = append(clayAreas, []int{startX, y, x, y})
				}
				startX = x
			}
		}
	}

	x := springPos[0]
	y := springPos[1]
	for tick := 0; true; tick++ {
		usingPrev := true
		lastDir := PY
		for {
			if grid[x][y+1].Type == Sand || (grid[x][y+1].Type == Water && !grid[x][y+1].Final) {
				y++
				lastDir = PY
				if grid[x][y].Type == Sand {
					grid[x][y].Type = Water
					if y == biggestY {
						grid[x][y].Final = true
						x = springPos[0]
						y = springPos[1]
					}
					break
				}
			} else if lastDir != PX && x > 0 && (grid[x-1][y].Type == Sand || (grid[x-1][y].Type == Water && !grid[x-1][y].Final)) && !(grid[x][y+1].Final && grid[x][y+1].Type == Water) {
				x--
				lastDir = NX
				if grid[x][y].Type == Sand {
					grid[x][y].Type = Water
					break
				}
			} else if lastDir != NX && x < biggestX+5 && (grid[x+1][y].Type == Sand || (grid[x+1][y].Type == Water && !grid[x+1][y].Final)) && !(grid[x][y+1].Final && grid[x][y+1].Type == Water) {
				x++
				lastDir = PX
				if grid[x][y].Type == Sand {
					grid[x][y].Type = Water
					break
				}
			} else if usingPrev {
				usingPrev = false
				x = springPos[0]
				y = springPos[1]
				lastDir = PY
			} else {
				if grid[x][y].Type == Water {
					grid[x][y].Final = true
					for _, pos := range clayAreas {
						good := true
						for x2 := pos[0] + 1; x2 < pos[2]; x2++ {
							if grid[x2][pos[1]].Type != Water || !grid[x2][pos[1]].Final {
								good = false
								break
							}
						}
						if good {
							for x2 := pos[0] + 1; x2 < pos[2]; x2++ {
								grid[x2][pos[1]].Type = RestWater
							}
						}
					}
					x = springPos[0]
					y = springPos[1]
					lastDir = PY
				} else {
					return grid, nil

				}
			}
		}
	}
	return nil, errors.New("should never be seen")
}

// One the first puzzle for the day
func One(filename string) (string, error) {
	grid, err := getGrid(filename)
	if err != nil {
		return "", nil
	}
	answer := -2
	for _, xWall := range grid {
		for _, block := range xWall {
			if block.Type == Water || block.Type == RestWater {
				answer++
			}
		}
	}
	return strconv.Itoa(answer), nil // Return with the answer
}

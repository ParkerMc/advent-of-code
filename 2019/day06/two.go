package day06

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

func getMap(item string, items map[string]*point) map[string]int {
	out := make(map[string]int)
	cur := items[items[items[item].orbit].orbit]
	for z := 1; cur != nil; z++ {
		out[cur.name] = z
		cur = items[cur.orbit]
	}
	return out
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	items := getPoints(lines)

	youMap := getMap("YOU", items)
	santaMap := getMap("SAN", items)

	min := 1000000
	for key, val := range youMap {
		if _, ok := santaMap[key]; ok && val+santaMap[key] < min {
			min = val + santaMap[key]
		}
	}
	return strconv.Itoa(min), nil // Will never get here
}

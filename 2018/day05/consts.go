// Package day05 2018
package day05

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "9704"
const testAnswerTwo = "6942"

var testFilename = "../test/private/" + tools.GetPackageName()

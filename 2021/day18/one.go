package day18

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

type entry struct {
	value int
	sub   []*entry
}

func loadList(in string) (*entry, int) {
	i := 0
	e := entry{}
	if in[0] == '[' {
		for in[i] != ']' {
			sE, iN := loadList(in[i+1:])
			e.sub = append(e.sub, sE)
			i += iN + 1
		}
		i++
	} else {
		e.value = int(in[0]) - 48
		return &e, 1
	}
	return &e, i
}

// func (e *entry) toStr() string {
// 	if e == nil {
// 		return ""
// 	}
// 	if len(e.sub) > 0 {
// 		out := "[" + e.sub[0].toStr()
// 		for _, e2 := range e.sub[1:] {
// 			out += "," + e2.toStr()
// 		}
// 		return out + "]"
// 	}
// 	return strconv.Itoa(e.value)
// }

func (e *entry) add(e2 *entry) *entry {
	return &entry{sub: []*entry{e, e2}}
}

func (e *entry) addRight(num int) bool {
	if len(e.sub) > 0 {
		for _, sub := range e.sub {
			if sub.addRight(num) {
				return true
			}
		}
	} else {
		e.value += num
		return true
	}
	return false
}

func (e *entry) addLeft(num int) bool {
	if len(e.sub) > 0 {
		for i := len(e.sub) - 1; i >= 0; i-- {
			if e.sub[i].addLeft(num) {
				return true
			}
		}
	} else {
		e.value += num
		return true
	}
	return false
}

func (e *entry) exsplode(depth int) (int, bool, int) {
	for i, sub := range e.sub {
		if depth == 4 && len(sub.sub) > 0 {
			left := sub.sub[0].value
			right := sub.sub[1].value
			if i == 0 {
				e.sub[0] = &entry{value: 0}
				e.sub[1].addRight(right)
				return left, true, 0
			}
			e.sub[1] = &entry{value: 0}
			e.sub[0].addLeft(left)

			return 0, true, right
		}
		if left, exsplode, right := sub.exsplode(depth + 1); exsplode {
			if i == 1 {
				if left > 0 {
					e.sub[0].addLeft(left)
					return 0, true, right
				}
			} else if right > 0 {
				e.sub[1].addRight(right)
				return left, true, 0
			}
			return left, true, right
		}
	}
	return 0, false, 0
}

func (e *entry) split() bool {
	if len(e.sub) > 0 {
		for _, sub := range e.sub {
			if sub.split() {
				return true
			}
		}
	} else {
		if e.value > 9 {
			// Do split
			val1 := e.value / 2
			e.sub = []*entry{{value: val1}, {value: e.value - val1}}
			return true
		}
	}
	return false
}

func (e *entry) reduce() {
	changed := true
	for changed {
		_, changed, _ = e.exsplode(1)
		if !changed {
			changed = e.split()
		}
	}
}

func (e *entry) magnitude() int {
	if len(e.sub) > 0 {
		return e.sub[0].magnitude()*3 + e.sub[1].magnitude()*2
	}
	return e.value
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	current, _ := loadList(lines[0])
	for _, line := range lines[1:] {
		e, _ := loadList(line)
		current = current.add(e)
		current.reduce()
	}

	return strconv.Itoa(current.magnitude()), nil // Return with the answer
}

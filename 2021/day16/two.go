package day16

import (
	"errors"
	"math"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

func solveMany(packets []packet) ([]int, error) {
	var err error
	out := make([]int, len(packets))
	for i, p := range packets {
		out[i], err = solve(p)
		if err != nil {
			return nil, err
		}
	}
	return out, nil
}

func solve(p packet) (int, error) {
	nums, err := solveMany(p.subPackets)
	if err != nil {
		return 0, err
	}
	switch p.typeID {
	case 0: // Sums
		answer := 0
		for _, val := range nums {
			answer += val
		}
		return answer, nil
	case 1: // Multiply
		answer := nums[0]
		for _, val := range nums[1:] {
			answer *= val
		}
		return answer, nil
	case 2: // Min
		answer := math.MaxInt
		for _, val := range nums {
			if answer > val {
				answer = val
			}
		}
		return answer, nil
	case 3: // Max
		answer := math.MinInt
		for _, val := range nums {
			if answer < val {
				answer = val
			}
		}
		return answer, nil
	case 4: // Literals
		return p.val, nil
	case 5: // Greater than
		if nums[0] > nums[1] {
			return 1, nil
		}
		return 0, nil
	case 6: // Less than
		if nums[0] < nums[1] {
			return 1, nil
		}
		return 0, nil
	case 7: // Equal
		if nums[0] == nums[1] {
			return 1, nil
		}
		return 0, nil
	}
	return 0, errors.New("unknown op")
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	bin := ""
	for _, hex := range lines[0] {
		bin += hexTable[hex]
	}
	packet, _ := readPacket(bin)
	answer, err := solve(packet)
	if err != nil {
		return "", err
	}

	return strconv.Itoa(answer), nil // Return with the answer
}

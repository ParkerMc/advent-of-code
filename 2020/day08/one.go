package day08

import (
	"fmt"
	"regexp"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// OPTYPE holds each op type
type OPTYPE int

// Set the OPTYPE's
const (
	OPTypeACC OPTYPE = 1
	OPTypeJMP OPTYPE = 2
	OPTypeNOP OPTYPE = 3
)

// OP holds each op(line) in the program
type OP struct {
	OP    OPTYPE
	Value int
}

var progamReg = regexp.MustCompile(`^([^\s]*) ((?:\+|-)\d*)$`)

func convertToProgram(lines []string) ([]OP, error) {
	program := make([]OP, len(lines))
	for i, line := range lines {
		match := progamReg.FindStringSubmatch(line)
		op := OP{}
		switch match[1] {
		case "acc":
			op.OP = OPTypeACC
		case "jmp":
			op.OP = OPTypeJMP
		case "nop":
			op.OP = OPTypeNOP
		default:
			return nil, fmt.Errorf("unknown instruction: %s", match[1])
		}
		var err error
		op.Value, err = strconv.Atoi(match[2])
		if err != nil {
			return nil, err
		}
		program[i] = op
	}
	return program, nil
}

func runUntilRepeat(program []OP) (int, []bool, bool) { // accumulator, a map of if instruction was called, good(actually ends)
	accumulator := 0
	n := 0
	length := len(program)
	done := make([]bool, len(program))
	var op OP
	for n < length {
		if done[n] {
			return accumulator, done, false
		}
		done[n] = true
		op = program[n]
		switch op.OP {
		case OPTypeACC:
			accumulator += op.Value
			n++
		case OPTypeJMP:
			n += op.Value
		case OPTypeNOP:
			n++
		}
	}
	return accumulator, done, true
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	program, err := convertToProgram(lines)
	if err != nil {
		return "", nil
	}
	accumulator, _, _ := runUntilRepeat(program)

	return strconv.Itoa(accumulator), nil // Return with the answer
}

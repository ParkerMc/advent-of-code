// Package day05 2018
package day05

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "5084"
const testAnswerTwo = "17882"

var testFilename = "../test/private/" + tools.GetPackageName()

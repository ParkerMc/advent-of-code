package day10

import (
	"errors"
	"sort"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	nums := make([]int, len(lines))
	for i, strNum := range lines {
		nums[i], err = strconv.Atoi(strNum)
		if err != nil {
			return "", err
		}
	}

	sort.Ints(nums)

	count1 := 0
	count3 := 0
	last := 0
	for _, next := range nums {
		switch next - last {
		case 1:
			count1++
		case 2:
		case 3:
			count3++
		default:
			return "", errors.New("invalid adaptor diff")
		}
		last = next
	}
	count3++ // device rated for 3 higher than heighest

	return strconv.Itoa(count1 * count3), nil // Return with the answer
}

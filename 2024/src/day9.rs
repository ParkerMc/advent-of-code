use std::time::Duration;

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    let mut i: usize = 0;
    let mut data = input
        .iter()
        .flat_map(|l| l.chars())
        .map(|c| c.to_string().parse::<usize>().unwrap())
        .chunks(2)
        .into_iter()
        .flat_map(|l| {
            let pair = l.collect_vec();
            let mut data = vec![i; pair[0]];
            if pair.len() > 1 {
                data.extend(vec![usize::MAX; pair[1]]);
            }
            i += 1;
            data
        })
        .collect_vec();
    let mut head = data
        .iter()
        .enumerate()
        .find(|(_, v)| **v == usize::MAX)
        .unwrap()
        .0;
    let mut tail = data.len() - 1;
    while head < tail {
        data[head] = data[tail];
        tail -= 1;
        while data[head] != usize::MAX {
            head += 1;
        }
        while data[tail] == usize::MAX {
            tail -= 1;
        }
    }

    data.iter()
        .take(tail + 1)
        .enumerate()
        .map(|(i, v)| i * v)
        .sum::<usize>()
        .to_string()
}

fn part2(input: &Vec<String>) -> String {
    let data = input
        .iter()
        .flat_map(|l| l.chars())
        .map(|c| c.to_string().parse::<usize>().unwrap())
        .chunks(2)
        .into_iter()
        .map(|c| c.collect_vec())
        .collect_vec();
    let mut i: usize = 0;
    let mut num: usize = 0;
    let mut data_groups_rev = data
        .iter()
        .map(|pair| {
            let f = File {
                id: num,
                size: pair[0],
                start_i: i,
            };
            num += 1;
            i += pair[0] + pair.get(1).unwrap_or(&0);
            f
        })
        .collect_vec();
    data_groups_rev.reverse();
    i = 0;
    let mut empty_groups = data
        .iter()
        .filter(|p| p.len() == 2)
        .map(|pair| {
            i += pair[0];
            let f = FreeSpace {
                size: pair[1],
                start_i: i,
            };
            i += pair[1];
            f
        })
        .collect_vec();
    num = 0;
    let mut data_vec = data
        .iter()
        .flat_map(|pair| {
            let mut data = vec![num; pair[0]];
            if pair.len() > 1 {
                data.extend(vec![usize::MAX; pair[1]]);
            }
            num += 1;
            data
        })
        .collect_vec();

    let mut largest_gap = 10;
    for f in data_groups_rev {
        if f.size > largest_gap {
            continue;
        }
        let to_option = empty_groups
            .iter()
            .enumerate()
            .take_while(|(_, e)| e.start_i < f.start_i)
            .find(|(_, e)| e.size >= f.size);
        if to_option.is_none() {
            largest_gap = f.size - 1;
            continue;
        }
        let (i, to) = to_option.unwrap();
        for offset in 0..f.size {
            data_vec[to.start_i + offset] = f.id;
            data_vec[f.start_i + offset] = usize::MAX;
        }
        if to.size == f.size {
            empty_groups.remove(i);
            continue;
        }
        empty_groups[i] = FreeSpace {
            size: to.size - f.size,
            start_i: to.start_i + f.size,
        }
    }

    data_vec
        .into_iter()
        .enumerate()
        .filter(|(_, v)| *v != usize::MAX)
        .map(|(i, v)| i * v)
        .sum::<usize>()
        .to_string()
}

struct File {
    id: usize,
    size: usize,
    start_i: usize,
}

struct FreeSpace {
    size: usize,
    start_i: usize,
}

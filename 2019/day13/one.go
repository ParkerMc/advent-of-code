package day13

import (
	"errors"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

const (
	empty  = 0
	wall   = 1
	block  = 2
	paddle = 3
	ball   = 4
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	p, err := tools.NewProgram(lines[0])
	if err != nil {
		return "", err
	}
	err = p.Run()
	if err != nil {
		return "", err
	}
	if !p.GetStopped() {
		return "", errors.New("program not stopped")
	}

	area := make(map[int]map[int]int, 0)
	for done := false; !done; {
		x, err := p.RemoveOutput()
		if err != nil {
			done = true
		} else {
			y, err := p.RemoveOutput()
			if err != nil {
				return "", err
			}
			val, err := p.RemoveOutput()
			if err != nil {
				return "", err
			}
			if _, ok := area[x]; !ok {
				area[x] = make(map[int]int, 0)
			}
			area[x][y] = val
		}
	}

	count := 0
	for _, col := range area {
		for _, spot := range col {
			if spot == block {
				count++
			}
		}
	}

	return strconv.Itoa(count), nil // Return with the answer
}

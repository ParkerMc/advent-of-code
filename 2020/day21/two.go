package day21

import (
	"fmt"
	"sort"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	_, allerginToFood := part1(lines)

	done := make(map[string]bool, 0)
	changed := 1
	for changed > 0 {
		changed = 0
		for allergin, foods := range allerginToFood {
			if len(foods) == 1 {
				for allergin2, foods2 := range allerginToFood {
					if allergin2 == allergin {
						continue
					}
					for i, food2 := range foods2 {
						if food2 == foods[0] {
							foods2[i] = foods2[len(foods2)-1]
							allerginToFood[allergin2] = foods2[:len(foods2)-1]
							break
						}
					}
				}
				if !done[allergin] {
					changed++
					done[allergin] = true
				}
			}
		}
	}

	keys := make([]string, len(allerginToFood))
	i := 0
	for key := range allerginToFood {
		keys[i] = key
		i++
	}

	sort.Strings(keys)

	badFoods := make([]string, 0)
	for _, allergin := range keys {
		if len(allerginToFood[allergin]) != 1 {
			return "", fmt.Errorf("wrong number of foods: %d", len(allerginToFood[allergin]))
		}
		badFoods = append(badFoods, allerginToFood[allergin]...)

	}

	return strings.Join(badFoods, ","), nil // Return with the answer
}

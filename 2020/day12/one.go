package day12

import (
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

const (
	north = 0
	east  = 1
	south = 2
	west  = 3
)

func dirL90(dir int) int {
	switch dir {
	case north:
		return west
	case south:
		return east
	case east:
		return north
	case west:
		return south
	}
	return east
}

func dirR90(dir int) int {
	switch dir {
	case north:
		return east
	case south:
		return west
	case east:
		return south
	case west:
		return north
	}
	return east
}

func dir180(dir int) int {
	switch dir {
	case north:
		return south
	case south:
		return north
	case east:
		return west
	case west:
		return east
	}
	return east
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	pos := tools.Pos2D{X: 0, Y: 0}
	dir := east
	for _, line := range lines {
		val, err := strconv.Atoi(line[1:])
		if err != nil {
			return "", err
		}
		switch line[0] {
		case 'N':
			pos.Y += val
		case 'S':
			pos.Y -= val
		case 'E':
			pos.X += val
		case 'W':
			pos.X -= val
		case 'L':
			switch val {
			case 90:
				dir = dirL90(dir)
			case 180:
				dir = dir180(dir)
			case 270:
				dir = dirR90(dir)
			default:
				return "", errors.New("angle isn't 90 or 180 or 270")
			}
		case 'R':
			switch val {
			case 90:
				dir = dirR90(dir)
			case 180:
				dir = dir180(dir)
			case 270:
				dir = dirL90(dir)
			default:
				return "", errors.New("angle isn't 90 or 180 or 270")
			}
		case 'F':
			switch dir {
			case north:
				pos.Y += val
			case south:
				pos.Y -= val
			case east:
				pos.X += val
			case west:
				pos.X -= val
			}
		default:
			return "", fmt.Errorf("unknown action: %c", line[0])
		}
	}

	return strconv.Itoa(tools.Abs(pos.X) + tools.Abs(pos.Y)), nil // Return with the answer
}

// Package day04 2019
package day04

import "gitlab.com/parkermc/advent-of-code/2019/tools"

const testAnswerOne = "1665"
const testAnswerTwo = "1131"

var testFilename = "../test/private/" + tools.GetPackageName()

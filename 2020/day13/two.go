package day13

import (
	"fmt"
	"math/big"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

type busT struct {
	i       int
	number  int
	highest int
}

var one = big.NewInt(1)

// Taken from https://rosettacode.org/wiki/Chinese_remainder_theorem#Go
// Would be cool to write my own but I don't know linear algebra
// Solves sets of x=(a % n)
func crt(a, n []*big.Int) (*big.Int, error) {
	p := new(big.Int).Set(n[0])
	for _, n1 := range n[1:] {
		p.Mul(p, n1)
	}
	var x, q, s, z big.Int
	for i, n1 := range n {
		q.Div(p, n1)
		z.GCD(nil, &s, n1, &q)
		if z.Cmp(one) != 0 {
			return nil, fmt.Errorf("%d not coprime", n1)
		}
		x.Add(&x, s.Mul(a[i], s.Mul(&s, &q)))
	}
	return x.Mod(&x, p), nil
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	split := strings.Split(lines[1], ",")
	busses := make([]busT, 0)
	for i, strBus := range split {
		if strBus != "x" {
			bus, err := strconv.Atoi(strBus)
			if err != nil {
				return "", err
			}
			busses = append(busses, busT{i: i, number: bus, highest: bus})
		}
	}

	a, n := make([]*big.Int, len(busses)), make([]*big.Int, len(busses))
	for i, bus := range busses {
		// Got to this 		log.Printf("(t+%d)%%%d == 0", bus.i, bus.number)
		// Then got online help with linear algebra
		n[i] = big.NewInt(int64(bus.number))
		a[i] = big.NewInt(int64((-bus.i) % bus.number))
	}

	answ, err := crt(a, n)
	return answ.String(), err // Return with the answer
}

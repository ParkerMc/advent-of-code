package tools

import "testing"

func TestAdd(t *testing.T) {
	pos2 := Pos2D{X: 1, Y: 2}
	pos2 = pos2.Add(Pos2D{X: 2, Y: 3})
	if pos2.X != 3 || pos2.Y != 5 {
		t.Fatal("Add error with 2D")
	}

	pos3 := Pos3D{X: 1, Y: 2, Z: 3}
	pos3 = pos3.Add(Pos3D{X: 2, Y: 3, Z: 4})
	if pos3.X != 3 || pos3.Y != 5 || pos3.Z != 7 {
		t.Fatal("Add error with 3D")
	}

	pos4 := Pos4D{W: 1, X: 2, Y: 3, Z: 4}
	pos4 = pos4.Add(Pos4D{W: 2, X: 3, Y: 4, Z: 5})
	if pos4.W != 3 || pos4.X != 5 || pos4.Y != 7 || pos4.Z != 9 {
		t.Fatal("Add error with 4D")
	}
}

func TestMDist(t *testing.T) {
	pos2 := Pos2D{X: -5, Y: -5}
	if pos2.MDist(&Pos2D{X: 5, Y: 5}) != 20 {
		t.Fatal("MDist error with 2D")
	}

	pos3 := Pos3D{X: -5, Y: -5, Z: -5}
	if pos3.MDist(&Pos3D{X: 5, Y: 5, Z: 5}) != 30 {
		t.Fatal("MDist error with 3D")
	}
}

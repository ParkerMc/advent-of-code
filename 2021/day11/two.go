package day11

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	grid := make([][]int, len(lines[0]))
	for x := 0; x < len(lines[0]); x++ {
		grid[x] = make([]int, len(lines))
	}

	for y, row := range lines {
		for x, val := range row {
			grid[x][y] = int(val) - 48
		}
	}

	fullCount := len(grid) * len(grid[0])
	toFlash := make([]tools.Pos2D, 0)
	for step := 0; ; step++ {
		// add one to all and mark things to be flashed
		for x, col := range grid {
			for y := range col {
				grid[x][y]++
				if grid[x][y] == 10 {
					toFlash = append(toFlash, tools.Pos2D{X: x, Y: y})
				}
			}
		}

		flashCount := 0
		for len(toFlash) > 0 {
			pos := toFlash[0]
			toFlash = toFlash[1:]
			flashCount++
			grid[pos.X][pos.Y] = 0
			for _, addPos := range []tools.Pos2D{
				{X: -1, Y: 1}, {X: 0, Y: 1}, {X: 1, Y: 1},
				{X: -1, Y: 0}, {X: 1, Y: 0},
				{X: -1, Y: -1}, {X: 0, Y: -1}, {X: 1, Y: -1},
			} {
				curPos := pos.Add(addPos)
				if curPos.X < 0 || curPos.Y < 0 || curPos.X >= len(grid) || curPos.Y >= len(grid[curPos.X]) || grid[curPos.X][curPos.Y] == 0 {
					continue
				}
				grid[curPos.X][curPos.Y]++
				if grid[curPos.X][curPos.Y] == 10 {
					toFlash = append(toFlash, curPos)
				}
			}
		}
		if flashCount == fullCount {
			return strconv.Itoa(step + 1), nil
		}
	}
}

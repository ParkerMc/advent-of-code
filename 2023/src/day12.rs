use std::{collections::HashMap, time::Duration, vec};

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    solve(parse_input(input)).to_string()
}

fn part2(input: &Vec<String>) -> String {
    let parsed = parse_input(input)
        .iter()
        .map(|(c, n)| {
            let mut c_out = c.clone();
            let mut n_out = n.clone();
            for _ in 0..4 {
                c_out.push('?');
                c_out.extend(c);
                n_out.extend(n);
            }
            (c_out, n_out)
        })
        .collect_vec();
    solve(parsed).to_string()
}

fn parse_input(input: &Vec<String>) -> Vec<(Vec<char>, Vec<usize>)> {
    input
        .iter()
        .map(|l| {
            let split = l.split(' ').collect_vec();
            (
                split[0].chars().collect_vec(),
                split[1]
                    .split(",")
                    .map(|n| n.parse().expect("Expected number"))
                    .collect_vec(),
            )
        })
        .collect_vec()
}

fn solve(parsed_input: Vec<(Vec<char>, Vec<usize>)>) -> usize {
    parsed_input
        .iter()
        .map(|(l, dmg)| {
            let mut required = l
                .iter()
                .enumerate()
                .filter(|(_, c)| **c == '#')
                .map(|(i, _)| i)
                .collect_vec();
            required.push(l.len());

            // From the left
            let mut possibles = vec![];
            let mut start_i = 0;
            for dist in dmg {
                let pos = (start_i..=l.len() - dist)
                    .filter(|s| {
                        (*s..(s + dist)).all(|i| l[i] != '.')
                            && (*s == 0 || l[s - 1] != '#') // Ensure the one before or after is not required
                            && (*s + dist >= l.len() || l[s + dist] != '#')
                    })
                    .map(|s| Range {
                        start: s,
                        end: s + dist,
                    })
                    .collect_vec();
                start_i = pos[0].end + 1;
                possibles.push(pos);
            }

            // From the right
            let mut end_i = l.len() + 1;
            for j in (0..possibles.len()).rev() {
                possibles[j].retain(|r| r.end < end_i);
                end_i = possibles[j].last().expect("No last").start;
            }

            // combine and filter
            let mut cache: HashMap<CacheKey, usize> = HashMap::new();
            combine_filter(0, &possibles, 0, &required, 0, &mut cache)
        })
        .sum::<usize>()
}

fn combine_filter<'a>(
    last_end: usize,
    source: &Vec<Vec<Range>>,
    i: usize,
    required: &Vec<usize>,
    required_i: usize,
    cache: &'a mut HashMap<CacheKey, usize>,
) -> usize {
    if i == source.len() {
        if required_i != required.len() - 1 {
            return 0;
        }
        return 1;
    }

    if let Some(ret) = cache.get(&CacheKey {
        last_end,
        i,
        required_i,
    }) {
        return *ret;
    }

    let res = source[i]
        .iter()
        .filter(|nr| (last_end == 0 || nr.start > last_end) && nr.start <= required[required_i])
        .map(|nr| {
            let mut new_ri = required_i;
            while nr.end > required[new_ri] {
                new_ri += 1;
            }
            combine_filter(nr.end, source, i + 1, required, new_ri, cache)
        })
        .sum();

    cache.insert(
        CacheKey {
            last_end,
            i,
            required_i,
        },
        res,
    );

    res
}

#[derive(Clone)]
struct Range {
    start: usize,
    end: usize,
}

#[derive(PartialEq, Eq, Hash)]
struct CacheKey {
    last_end: usize,
    i: usize,
    required_i: usize,
}

// Package day24 2020
package day24

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "485"
const testAnswerTwo = "3933"

var testFilename = "../test/private/" + tools.GetPackageName()

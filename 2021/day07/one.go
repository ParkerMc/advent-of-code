package day07

import (
	"math"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	split := strings.Split(lines[0], ",")
	crabs := make([]int, len(split))
	for i, crab := range split {
		crabs[i], err = strconv.Atoi(crab)
		if err != nil {
			return "", err
		}
	}

	minV := math.MaxInt
	maxV := 0
	for _, crab := range crabs {
		if crab < minV {
			minV = crab
		}
		if crab > maxV {
			maxV = crab
		}
	}

	answerMap := make(map[int]int, maxV-minV)
	for _, crab := range crabs {
		for i := minV; i <= maxV; i++ {
			answerMap[i] += tools.Abs(crab - i)
		}
	}

	answer := math.MaxInt
	for _, val := range answerMap {
		if val < answer {
			answer = val
		}
	}

	return strconv.Itoa(answer), nil // Return with the answer
}

package day09

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// Marble used to store the marbles
type Marble struct {
	val   int
	Prev  *Marble
	After *Marble
}

func solve(filename string, multiplyer int) (int, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return 0, err
	}
	players, err := strconv.Atoi(strings.Split(lines[0], " ")[0])
	if err != nil {
		return 0, err
	}
	lastPoints, err := strconv.Atoi(strings.Split(lines[0], " ")[6])
	if err != nil {
		return 0, err
	}
	lastPoints *= multiplyer
	points := make([]int, players)
	player := -1
	marble := new(Marble)
	marble.val = 0
	marble.Prev = marble
	marble.After = marble
	currentMarble := marble

	for marbleWorth := 1; marbleWorth <= lastPoints; marbleWorth++ {
		player++
		if player == players {
			player = 0
		}
		if marbleWorth%23 == 0 {
			points[player] += marbleWorth + currentMarble.Prev.Prev.Prev.Prev.Prev.Prev.Prev.val
			currentMarble = currentMarble.Prev.Prev.Prev.Prev.Prev.Prev
			currentMarble.Prev.Prev.After = currentMarble
			currentMarble.Prev = currentMarble.Prev.Prev
		} else {
			marble := &Marble{marbleWorth, currentMarble.After, currentMarble.After.After}
			currentMarble.After.After.Prev = marble
			currentMarble.After.After = marble
			currentMarble = marble
		}
	}

	mostPts := 0
	for _, point := range points {
		if mostPts < point {
			mostPts = point
		}
	}
	return mostPts, nil
}

// One the first puzzle for the day
func One(filename string) (string, error) {
	answer, err := solve(filename, 1)
	return strconv.Itoa(answer), err // Return with the answer
}

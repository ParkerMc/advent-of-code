package day01

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	answer := 0
	last := 999999999999
	for _, val := range lines {
		intVal, err := strconv.Atoi(val)
		if err != nil {
			return "", err
		}

		if intVal > last {
			answer++
		}
		last = intVal
	}

	return strconv.Itoa(answer), nil // Return with the answer
}

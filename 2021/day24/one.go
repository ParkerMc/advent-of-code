package day24

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	rules, err := getRules(filename)
	if err != nil {
		return "", err
	}

	num := make([]int, 14)
	for _, r := range rules {

		if r.diff > 0 {
			num[r.fromI] = 9 - r.diff
			num[r.toI] = 9
		} else {
			num[r.fromI] = 9
			num[r.toI] = 9 + r.diff
		}
	}

	answer := 0
	for _, n := range num {
		answer *= 10
		answer += n
	}

	return strconv.Itoa(answer), nil // Return with the answer
}

type rule struct {
	fromI int
	toI   int
	diff  int
}

func getRules(filename string) ([]rule, error) {
	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return nil, err
	}

	a := make([]int, 0)
	b := make([]int, 0)
	c := make([]int, 0)
	nextC := false
	nextB := false
	for _, line := range lines {
		if nextB {
			split := strings.Split(line, " ")
			v, err := strconv.Atoi(split[len(split)-1])
			if err != nil {
				return nil, err
			}
			b = append(b, v)
			nextB = false
			continue
		}
		if strings.HasPrefix(line, "div z ") {
			split := strings.Split(line, " ")
			v, err := strconv.Atoi(split[len(split)-1])
			if err != nil {
				return nil, err
			}
			a = append(a, v)
			nextB = true
			continue
		}
		if nextC {
			split := strings.Split(line, " ")
			v, err := strconv.Atoi(split[len(split)-1])
			if err != nil {
				return nil, err
			}
			c = append(c, v)
			nextC = false
			continue
		}
		if line == "add y w" {
			nextC = true
		}
	}

	rules := make([]rule, 0)
	stack := make([]int, 0)
	for i, v := range a {
		if v == 26 {
			lastI := stack[len(stack)-1]
			stack = stack[:len(stack)-1]
			diff := c[lastI] + b[i]
			rules = append(rules, rule{fromI: lastI, toI: i, diff: diff})
		} else {
			stack = append(stack, i)
		}
	}

	return rules, nil
}

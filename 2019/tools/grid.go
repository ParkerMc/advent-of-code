package tools

// GridSet sets a spot on the grid
func GridSet(area map[int]map[int]int, pos Pos2D, val int) {
	if _, ok := area[pos.X]; !ok {
		area[pos.X] = map[int]int{pos.Y: val}
	} else {
		area[pos.X][pos.Y] = val
	}
}

// GridMinMax returns minX, maxX, minY, maxY
func GridMinMax(area map[int]map[int]int) (int, int, int, int) {
	minX := 10000000000
	maxX := -10000000000
	minY := 10000000000
	maxY := -10000000000
	for x, line := range area {
		if x < minX {
			minX = x
		}
		if x > maxX {
			maxX = x
		}
		for y := range line {
			if y < minY {
				minY = y
			}
			if y > maxY {
				maxY = y
			}
		}
	}
	return minX, maxX, minY, maxY
}

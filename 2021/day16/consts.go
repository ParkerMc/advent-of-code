// Package day16 2018
package day16

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "949"
const testAnswerTwo = "1114600142730"

var testFilename = "../test/private/" + tools.GetPackageName()

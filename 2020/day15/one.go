package day15

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	split := strings.Split(lines[0], ",")
	nums := make([]int, len(split))
	for i, strNum := range split {
		nums[i], err = strconv.Atoi(strNum)
		if err != nil {
			return "", err
		}
	}

	lastI := make(map[int]int, len(nums))
	for i, num := range nums[:len(nums)-1] {
		lastI[num] = i + 1
	}
	lastNum := nums[len(nums)-1]

	for i := len(nums); i < 2020; i++ {
		if val, ok := lastI[lastNum]; ok {
			lastI[lastNum] = i
			lastNum = i - val
		} else {
			lastI[lastNum] = i
			lastNum = 0
		}
	}

	return strconv.Itoa(lastNum), nil // Return with the answer
}

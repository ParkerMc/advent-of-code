package day11

import (
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// GridPos the item the pos represents
type GridPos int

// Set possible Gridpos values
const (
	GridPosFloor        GridPos = 0
	GridPosSeatEmpty    GridPos = 1
	GridPosSeatOccupied GridPos = 2
)

// func print(grid [][]GridPos) {
// 	out := make([]string, len(grid[0]))
// 	for _, cols := range grid {
// 		for y, val := range cols {
// 			switch val {
// 			case GridPosFloor:
// 				out[y] += "."
// 				break
// 			case GridPosSeatEmpty:
// 				out[y] += "L"
// 				break
// 			case GridPosSeatOccupied:
// 				out[y] += "#"
// 				break
// 			}
// 		}
// 	}

// 	for _, line := range out {
// 		log.Print(line)
// 	}
// }

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	xCount := len(lines[0])
	yCount := len(lines)
	seats := make([][]GridPos, xCount)
	for i := 0; i < xCount; i++ {
		seats[i] = make([]GridPos, yCount)
	}

	for y, line := range lines {
		for x, c := range line {
			switch c {
			case 'L':
				seats[x][y] = GridPosSeatEmpty
			case '.':
				seats[x][y] = GridPosFloor
			default:
				return "", fmt.Errorf("unknown char in data: %c", c)
			}
		}
	}

	for {
		toSwap := make([]tools.Pos2D, 0)
		for x, cols := range seats {
			for y, val := range cols {
				occupied := 0
				if y-1 >= 0 && cols[y-1] == GridPosSeatOccupied {
					occupied++
				}
				if y+1 < len(cols) && cols[y+1] == GridPosSeatOccupied {
					occupied++
				}
				if x-1 >= 0 {
					newCols := seats[x-1]
					if newCols[y] == GridPosSeatOccupied {
						occupied++
					}
					if y-1 >= 0 && newCols[y-1] == GridPosSeatOccupied {
						occupied++
					}
					if y+1 < len(newCols) && newCols[y+1] == GridPosSeatOccupied {
						occupied++
					}
				}
				if x+1 < len(seats) {
					newCols := seats[x+1]
					if newCols[y] == GridPosSeatOccupied {
						occupied++
					}
					if y-1 >= 0 && newCols[y-1] == GridPosSeatOccupied {
						occupied++
					}
					if y+1 < len(newCols) && newCols[y+1] == GridPosSeatOccupied {
						occupied++
					}
				}

				if val == GridPosSeatEmpty && occupied == 0 {
					toSwap = append(toSwap, tools.Pos2D{X: x, Y: y})
				} else if val == GridPosSeatOccupied && occupied >= 4 {
					toSwap = append(toSwap, tools.Pos2D{X: x, Y: y})
				}
			}
		}

		for _, swap := range toSwap {
			switch seats[swap.X][swap.Y] {
			case GridPosSeatEmpty:
				seats[swap.X][swap.Y] = GridPosSeatOccupied
			case GridPosSeatOccupied:
				seats[swap.X][swap.Y] = GridPosSeatEmpty
			default:
				return "", errors.New("invalid swap pos")
			}
		}
		if len(toSwap) == 0 {
			count := 0
			for _, cols := range seats {
				for _, val := range cols {
					if val == GridPosSeatOccupied {
						count++
					}
				}
			}
			return strconv.Itoa(count), nil
		}
	}
}

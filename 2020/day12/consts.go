// Package day12 2020
package day12

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "2297"
const testAnswerTwo = "89984"

var testFilename = "../test/private/" + tools.GetPackageName()

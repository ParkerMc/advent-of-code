// Package day08 2018
package day08

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "42472"
const testAnswerTwo = "21810"

var testFilename = "../test/private/" + tools.GetPackageName()

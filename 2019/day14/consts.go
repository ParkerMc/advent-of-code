// Package day14 2019
package day14

import "gitlab.com/parkermc/advent-of-code/2019/tools"

const testAnswerOne = "399063"
const testAnswerTwo = "4215654"

var testFilename = "../test/private/" + tools.GetPackageName()

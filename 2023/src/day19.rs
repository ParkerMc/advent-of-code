use std::{collections::HashMap, time::Duration, u64, vec};

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    let (rules, items) = load_data(input);
    items
        .iter()
        .filter(|i| {
            let mut rule_name = "in";
            while let Some(rule) = rules.get(rule_name) {
                if rule.gt < i.get(&rule.var) && rule.lt > i.get(&rule.var) {
                    rule_name = &rule.dest;
                } else {
                    rule_name = &rule.default;
                }
            }
            rule_name == "A"
        })
        .map(|i| (i.x + i.m + i.a + i.s))
        .sum::<u64>()
        .to_string()
}

fn part2(input: &Vec<String>) -> String {
    // let (rules, _) = load_data(input);
    // let answer = 0;
    // let mut queue: Vec<QueueEntry> = vec![];
    // while queue.len() > 0 {
    //     let cur = queue.pop().unwrap();
    //     let (conditions, default) = match cur.next {
    //         QueueNextEntry::Rule(rn) => rules.get(rn).map(|r| (r.conditions, r.default)).unwrap(),
    //         QueueNextEntry::Conditions(c, d) => (c, d),
    //     };
    // }
    String::from("")
}

// struct QueueEntry {
//     min: Item,
//     max: Item,
//     // next: QueueNextEntry,
// }

// enum QueueNextEntry<'a> {
//     Rule(&'a str),
//     Conditions(Vec<Condition>, &'a str),
// }

// impl QueueEntry {
//     fn new(next: String) -> Self {
//         Self {
//             min: Item {
//                 x: 1,
//                 m: 1,
//                 a: 1,
//                 s: 1,
//             },
//             max: Item {
//                 x: 4000,
//                 m: 4000,
//                 a: 4000,
//                 s: 400,
//             },
//             next,
//         }
//     }
// }

#[derive(Clone, Copy)]
enum Var {
    X,
    M,
    A,
    S,
}

impl Var {
    fn from(c: char) -> Result<Self, String> {
        match c {
            'x' => Ok(Self::X),
            'm' => Ok(Self::M),
            'a' => Ok(Self::A),
            's' => Ok(Self::S),
            _ => Err("Failed to parse char to var name".to_owned()),
        }
    }
}

#[derive(Debug)]
struct Item {
    x: u64,
    m: u64,
    a: u64,
    s: u64,
}

impl Item {
    fn get(&self, var: &Var) -> u64 {
        match var {
            Var::X => self.x,
            Var::M => self.m,
            Var::A => self.a,
            Var::S => self.s,
        }
    }
}

struct Condition {
    var: Var,
    gt: u64,
    lt: u64,
    dest: String,
}

struct Rule {
    var: Var,
    gt: u64,
    lt: u64,
    dest: String,
    default: String,
}

fn load_data(input: &Vec<String>) -> (HashMap<String, Rule>, Vec<Item>) {
    let mut loading_rules = true;
    let mut items: Vec<Item> = vec![];
    let mut rules = HashMap::new();

    for line in input {
        if line.is_empty() {
            loading_rules = false;
            continue;
        }
        if loading_rules {
            let split = line.split_once('{').expect("expected split");
            let d_split = split
                .1
                .strip_suffix('}')
                .expect("Expented input to end with \"}\"")
                .rsplit_once(',')
                .expect("Expected split for default");
            let conditions = d_split
                .0
                .split(',')
                .map(|s| {
                    let c_split = s.split_once(':').expect("Failed to split with condition");
                    let num = c_split.0[2..]
                        .parse()
                        .expect("Expected number in condition");
                    match c_split.0.chars().nth(1).expect("Expect < or >") {
                        '>' => Condition {
                            gt: num,
                            lt: 4001,
                            var: Var::from(c_split.0.chars().next().expect("Expected char"))
                                .expect("Expected valid char for Var"),
                            dest: c_split.1.to_string(),
                        },
                        '<' => Condition {
                            gt: 0,
                            lt: num,
                            var: Var::from(c_split.0.chars().next().expect("Expected char"))
                                .expect("Expected valid char for Var"),
                            dest: c_split.1.to_string(),
                        },
                        _ => panic!("Expected < or >"),
                    }
                })
                .collect_vec();
            let mut i = 0;
            for c in conditions.iter() {
                rules.insert(
                    if i == 0 {
                        split.0.to_string()
                    } else {
                        split.0.to_string() + i.to_string().as_str()
                    },
                    Rule {
                        var: c.var,
                        gt: c.gt,
                        lt: c.lt,
                        dest: c.dest.clone(),
                        default: if i == conditions.len() - 1 {
                            d_split.1.to_owned()
                        } else {
                            split.0.to_string() + (i + 1).to_string().as_str()
                        },
                    },
                );
                i += 1;
            }
            continue;
        }
        let nums: (u64, u64, u64, u64) = line
            .strip_suffix('}')
            .expect("Expented input to end with \"}\"")
            .split(',')
            .map(|s| {
                s.split_once('=')
                    .expect("Expected split")
                    .1
                    .parse::<u64>()
                    .expect("Expected number")
            })
            .collect_tuple()
            .expect("Expected tuple");
        items.push(Item {
            x: nums.0,
            m: nums.1,
            a: nums.2,
            s: nums.3,
        })
    }

    (rules, items)
}

// Package day18 2018
package day18

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "360720"
const testAnswerTwo = "197276"

var testFilename = "../test/private/" + tools.GetPackageName()

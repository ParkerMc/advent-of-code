package day10

// Two the second puzzle for the day
func Two(filename string) (string, error) {
	_, answer, err := solver(filename)
	return answer, err // Return with the answer
}

package day16

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

var hexTable = map[rune]string{
	'0': "0000",
	'1': "0001",
	'2': "0010",
	'3': "0011",
	'4': "0100",
	'5': "0101",
	'6': "0110",
	'7': "0111",
	'8': "1000",
	'9': "1001",
	'A': "1010",
	'B': "1011",
	'C': "1100",
	'D': "1101",
	'E': "1110",
	'F': "1111",
}

type packet struct {
	version    int
	typeID     int
	val        int
	subPackets []packet
}

func binToNumber(bin string) int {
	out := 0
	for _, val := range bin {
		out <<= 1
		out += int(val) - 48
	}
	return out
}

func readPacket(bin string) (packet, int) {
	newPacket := packet{
		version: binToNumber(bin[0:3]),
		typeID:  binToNumber(bin[3:6]),
	}
	i := 6
	if newPacket.typeID == 4 {
		number := ""

		for ; i < len(bin); i += 5 {
			number += bin[i+1 : i+5]
			if bin[i] == '0' {
				break
			}
		}
		i += 5
		newPacket.val = binToNumber(number)
	} else {
		if bin[i] == '0' {
			// next 15 bits is the total legnth
			endOfPacket := binToNumber(bin[i+1 : i+16])
			i += 16
			subPackets := bin[i : i+endOfPacket]
			for len(subPackets) > 0 {
				packet, j := readPacket(subPackets)
				newPacket.subPackets = append(newPacket.subPackets, packet)
				subPackets = subPackets[j:]
			}
			i += endOfPacket
		} else {
			// next 11 bits is the number of packets
			numPackets := binToNumber(bin[i+1 : i+12])
			i += 12
			for k := 0; k < numPackets; k++ {
				packet, j := readPacket(bin[i:])
				i += j
				newPacket.subPackets = append(newPacket.subPackets, packet)
			}
		}

	}

	return newPacket, i
}

func sumVersions(p packet) int {
	out := p.version
	for _, p2 := range p.subPackets {
		out += sumVersions(p2)
	}
	return out
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	bin := ""
	for _, hex := range lines[0] {
		bin += hexTable[hex]
	}
	packet, _ := readPacket(bin)

	return strconv.Itoa(sumVersions(packet)), nil // Return with the answer
}

// Package day19 2018
package day19

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "2072"
const testAnswerTwo = "27578880"

var testFilename = "../test/private/" + tools.GetPackageName()

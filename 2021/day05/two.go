package day05

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	grid, err := loadData(lines, true)
	if err != nil {
		return "", err
	}

	return strconv.Itoa(getAnswer(grid)), nil // Return with the answer
}

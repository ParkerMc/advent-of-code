use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) {
    utils::day::run_day(part, module_path!(), part1, part2, input);
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn get_pairs(input: &Vec<String>) -> impl Iterator<Item = Vec<Vec<u32>>> + '_ {
    input.iter().map(|l| -> Vec<Vec<u32>> {
        l.split(",")
            .map(|f| {
                f.split("-")
                    .map(|f| f.parse().expect("Range not int"))
                    .collect()
            })
            .collect()
    })
}

fn part1(input: &Vec<String>) -> String {
    let count = get_pairs(input)
        .filter(|pairs| {
            (pairs[0][0] <= pairs[1][0] && pairs[0][1] >= pairs[1][1])
                || (pairs[0][0] >= pairs[1][0] && pairs[0][1] <= pairs[1][1])
        })
        .count();
    count.to_string()
}

fn part2(input: &Vec<String>) -> String {
    let count = get_pairs(input)
        .filter(|pairs| {
            (pairs[0][0] <= pairs[1][0] && pairs[0][1] >= pairs[1][0])
                || (pairs[0][0] <= pairs[1][1] && pairs[0][1] >= pairs[1][1])
                || (pairs[1][0] <= pairs[0][0] && pairs[1][1] >= pairs[0][0])
        })
        .count();
    count.to_string()
}

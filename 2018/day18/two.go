package day18

// Two the second puzzle for the day
func Two(filename string) (string, error) {
	return run(filename, 1000000000)
}

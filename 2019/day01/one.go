package day01

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	sum := 0
	for _, line := range lines {
		num, err := strconv.Atoi(line)
		if err != nil {
			return "", err
		}
		sum += num/3 - 2
	}

	return strconv.Itoa(sum), nil // Return with the answer
}

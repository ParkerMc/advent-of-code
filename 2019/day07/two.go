package day07

import (
	"log"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	nums, err := tools.CreateIntcodeData(lines[0])
	if err != nil {
		return "", err
	}

	most := 0
	for ap := 5; ap < 10; ap++ {
		for bp := 5; bp < 10; bp++ {
			if ap == bp {
				continue
			}
			for cp := 5; cp < 10; cp++ {
				if ap == cp || bp == cp {
					continue
				}
				for dp := 5; dp < 10; dp++ {
					if ap == dp || bp == dp || cp == dp {
						continue
					}
					for ep := 5; ep < 10; ep++ {
						if ap == ep || bp == ep || cp == ep || dp == ep {
							continue
						}
						a := tools.NewProgramFromData(nums)
						b := tools.NewProgramFromData(nums)
						c := tools.NewProgramFromData(nums)
						d := tools.NewProgramFromData(nums)
						e := tools.NewProgramFromData(nums)
						a.AddInput(ap)
						b.AddInput(bp)
						c.AddInput(cp)
						d.AddInput(dp)
						e.AddInput(ep)
						pros := []*tools.IntcodeProgram{a, b, c, d, e}

						i := 0
						in := 0
						lastStarted := false
						for (!lastStarted) || !pros[4].GetStopped() {
							if (!lastStarted) && i == 4 {
								lastStarted = true
							}
							pros[i].AddInput(in)
							err = pros[i].Run()
							if err != nil {
								return "", err
							}
							in, err = pros[i].RemoveOutput()
							if err != nil {
								log.Print(i)
								return "", err
							}
							i++
							if i == 5 {
								i = 0
							}
						}
						if in > most {
							most = in
						}
					}
				}
			}
		}
	}

	return strconv.Itoa(most), nil // Return with the answer
}

package day08

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

func addMetaData(data []string, start int) (indexUsed int, answer int, err error) {
	answer = 0
	indexUsed = 2
	childrenCount, err := strconv.Atoi(data[start])
	if err != nil {
		return 0, 0, err
	}
	metadataCount, err := strconv.Atoi(data[start+1])
	if err != nil {
		return 0, 0, err
	}
	for i := 0; i < childrenCount; i++ {
		used, add, err := addMetaData(data, start+indexUsed)
		if err != nil {
			return 0, 0, err
		}
		answer += add
		indexUsed += used
	}
	for i := 0; i < metadataCount; i++ {
		add, err := strconv.Atoi(data[start+indexUsed])
		if err != nil {
			return 0, 0, nil
		}
		answer += add
		indexUsed++
	}
	return indexUsed, answer, nil
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	data := strings.Split(lines[0], " ")
	_, answer, err := addMetaData(data, 0)
	if err != nil {
		return "", err
	}
	return strconv.Itoa(answer), nil // Return with the answer
}

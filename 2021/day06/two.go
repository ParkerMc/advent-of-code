package day06

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	answer, err := runForDays(lines[0], 256)
	if err != nil {
		return "", err
	}

	return strconv.Itoa(answer), nil // Return with the answer
}

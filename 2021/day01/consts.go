// Package day01 2018
package day01

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "1616"
const testAnswerTwo = "1645"

var testFilename = "../test/private/" + tools.GetPackageName()

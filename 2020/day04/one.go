package day04

import (
	"regexp"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

type id struct {
	byr string
	iyr string
	eyr string
	hgt string
	hcl string
	ecl string
	pid string
	cid string
}

func (i *id) isValid() bool { // removed || i.cid == ""
	return (i.byr != "" &&
		i.iyr != "" &&
		i.eyr != "" &&
		i.hgt != "" &&
		i.hcl != "" &&
		i.ecl != "" &&
		i.pid != "")
}

func getValidIDs(lines []string, firstPuzzle bool) int {
	reg := regexp.MustCompile(`([^:]*):([^\s]*)\s*`)

	valid := 0
	obj := id{}
	for _, line := range lines {
		if line == "" {
			if (firstPuzzle && obj.isValid()) || (!firstPuzzle && obj.isValid2()) {
				valid++
			}
			obj = id{}
		} else {
			for _, match := range reg.FindAllStringSubmatch(line, -1) {
				switch match[1] {
				case "byr":
					obj.byr = match[2]
				case "iyr":
					obj.iyr = match[2]
				case "eyr":
					obj.eyr = match[2]
				case "hgt":
					obj.hgt = match[2]
				case "hcl":
					obj.hcl = match[2]
				case "ecl":
					obj.ecl = match[2]
				case "pid":
					obj.pid = match[2]
				case "cid":
					obj.cid = match[2]
				}
			}
		}
	}
	if (firstPuzzle && obj.isValid()) || (!firstPuzzle && obj.isValid2()) {
		valid++
	}
	return valid
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	return strconv.Itoa(getValidIDs(lines, true)), nil // Return with the answer
}

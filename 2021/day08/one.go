package day08

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	answer := 0
	for _, line := range lines {
		split := strings.Split(line, " | ")
		split2 := strings.Split(split[1], " ")
		for _, segment := range split2 {
			length := len(segment)
			if length == 2 || length == 4 || length == 3 || length == 7 {
				answer++
			}
		}
	}

	return strconv.Itoa(answer), nil // Return with the answer
}

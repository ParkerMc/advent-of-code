package day22

import (
	"container/heap"
	"errors"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

type queueEntry struct {
	pos  tools.Pos2D
	val  int
	gear item
}

type item int

const (
	none        item = 0
	torch       item = 1
	clibingGear item = 2
)

func (i item) isValid(area int) bool {
	for _, vI := range validGear[area] {
		if vI == i {
			return true
		}
	}
	return false
}

func (i item) otherValid(area int) item {
	for _, vI := range validGear[area] {
		if vI != i {
			return vI
		}
	}
	return i // Should never get here
}

var validGear = map[int][]item{rocky: {clibingGear, torch}, wet: {clibingGear, none}, narrow: {torch, none}}

// Two the second puzzle for the day
func Two(filename string) (string, error) {
	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	split := strings.Split(lines[1][8:], ",")
	target := tools.Pos2D{}
	target.X, err = strconv.Atoi(split[0])
	if err != nil {
		return "", err
	}
	target.Y, err = strconv.Atoi(split[1])
	if err != nil {
		return "", err
	}

	size := target
	size.X *= 10
	size.Y *= 10

	grid, _, err := generateGrid(lines, size)
	if err != nil {
		return "", err
	}

	// tools.GridSlicePrint(grid, map[int]string{rocky: ".", wet: "=", narrow: "|"})

	queue := make(tools.PriorityQueue, 0)
	heap.Init(&queue)
	heap.Push(&queue, &tools.PriorityQueueItem{
		Value:    queueEntry{pos: tools.Pos2D{X: 0, Y: 0}, val: 0, gear: torch},
		Priority: 0,
	})
	outGrid := make([][][]int, 3)
	for i := range outGrid {
		subGrid := make([][]int, len(grid))
		outGrid[i] = subGrid
		for x := range subGrid {
			subGrid[x] = make([]int, len(grid[0]))
		}
	}
	outGrid[torch][0][0] = 1
	for queue.Len() > 0 {
		item := heap.Pop(&queue).(*tools.PriorityQueueItem).Value.(queueEntry)
		if outGrid[item.gear][item.pos.X][item.pos.Y] != 0 && outGrid[item.gear][item.pos.X][item.pos.Y] <= item.val {
			continue
		}
		outGrid[item.gear][item.pos.X][item.pos.Y] = item.val
		if item.pos.X == target.X && item.pos.Y == target.Y && item.gear == torch {
			return strconv.Itoa(item.val), nil
		}
		// Swap gear
		otherItem := item.gear.otherValid(grid[item.pos.X][item.pos.Y])
		if outGrid[otherItem][item.pos.X][item.pos.Y] == 0 {
			newVal := item.val + 7
			heap.Push(&queue, &tools.PriorityQueueItem{
				Value:    queueEntry{pos: item.pos, val: newVal, gear: otherItem},
				Priority: newVal + target.MDist(&item.pos),
			})
		}
		// Cells around
		for _, posAdd := range []tools.Pos2D{{X: 1, Y: 0}, {X: -1, Y: 0}, {X: 0, Y: 1}, {X: 0, Y: -1}} {
			nextPos := item.pos.Add(posAdd)
			if nextPos.X < 0 || nextPos.Y < 0 || nextPos.X >= len(grid) || nextPos.Y >= len(grid[0]) {
				continue
			}
			gridVal := grid[nextPos.X][nextPos.Y]
			curOutGrid := outGrid[item.gear][nextPos.X][nextPos.Y]
			newVal := item.val + 1
			if (curOutGrid != 0 && curOutGrid <= newVal) || !item.gear.isValid(gridVal) {
				continue
			}
			heap.Push(&queue, &tools.PriorityQueueItem{
				Value:    queueEntry{pos: nextPos, val: newVal, gear: item.gear},
				Priority: newVal + target.MDist(&nextPos),
			})
		}
	}
	return "", errors.New("queue ran out")
}

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) {
    utils::day::run_day(part, module_path!(), part1, part2, input);
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }

    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    input
        .iter()
        .map(|l| l.split(" ").map(RPS::from_str).collect::<Vec<RPS>>())
        .map(|arr| arr[1].score() + arr[1].result(&arr[0]).score())
        .sum::<i64>()
        .to_string()
}

pub fn part2(input: &Vec<String>) -> String {
    input
        .iter()
        .map(|l| l.split(" "))
        .map(|mut f| {
            (
                RPS::from_str(f.next().expect("Expected A,B,or,C")),
                RpsResult::from_str(f.next().expect("Expected X,Y, or Z")),
            )
        })
        .map(|t| t.1.score() + RPS::from_result(&t.1, &t.0).score())
        .sum::<i64>()
        .to_string()
}

#[derive(PartialEq)]
enum RpsResult {
    Lose = 0,
    Draw = 3,
    Win = 6,
}
impl RpsResult {
    fn from_str(input: &str) -> Self {
        match input {
            "X" => Self::Lose,
            "Y" => Self::Draw,
            "Z" => Self::Win,
            _ => panic!("Rock, Paper, Scissors result {}", input),
        }
    }
    fn score(&self) -> i64 {
        match self {
            Self::Lose => 0,
            Self::Draw => 3,
            Self::Win => 6,
        }
    }
}

#[derive(PartialEq, Clone, Copy)]
enum RPS {
    Rock,
    Paper,
    Scissors,
}
impl RPS {
    fn from_str(input: &str) -> Self {
        match input {
            "A" | "X" => Self::Rock,
            "B" | "Y" => Self::Paper,
            "C" | "Z" => Self::Scissors,
            _ => panic!("Unknown Rock Paper or Scissors {}", input),
        }
    }

    fn from_result(result: &RpsResult, opponent: &Self) -> Self {
        match (result, opponent) {
            (RpsResult::Win, Self::Rock) => Self::Paper,
            (RpsResult::Win, Self::Paper) => Self::Scissors,
            (RpsResult::Win, Self::Scissors) => Self::Rock,

            (RpsResult::Lose, Self::Rock) => Self::Scissors,
            (RpsResult::Lose, Self::Paper) => Self::Rock,
            (RpsResult::Lose, Self::Scissors) => Self::Paper,
            (RpsResult::Draw, _) => opponent.clone(),
        }
    }

    fn result(&self, opponent: &Self) -> RpsResult {
        if self == opponent {
            return RpsResult::Draw;
        }
        match self {
            Self::Paper if matches!(opponent, Self::Rock) => RpsResult::Win,
            Self::Rock if matches!(opponent, Self::Scissors) => RpsResult::Win,
            Self::Scissors if matches!(opponent, Self::Paper) => RpsResult::Win,
            _ => RpsResult::Lose,
        }
    }

    fn score(&self) -> i64 {
        match self {
            Self::Rock => 1,
            Self::Paper => 2,
            Self::Scissors => 3,
        }
    }
}

package day14

import (
	"fmt"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	bitmask := ""
	nums := make(map[int64]int64)
	for _, line := range lines {
		match := reg.FindStringSubmatch(line)
		switch match[1] {
		case "mask":
			bitmask = match[3]
		case "mem":
			firstAddr, err := strconv.Atoi(match[2])
			if err != nil {
				return "", err
			}
			addrs := []int64{int64(firstAddr)}
			for i, c := range bitmask {
				switch c {
				case 'X':
					newAddrs := make([]int64, len(addrs)*2)
					for j, addr := range addrs {
						newAddrs[j*2] = setBit(addr, 35-i)
						newAddrs[j*2+1] = clearBit(addr, 35-i)
					}
					addrs = newAddrs
				case '0':
				case '1':
					for j, addr := range addrs {
						addrs[j] = setBit(addr, 35-i)
					}
				default:
					return "", fmt.Errorf("invalid bitmask char: %c", c)
				}
			}
			num, err := strconv.ParseInt(match[3], 10, 0)
			if err != nil {
				return "", err
			}
			for _, addr := range addrs {
				nums[addr] = num
			}
		default:
			return "", fmt.Errorf("unknown thing to set: %s", match[1])
		}
	}

	sum := int64(0)
	for _, val := range nums {
		sum += val
	}

	return strconv.FormatInt(sum, 10), nil // Return with the answer
}

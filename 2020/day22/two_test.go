package day22

import "testing"

func TestTwo(t *testing.T) {
	answer, err := Two(testFilename)
	if err != nil {
		t.Fatal(err)
	}
	if answer != testAnswerTwo {
		t.Fatalf("Answer should be \"%s\" but was \"%s\"", testAnswerTwo, answer)
	}
}

func BenchmarkTwo(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Two(testFilename)
	}
}

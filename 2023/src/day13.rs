use std::time::Duration;

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn solve(input: &Vec<String>, off_by: usize) -> usize {
    let maps = input
        .split(|l| l.is_empty())
        .map(|m| m.iter().map(|l| l.chars().collect_vec()).collect_vec())
        .collect_vec();

    maps.iter()
        .map(|m| {
            let x_len = m[0].len();
            // Vertical line
            let mut possible = (0..(x_len - 1)).map(|mid| (mid, 0)).collect_vec();
            for l in m {
                possible.retain_mut(|(mid, cur)| {
                    for step in 0..=(*mid).min(x_len - *mid - 2) {
                        if l[*mid - step] != l[*mid + step + 1] {
                            *cur += 1;
                            if *cur > off_by {
                                return false;
                            }
                        }
                    }
                    true
                });
                if possible.is_empty() {
                    break;
                }
            }
            if let Some(res) = possible.iter().find(|(_, cur)| *cur == off_by) {
                return res.0 + 1;
            }

            possible = (0..(m.len() - 1)).map(|mid| (mid, 0)).collect_vec();
            for x in 0..x_len {
                possible.retain_mut(|(mid, cur)| {
                    for step in 0..=(*mid).min(m.len() - *mid - 2) {
                        if m[*mid - step][x] != m[*mid + step + 1][x] {
                            *cur += 1;
                            if *cur > off_by {
                                return false;
                            }
                        }
                    }
                    true
                });
                if possible.is_empty() {
                    break;
                }
            }
            if let Some(res) = possible.iter().find(|(_, cur)| *cur == off_by) {
                return (res.0 + 1) * 100;
            }
            0
        })
        .sum::<usize>()
}

// 27505

fn part1(input: &Vec<String>) -> String {
    solve(input, 0).to_string()
}

fn part2(input: &Vec<String>) -> String {
    solve(input, 1).to_string()
}

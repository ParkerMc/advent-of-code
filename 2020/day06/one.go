package day06

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRequireLast(filename)
	if err != nil {
		return "", err
	}

	count := 0
	group := make(map[rune]bool, 26)
	for _, line := range lines {
		if line == "" {
			for range group {
				count++
			}
			group = make(map[rune]bool, 26)
			continue
		}
		for _, c := range line {
			group[c] = true
		}
	}

	return strconv.Itoa(count), nil // Return with the answer
}

package day21

import (
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

var inputReg = regexp.MustCompile(`^(.+) \(contains (.+)\)$`)

func part1(lines []string) (map[string]int, map[string][]string) {
	foodTimes := make(map[string]int, 0)
	allerginToFood := make(map[string][]string, 0)
	for _, line := range lines {
		reg := inputReg.FindStringSubmatch(line)
		inFoods := strings.Split(reg[1], " ")
		for _, food := range inFoods {
			foodTimes[food]++
		}
		for _, allergin := range strings.Split(reg[2], ", ") {
			if foods, ok := allerginToFood[allergin]; ok {
				toRemove := make([]int, 0)
				for i, food := range foods {
					contains := false
					for _, inFood := range inFoods {
						if inFood == food {
							contains = true
						}
					}
					if !contains {
						toRemove = append(toRemove, i)
					}
				}
				for i, remove := range toRemove {
					allerginToFood[allergin] = append(allerginToFood[allergin][:remove-i], allerginToFood[allergin][remove-i+1:]...)
				}
			} else {
				allerginToFood[allergin] = make([]string, len(inFoods))
				copy(allerginToFood[allergin], inFoods)
			}
		}

	}
	return foodTimes, allerginToFood
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	foodTimes, allerginToFood := part1(lines)

	out := 0
	for food, times := range foodTimes {
		good := true
		for _, list := range allerginToFood {
			for _, item := range list {
				if item == food {
					good = false
					break
				}
			}
			if !good {
				break
			}
		}
		if good {
			out += times
		}
	}

	return strconv.Itoa(out), nil // Return with the answer
}

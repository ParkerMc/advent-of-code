package day05

import (
	"errors"
	"math"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	found := make([]bool, 128*8)

	for _, line := range lines {
		rowHigh, rowLow := 127, 0
		colHigh, colLow := 7, 0
		for _, c := range line {
			switch c {
			case 'B':
				rowLow = int(math.Ceil((float64(rowHigh)-float64(rowLow))/2.0)) + rowLow
			case 'F':
				rowHigh = (rowHigh-rowLow)/2 + rowLow
			case 'R':
				colLow = int(math.Ceil((float64(colHigh)-float64(colLow))/2.0)) + colLow
			case 'L':
				colHigh = (colHigh-colLow)/2 + colLow
			}
		}
		if rowLow == rowHigh && colLow == colHigh {
			row := rowLow
			col := colLow
			num := row*8 + col
			found[num] = true
		} else {
			return "", errors.New("error with seat code")
		}
	}

	for i, seat := range found {
		if !seat && i > 0 && found[i-1] && i+1 < len(found) && found[i+1] {
			return strconv.Itoa(i), nil
		}
	}

	return "", errors.New("couldn't find seat") // Return with the answer
}

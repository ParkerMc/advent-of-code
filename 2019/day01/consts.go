// Package day01 2019
package day01

import "gitlab.com/parkermc/advent-of-code/2019/tools"

const testAnswerOne = "3167282"
const testAnswerTwo = "4748063"

var testFilename = "../test/private/" + tools.GetPackageName()

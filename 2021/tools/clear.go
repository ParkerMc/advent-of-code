// Package tools A set of tools for multiple days
package tools

import (
	"log"
	"os"
	"os/exec"
	"runtime"
)

var clearFuncs map[string]func() //create a map for storing clear funcs

func init() {
	clearFuncs = make(map[string]func()) //Initialize it
	clearFuncs["linux"] = func() {
		cmd := exec.Command("clear") //Linux example, its tested
		cmd.Stdout = os.Stdout
		err := cmd.Run()
		if err != nil {
			log.Print(err)
		}
	}
	clearFuncs["windows"] = func() {
		cmd := exec.Command("cmd", "/c", "cls") //Windows example, its tested
		cmd.Stdout = os.Stdout
		err := cmd.Run()
		if err != nil {
			log.Print(err)
		}
	}
}

// ClearOutput clears the screen
func ClearOutput() {
	value, ok := clearFuncs[runtime.GOOS] //runtime.GOOS -> linux, windows, darwin etc.
	if ok {                               //if we defined a clear func for that platform:
		value() //we execute it
	} else { //unsupported platform
		panic("Your platform is unsupported! I can't clear terminal screen :(")
	}
}

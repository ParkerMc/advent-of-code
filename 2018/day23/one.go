package day23

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

type nanobot struct {
	pos    tools.Pos3D
	rangen int
}

func loadData(lines []string) ([]*nanobot, error) {
	nanobots := make([]*nanobot, 0)
	for _, line := range lines {
		rSplit := strings.Split(line, "r=")
		r, err := strconv.Atoi(rSplit[1])
		if err != nil {
			return nil, err
		}

		posSplit := strings.Split(strings.Split(strings.Split(line, "<")[1], ">")[0], ",")

		x, err := strconv.Atoi(posSplit[0])
		if err != nil {
			return nil, err
		}
		y, err := strconv.Atoi(posSplit[1])
		if err != nil {
			return nil, err
		}
		z, err := strconv.Atoi(posSplit[2])
		if err != nil {
			return nil, err
		}

		nanobots = append(nanobots, &nanobot{
			rangen: r,
			pos:    tools.Pos3D{X: x, Y: y, Z: z},
		})
	}
	return nanobots, nil
}

// One the first puzzle for the day
func One(filename string) (string, error) {
	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	nanobots, err := loadData(lines)
	if err != nil {
		return "", err
	}

	maxV := 0
	maxI := 0
	for i, bot := range nanobots {
		if bot.rangen > maxV {
			maxV = bot.rangen
			maxI = i
		}
	}

	mainBot := nanobots[maxI]
	count := 0
	for _, bot := range nanobots {
		if bot.pos.MDist(&mainBot.pos) <= mainBot.rangen {
			count++
		}
	}

	return strconv.Itoa(count), nil
}

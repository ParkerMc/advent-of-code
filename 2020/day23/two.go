package day23

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	cups, cupMap, highest, err := load(lines, true)
	if err != nil {
		return "", err
	}

	toAdd := 1000000 - cups.Len()
	for i := 0; i < toAdd; i++ {
		highest++
		cupMap[highest] = cups.PushBack(highest)
	}

	solve(cups, cupMap, highest, 10000000)

	out := cupMap[1].Next().Value.(int) * cupMap[1].Next().Next().Value.(int)

	return strconv.Itoa(out), nil // Return with the answer
}

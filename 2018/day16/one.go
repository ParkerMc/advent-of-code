package day16

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	answer := 0
	for i, line := range lines {
		if strings.Contains(line, "Before") {
			beforeStr := strings.Split(strings.Split(strings.Split(line, "[")[1], "]")[0], ", ")
			before := make([]int, 0)
			for _, numStr := range beforeStr {
				num, err := strconv.Atoi(numStr)
				if err != nil {
					return "", err
				}
				before = append(before, num)
			}
			/*opcode, err := strconv.Atoi(strings.Split(lines[i+1], " ")[0])
			if err != nil {
				return "", err
			}*/
			a, err := strconv.Atoi(strings.Split(lines[i+1], " ")[1])
			if err != nil {
				return "", err
			}
			b, err := strconv.Atoi(strings.Split(lines[i+1], " ")[2])
			if err != nil {
				return "", err
			}
			c, err := strconv.Atoi(strings.Split(lines[i+1], " ")[3])
			if err != nil {
				return "", err
			}
			afterStr := strings.Split(strings.Split(strings.Split(lines[i+2], "[")[1], "]")[0], ", ")
			after := make([]int, 0)
			for _, numStr := range afterStr {
				num, err := strconv.Atoi(numStr)
				if err != nil {
					return "", err
				}
				after = append(after, num)
			}
			behaveLike := 0

			// addr
			if before[a]+before[b] == after[c] {
				behaveLike++
			}
			//addi
			if before[a]+b == after[c] {
				behaveLike++
			}

			// mulr
			if before[a]*before[b] == after[c] {
				behaveLike++
			}
			//muli
			if before[a]*b == after[c] {
				behaveLike++
			}

			// banr
			if before[a]&before[b] == after[c] {
				behaveLike++
			}
			//bani
			if before[a]&b == after[c] {
				behaveLike++
			}

			// borr
			if before[a]|before[b] == after[c] {
				behaveLike++
			}
			//bori
			if before[a]|b == after[c] {
				behaveLike++
			}

			// setr
			if before[a] == after[c] {
				behaveLike++
			}
			//seti
			if a == after[c] {
				behaveLike++
			}

			//gtir
			if (after[c] == 1 && a > before[b]) || (after[c] == 0 && a <= before[b]) {
				behaveLike++
			}
			//gtri
			if (after[c] == 1 && before[a] > b) || (after[c] == 0 && before[a] <= b) {
				behaveLike++
			}
			//gtrr
			if (after[c] == 1 && before[a] > before[b]) || (after[c] == 0 && before[a] <= before[b]) {
				behaveLike++
			}

			//eqir
			if (after[c] == 1 && a == before[b]) || (after[c] == 0 && a != before[b]) {
				behaveLike++
			}
			//eqri
			if (after[c] == 1 && before[a] == b) || (after[c] == 0 && before[a] != b) {
				behaveLike++
			}
			//eqrr
			if (after[c] == 1 && before[a] == before[b]) || (after[c] == 0 && before[a] != before[b]) {
				behaveLike++
			}

			if behaveLike >= 3 {
				answer++
			}
		}
	}
	return strconv.Itoa(answer), nil // Return with the answer
}

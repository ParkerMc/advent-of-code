package day18

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	maxMagnitude := 0
	for i, line1 := range lines {
		for j, line2 := range lines {
			if i == j {
				continue
			}
			list1, _ := loadList(line1)
			list2, _ := loadList(line2)

			newList := list1.add(list2)
			newList.reduce()
			magnitude := newList.magnitude()
			if magnitude > maxMagnitude {
				maxMagnitude = magnitude
			}
		}
	}

	return strconv.Itoa(maxMagnitude), nil // Return with the answer
}

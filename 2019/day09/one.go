package day09

import (
	"errors"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	p, err := tools.NewProgram(lines[0])
	if err != nil {
		return "", err
	}

	p.AddInput(1)

	err = p.Run()
	if err != nil {
		return "", err
	}

	if !p.GetStopped() {
		return "", errors.New("program not stopped")
	}

	data, err := p.RemoveOutput()
	if err != nil {
		return "", err
	}

	return strconv.Itoa(data), nil // Return with the answer
}

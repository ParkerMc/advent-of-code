package tools

import (
	"testing"
)

// TestTrimFirstChar Tests the trimFirstChar function
func TestTrimFirstChar(t *testing.T) {
	if a, b := TrimFirstChar("1"); a != "1" || b != "" {
		t.Fatal("trimFirstChar returned wrong value")
	}

	if a, b := TrimFirstChar(""); a != "" || b != "" {
		t.Fatal("trimFirstChar returned wrong value")
	}

	if a, b := TrimFirstChar("abcde"); a != "a" || b != "bcde" {
		t.Fatal("trimFirstChar returned wrong value")
	}
}

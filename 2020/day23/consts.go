// Package day23 2020
package day23

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "35827964"
const testAnswerTwo = "5403610688"

var testFilename = "../test/private/" + tools.GetPackageName()

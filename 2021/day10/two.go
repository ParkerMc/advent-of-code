package day10

import (
	"sort"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

var scores2 = map[rune]int{')': 1, ']': 2, '}': 3, '>': 4}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	realScores := make([]int, 0)
	for _, line := range lines {
		symbolStack := make([]rune, 0)
		corrupted := false
		for _, r := range line {
			switch r {
			case '(':
				symbolStack = append(symbolStack, ')')
			case '[':
				symbolStack = append(symbolStack, ']')
			case '{':
				symbolStack = append(symbolStack, '}')
			case '<':
				symbolStack = append(symbolStack, '>')
			case '>':
				fallthrough
			case ']':
				fallthrough
			case '}':
				fallthrough
			case ')':
				poped := symbolStack[len(symbolStack)-1]
				symbolStack = symbolStack[:len(symbolStack)-1]
				if poped != r {
					corrupted = true
				}
			}
			if corrupted {
				break
			}
		}
		if !corrupted {
			score := 0
			for len(symbolStack) > 0 {
				symbol := symbolStack[len(symbolStack)-1]
				symbolStack = symbolStack[:len(symbolStack)-1]
				score *= 5
				score += scores2[symbol]
			}
			realScores = append(realScores, score)
		}
	}

	sort.Ints(realScores)
	for len(realScores) > 1 {
		realScores = realScores[1 : len(realScores)-1]
	}

	return strconv.Itoa(realScores[0]), nil // Return with the answer
}

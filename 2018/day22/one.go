package day22

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

const (
	rocky  = 0
	wet    = 1
	narrow = 2
)

func generateGrid(lines []string, size tools.Pos2D) (grid [][]int, answer int, err error) {
	depth, err := strconv.Atoi(lines[0][7:])
	if err != nil {
		return nil, 0, err
	}
	split := strings.Split(lines[1][8:], ",")
	target := tools.Pos2D{}
	target.X, err = strconv.Atoi(split[0])
	if err != nil {
		return nil, 0, err
	}
	target.Y, err = strconv.Atoi(split[1])
	if err != nil {
		return nil, 0, err
	}

	if size.X == 0 && size.Y == 0 {
		size = target
		size.X++
		size.Y++
	}

	grid = make([][]int, size.X)
	xAdd := 16807 % 20183
	xLastIndex := depth
	for x := range grid {
		var prevCol []int
		if x > 0 {
			prevCol = grid[x-1]
		}
		col := make([]int, size.Y)
		grid[x] = col
		if x == 0 {
			yAdd := 48271 % 20183
			yLastIndex := depth - yAdd
			for y := range col {
				yLastIndex += yAdd
				col[y] = yLastIndex % 20183
			}
			continue
		}
		for y := range col {
			if y == 0 {
				xLastIndex += xAdd
				col[y] = xLastIndex % 20183
				continue
			}
			if x == target.X && y == target.Y {
				col[y] = depth % 20183
				continue
			}
			col[y] = ((prevCol[y] * col[y-1]) + depth) % 20183
		}
	}

	answer = 0
	for _, col := range grid {
		for y, val := range col {
			modVal := val % 3
			col[y] = modVal
			answer += modVal
		}
	}
	return grid, answer, nil
}

// One the first puzzle for the day
func One(filename string) (string, error) {
	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	_, answer, err := generateGrid(lines, tools.Pos2D{})
	if err != nil {
		return "", err
	}
	// tools.GridSlicePrint(grid, map[int]string{rocky: ".", wet: "=", narrow: "|"})

	return strconv.Itoa(answer), nil // Return with the answer
}

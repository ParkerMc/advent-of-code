// Package day03 2019
package day03

import "gitlab.com/parkermc/advent-of-code/2019/tools"

const testAnswerOne = "4981"
const testAnswerTwo = "164012"

var testFilename = "../test/private/" + tools.GetPackageName()

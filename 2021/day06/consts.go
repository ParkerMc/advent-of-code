// Package day06 2018
package day06

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "366057"
const testAnswerTwo = "1653559299811"

var testFilename = "../test/private/" + tools.GetPackageName()

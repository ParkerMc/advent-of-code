package day15

import (
	"container/list"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

const (
	start = 1
	wall  = 2
	empty = 3
	o2    = 4

	north = 1
	south = 2
	west  = 3
	east  = 4

	hitWall   = 0
	moved     = 1
	modedToO2 = 2
)

type robot struct {
	pos tools.Pos2D
	dir int
}

func getPosPDir(pos tools.Pos2D, dir int) tools.Pos2D {
	switch dir {
	case north:
		return tools.Pos2D{X: pos.X, Y: pos.Y + 1}
	case south:
		return tools.Pos2D{X: pos.X, Y: pos.Y - 1}
	case west:
		return tools.Pos2D{X: pos.X - 1, Y: pos.Y}
	case east:
		return tools.Pos2D{X: pos.X + 1, Y: pos.Y}
	}
	return tools.Pos2D{}
}

func getLeft(dir int) int {
	switch dir {
	case north:
		return west
	case south:
		return east
	case west:
		return south
	case east:
		return north
	}
	return -1
}

func getRight(dir int) int {
	switch dir {
	case north:
		return east
	case south:
		return west
	case west:
		return north
	case east:
		return south
	}
	return -1
}

// func print(area map[int]map[int]int) {
// 	minX, maxX, minY, maxY := tools.GridMinMax(area)
// 	for y := maxY; y >= minY; y-- {
// 		for x := minX; x <= maxX; x++ {
// 			if val, ok := area[x]; ok && val[y] != 0 {
// 				switch val[y] {
// 				case start:
// 					fmt.Print("X")
// 				case wall:
// 					fmt.Print("#")
// 				case empty:
// 					fmt.Print(".")
// 				case o2:
// 					fmt.Print("O")
// 				}
// 			} else {
// 				fmt.Print(" ")
// 			}
// 		}
// 		fmt.Println()
// 	}
// }

func createArea(lines []string) (map[int]map[int]int, tools.Pos2D, error) {
	area := map[int]map[int]int{0: {0: start}}
	o2pos := tools.Pos2D{}

	r := robot{pos: tools.Pos2D{X: 0, Y: 0}, dir: north}
	p, err := tools.NewProgram(lines[0])
	if err != nil {
		return nil, o2pos, err
	}

	leftGo := false
	start := tools.Pos2D{X: 0, Y: 0}
	for !leftGo || r.pos != start {
		r.dir = getRight(r.dir)
		for {
			pos := getPosPDir(r.pos, r.dir)
			if val, ok := area[pos.X]; !ok || val[pos.Y] != wall {
				break
			}
			r.dir = getLeft(r.dir)
		}
		p.AddInput(r.dir)
		err = p.Run()
		if err != nil {
			return nil, o2pos, err
		}
		dat, err := p.RemoveOutput()
		if err != nil {
			return nil, o2pos, err
		}
		switch dat {
		case hitWall:
			tools.GridSet(area, getPosPDir(r.pos, r.dir), wall)
			r.dir = getLeft(r.dir)
		case moved:
			r.pos = getPosPDir(r.pos, r.dir)
			tools.GridSet(area, r.pos, empty)
		case modedToO2:
			r.pos = getPosPDir(r.pos, r.dir)
			tools.GridSet(area, r.pos, o2)
			o2pos = r.pos
		}
		if !leftGo && r.pos != start {
			leftGo = true
		}
	}
	return area, o2pos, nil
}

type distEntry struct {
	last tools.Pos2D
	z    int
}

func getDist02(area map[int]map[int]int) int {
	counted := make(map[int]map[int]bool)
	counted[0] = make(map[int]bool)
	counted[0][0] = true
	queue := list.New()
	queue.PushBack(distEntry{last: tools.Pos2D{X: 0, Y: 0}, z: 1})
	for queue.Front() != nil {
		val, _ := queue.Front().Value.(distEntry)
		last := val.last
		z := val.z
		queue.Remove(queue.Front())
		for _, p := range []tools.Pos2D{{X: 1, Y: 0}, {X: -1, Y: 0}, {X: 0, Y: 1}, {X: 0, Y: -1}} {
			if (!counted[last.X+p.X][last.Y+p.Y]) && area[last.X+p.X][last.Y+p.Y] != wall {
				if counted[last.X+p.X] == nil {
					counted[last.X+p.X] = make(map[int]bool)
				}
				counted[last.X+p.X][last.Y+p.Y] = true
				queue.PushBack(distEntry{last: tools.Pos2D{X: last.X + p.X, Y: last.Y + p.Y}, z: z + 1})
				if area[last.X+p.X][last.Y+p.Y] == o2 {
					return z
				}
			}
		}

	}
	return -1
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	area, _, err := createArea(lines)
	if err != nil {
		return "", err
	}

	return strconv.Itoa(getDist02(area)), nil // Return with the answer
}

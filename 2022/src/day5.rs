use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) {
    utils::day::run_day(part, module_path!(), part1, part2, input);
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn load_input(input: &Vec<String>) -> (Vec<Vec<String>>, usize) {
    let label_i = input
        .iter()
        .position(|l| l.trim_start().starts_with("1"))
        .expect("Failed to find labels");
    let stack_count = input[label_i].split(" ").filter(|s| !s.is_empty()).count();
    let mut stacks: Vec<Vec<String>> = Vec::with_capacity(stack_count);
    for _ in 0..stack_count {
        stacks.push(Vec::new());
    }

    let chunked_lines: Vec<Vec<String>> = input
        .iter()
        .take(label_i)
        .rev()
        .map(|l| {
            l.chars()
                .collect::<Vec<char>>()
                .chunks(4)
                .map(|c| c[1].to_string())
                .collect()
        })
        .collect();
    for line in chunked_lines {
        for (i, chunk) in line.iter().enumerate() {
            if chunk.trim().is_empty() {
                continue;
            }
            stacks[i].push(chunk.to_owned())
        }
    }
    (stacks, label_i + 2)
}

fn part1(input: &Vec<String>) -> String {
    let (mut stacks, start) = load_input(input);
    for line in input.iter().skip(start) {
        let nums: Vec<usize> = line.split(" ").filter_map(|f| f.parse().ok()).collect();
        for _ in 0..nums[0] {
            if let Some(v) = stacks[nums[1] - 1].pop() {
                stacks[nums[2] - 1].push(v);
            }
        }
    }
    stacks
        .iter()
        .filter_map(|f| f.get(f.len() - 1).map(|s| s.to_owned()))
        .collect::<Vec<String>>()
        .join("")
}

fn part2(input: &Vec<String>) -> String {
    let (mut stacks, start) = load_input(input);
    for line in input.iter().skip(start) {
        let nums: Vec<usize> = line.split(" ").filter_map(|f| f.parse().ok()).collect();
        let mut to_add: Vec<String> = (0..nums[0])
            .filter_map(|_| stacks[nums[1] - 1].pop())
            .collect();
        to_add.reverse();
        stacks[nums[2] - 1].append(&mut to_add);
    }
    stacks
        .iter()
        .filter_map(|f| f.get(f.len() - 1).map(|s| s.to_owned()))
        .collect::<Vec<String>>()
        .join("")
}

use std::{fs, path::Path};

pub fn test_day(mod_path: &str, part_str: &str, run: fn(&Vec<String>) -> String) {
    // Extract info from module path

    let day_str = mod_path
        .split("::")
        .find_map(|f| f.strip_prefix("day"))
        .expect("Failed to get day number");

    // Get a list of all the tests
    // TODO cleanup
    let base_path = String::from("resources/test/private/day") + day_str + "/";
    let files = fs::read_dir(&base_path)
        .unwrap_or_else(|_| panic!("Failed to read dir at {}", &base_path))
        .map(|f| f.expect("Failed to list file"))
        .filter(|f| f.file_type().map(|e| e.is_file()).unwrap_or(false))
        .map(|f| {
            f.file_name()
                .to_str()
                .expect("Failed to convert file name to str")
                .to_owned()
        });

    let tests = files
        .filter_map(|f| f.strip_prefix("input_").map(|f| f.to_owned()))
        .filter(|f| Path::new(&(base_path.clone() + "output" + part_str + "_" + f)).exists()); // Find only tests with out files

    // Run each test
    for test_name in tests {
        println!(
            "Running test day {} part {} test {}",
            day_str, part_str, test_name
        );
        let result = run(&super::io::read_input(
            &(base_path.clone() + "input_" + &test_name),
        ));
        let answer =
            fs::read_to_string(&(base_path.clone() + "output" + part_str + "_" + &test_name))
                .unwrap_or(String::new());
        assert_eq!(result.trim(), answer.trim(), "Failed a test puzzle");
    }
}

package day10

import (
	"math"
	"strconv"

	"github.com/golang-collections/collections/set"
	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

type asteroid struct {
	pos tools.Pos2D
}

func getAsteroids(lines []string) []*asteroid {
	asteroids := make([]*asteroid, 0)
	for y, line := range lines {
		for x, val := range line {
			if val != '.' {
				asteroids = append(asteroids, &asteroid{pos: tools.Pos2D{X: x, Y: y}})
			}
		}
	}
	return asteroids
}

func getAngle(point1, point2 tools.Pos2D) float64 {
	angle := math.Atan2(float64(point2.Y-point1.Y), float64(point2.X-point1.X))*(180/math.Pi) + 90
	if angle < 0 {
		angle += 360
	}
	return angle
}

func getMag(point1, point2 tools.Pos2D) float64 {
	return math.Sqrt(math.Pow(float64(point1.X-point2.X), 2) + math.Pow(float64(point1.Y-point2.Y), 2))
}

func getStationPos(asteroids []*asteroid) (tools.Pos2D, int) {

	sets := make([]*set.Set, len(asteroids))
	for i := 0; i < len(asteroids); i++ {
		sets[i] = set.New()
		for j := 0; j < len(asteroids); j++ {
			sets[i].Insert(getAngle(asteroids[i].pos, asteroids[j].pos))

		}
	}

	most := 0
	mostI := -1
	for i, s := range sets {
		if s.Len() > most {
			most = s.Len()
			mostI = i
		}
	}
	return asteroids[mostI].pos, most
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	_, most := getStationPos(getAsteroids(lines))
	return strconv.Itoa(most), nil // Return with the answer
}

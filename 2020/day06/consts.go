// Package day06 2020
package day06

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "7120"
const testAnswerTwo = "3570"

var testFilename = "../test/private/" + tools.GetPackageName()

package day22

import (
	"container/list"
	"log"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// TODO make faster than 1s

const (
	debug = false
)

func load(lines []string) ([]*list.List, int, int, error) {
	totalCardCount := 0
	decks := make([]*list.List, 0)

	player := -1
	for _, line := range lines {
		if strings.HasPrefix(line, "Player ") {
			player++
			decks = append(decks, list.New())
		} else if line != "" {
			card, err := strconv.Atoi(line)
			if err != nil {
				return nil, 0, 0, err
			}
			decks[player].PushBack(card)
			totalCardCount++
		}
	}
	return decks, player + 1, totalCardCount, nil

}

func printDecks(decks []*list.List) {
	for player, deck := range decks {
		log.Printf("Player %d's deck: %s", player+1, strings.Join(toStrArr(deck), ", "))
	}
}

func toStrArr(list *list.List) []string {
	out := make([]string, list.Len())
	current := list.Front()
	i := 0
	for current != nil {
		out[i] = strconv.Itoa(current.Value.(int))
		i++
		current = current.Next()
	}
	return out
}

func countWinner(decks []*list.List, playerWon int) int {
	out := 0

	length := decks[playerWon].Len()
	current := decks[playerWon].Front()
	i := 0
	for current != nil {
		out += (length - i) * current.Value.(int)
		i++
		current = current.Next()
	}
	return out
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	decks, palyerCount, totalCardCount, err := load(lines)
	if err != nil {
		return "", err
	}

	playerWon := -1
	for i := 1; playerWon == -1; i++ {
		//playerWon = 0 // TODO remove
		if debug {
			log.Printf("-- Round %d --", i)
			printDecks(decks)
		}

		currentCards := make([]int, palyerCount)
		for player, deck := range decks {
			cardE := deck.Front()
			if cardE != nil {
				deck.Remove(cardE)
				currentCards[player] = cardE.Value.(int)
				if debug {
					log.Printf("Player %d plays: %d", player+1, cardE.Value.(int))
				}
			}
		}

		highestVal := 0
		highestPlayer := 0
		for player, card := range currentCards {
			if card > highestVal {
				highestVal = card
				highestPlayer = player
			}
		}
		if debug {
			log.Printf("Player %d wins the Round!", highestPlayer+1)
		}

		sort.Sort(sort.Reverse(sort.IntSlice(currentCards)))
		for _, card := range currentCards {
			if card != 0 {
				decks[highestPlayer].PushBack(card)
			}
		}

		for player, deck := range decks {
			if deck.Len() == totalCardCount {
				playerWon = player
			}
		}
	}

	if debug {
		log.Print("== Post-game results ==")
		printDecks(decks)
	}

	return strconv.Itoa(countWinner(decks, playerWon)), nil // Return with the answer
}

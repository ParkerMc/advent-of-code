package day05

import (
	"strconv"
	"strings"
	"unicode"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

func react(data string) string {
	for {
		count := 0
		for _, letter := range "qwertyuiopasdfghjklzxcvbnm" {
			count += strings.Count(data, string([]rune{unicode.ToUpper(letter), unicode.ToLower(letter)})) + strings.Count(data, string([]rune{unicode.ToLower(letter), unicode.ToUpper(letter)}))
			data = strings.Replace(strings.Replace(data, string([]rune{unicode.ToUpper(letter), unicode.ToLower(letter)}), "", -1), string([]rune{unicode.ToLower(letter), unicode.ToUpper(letter)}), "", -1)
		}
		if count == 0 {
			return data
		}
	}
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	return strconv.Itoa(len(react(lines[0]))), nil // Return with the answer
}

use std::{collections::HashSet, time::Duration};

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    let (width, height, bots) = load(input);
    let mid_x = width / 2;
    let mid_y = height / 2;
    let mut scores = vec![0, 0, 0, 0];
    bots.iter()
        .map(|((x, y), (vx, vy))| {
            let mut ex = (x + vx * 100) % width;
            if ex < 0 {
                ex += width;
            }
            let mut ey = (y + vy * 100) % height;
            if ey < 0 {
                ey += height;
            }
            (ex, ey)
        })
        .for_each(|(x, y)| {
            if x < mid_x {
                if y < mid_y {
                    scores[0] += 1;
                    return;
                } else if y == mid_y {
                    return;
                }
                scores[1] += 1;
                return;
            } else if x == mid_x {
                return;
            }
            if y < mid_y {
                scores[2] += 1;
                return;
            } else if y == mid_y {
                return;
            }
            scores[3] += 1;
            return;
        });
    (scores[0] * scores[1] * scores[2] * scores[3]).to_string()
}

fn part2(input: &Vec<String>) -> String {
    let (width, height, mut bots) = load(input);
    let mut count = 0;
    let mut good = false;
    while !good {
        let mut set = HashSet::new();
        good = true;
        count += 1;
        for ((x, y), (vx, vy)) in bots.iter_mut() {
            *x = (*x + *vx) % width;
            if *x < 0 {
                *x += width;
            }
            *y = (*y + *vy) % height;
            if *y < 0 {
                *y += height;
            }
            if good && !set.insert((*x, *y)) {
                good = false;
            }
        }
    }
    count.to_string()
}

fn load(input: &Vec<String>) -> (i32, i32, Vec<((i32, i32), (i32, i32))>) {
    let mut skip = 0;
    let mut width = 101;
    let mut height = 103;
    if input[2] == "# Do not remove above here" {
        width = input[0].parse::<i32>().unwrap();
        height = input[1].parse::<i32>().unwrap();
        skip = 3;
    }
    let bots = input
        .iter()
        .skip(skip)
        .map(|l| {
            let (pos_raw, vol_raw) = l.split_whitespace().collect_tuple().unwrap();
            let (x, y) = parse_cord(pos_raw);
            let (vx, vy) = parse_cord(vol_raw);
            ((x, y), (vx, vy))
        })
        .collect_vec();
    (width, height, bots)
}

fn parse_cord(line: &str) -> (i32, i32) {
    line.split('=')
        .last()
        .unwrap()
        .split(",")
        .map(|s| s.parse::<i32>().unwrap())
        .collect_tuple()
        .unwrap()
}

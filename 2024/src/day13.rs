use std::time::Duration;

use itertools::Itertools;
use rulinalg::{matrix, vector};

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    solve(input, 0)
}

fn part2(input: &Vec<String>) -> String {
    solve(input, 10000000000000)
}

fn solve(input: &Vec<String>, add: u64) -> String {
    input
        .chunks(4)
        .map(|c| {
            let (ax, ay) = parse_cord(&c[0], '+');
            let (bx, by) = parse_cord(&c[1], '+');
            let (mut gx, mut gy) = parse_cord(&c[2], '=');
            gx += add;
            gy += add;

            let sol_op = matrix![ax as f64, bx as f64; ay as f64,by as f64]
                .solve(vector![gx as f64, gy as f64]);
            if sol_op.is_err() {
                return 0;
            }
            let answer = sol_op.unwrap();
            let a = answer.data()[0].round() as u64;
            let b = answer.data()[1].round() as u64;
            if ax * a + bx * b != gx || ay * a + by * b != gy {
                return 0;
            }
            a * 3 + b
        })
        .sum::<u64>()
        .to_string()
}

fn parse_cord(line: &str, pre_pos: char) -> (u64, u64) {
    line.split(',')
        .map(|s| s.split(pre_pos).last().unwrap().parse::<u64>().unwrap())
        .collect_tuple()
        .unwrap()
}

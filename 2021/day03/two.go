package day03

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

func leastCommon(lines []string, i int, equal int) int {
	counter := make([]int, 2)
	for _, line := range lines {
		counter[line[i]-48]++
	}

	if counter[0] == counter[1] {
		return equal
	} else if counter[0] < counter[1] {
		return 0
	}
	return 1
}

func mostCommon(lines []string, i int, equal int) int {
	counter := make([]int, 2)
	for _, line := range lines {
		counter[line[i]-48]++
	}

	if counter[0] == counter[1] {
		return equal
	} else if counter[1] < counter[0] {
		return 0
	}
	return 1
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	counter := make([][]int, len(lines[0]))
	for i := range counter {
		counter[i] = make([]int, 2)
	}

	for _, line := range lines {
		for i, bit := range line {
			counter[i][bit-48]++
		}
	}

	validO2 := lines
	for i := 0; len(validO2) > 1; i++ {
		keepByte := byte(mostCommon(validO2, i, 1) + 48)
		tmpO2 := make([]string, 0)
		for _, line := range validO2 {
			if line[i] == keepByte {
				tmpO2 = append(tmpO2, line)
			}
		}
		validO2 = tmpO2
	}
	validCO2 := lines
	for i := 0; len(validCO2) > 1; i++ {
		keepByte := byte(leastCommon(validCO2, i, 0) + 48)
		tmpCO2 := make([]string, 0)
		for _, line := range validCO2 {
			if line[i] == keepByte {
				tmpCO2 = append(tmpCO2, line)
			}
		}
		validCO2 = tmpCO2
	}

	o2 := 0
	for _, bit := range validO2[0] {
		o2 <<= 1
		o2 += int(bit - 48)
	}

	co2 := 0
	for _, bit := range validCO2[0] {
		co2 <<= 1
		co2 += int(bit - 48)
	}

	return strconv.Itoa(o2 * co2), nil // Return with the answer
}

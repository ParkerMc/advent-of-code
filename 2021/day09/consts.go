// Package day09 2018
package day09

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "489"
const testAnswerTwo = "1056330"

var testFilename = "../test/private/" + tools.GetPackageName()

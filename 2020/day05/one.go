package day05

import (
	"errors"
	"math"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	highest := 0
	for _, line := range lines {
		rowHigh, rowLow := 127, 0
		colHigh, colLow := 7, 0
		for _, c := range line {
			switch c {
			case 'B':
				rowLow = int(math.Ceil((float64(rowHigh)-float64(rowLow))/2.0)) + rowLow
			case 'F':
				rowHigh = (rowHigh-rowLow)/2 + rowLow
			case 'R':
				colLow = int(math.Ceil((float64(colHigh)-float64(colLow))/2.0)) + colLow
			case 'L':
				colHigh = (colHigh-colLow)/2 + colLow
			}
		}
		if rowLow == rowHigh && colLow == colHigh {
			row := rowLow
			col := colLow
			num := row*8 + col
			if num > highest {
				highest = num
			}
		} else {
			return "", errors.New("error with seat code")
		}
	}

	return strconv.Itoa(highest), nil // Return with the answer
}

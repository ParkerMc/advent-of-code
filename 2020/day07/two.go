package day07

import (
	"errors"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

type iBag struct {
	name  string
	count int
}

func countContains(bagMap map[string][]iBag, startBag string) int {
	count := 1
	for _, bag := range bagMap[startBag] {
		count += bag.count * countContains(bagMap, bag.name)
	}
	return count
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	bagMap := make(map[string][]iBag, len(lines))
	for _, line := range lines {
		bagMatch := bagReg.FindStringSubmatch(line)
		if len(bagMatch) == 0 {
			return "", errors.New("data error when using regex")
		}

		containsMatch := containsReg.FindAllStringSubmatch(line, -1)
		if len(containsMatch) == 0 {
			return "", errors.New("data error when using regex")
		}

		bagMap[bagMatch[1]] = make([]iBag, 0)
		for _, subMatch := range containsMatch {
			if subMatch[2] != "" {
				count, err := strconv.Atoi(subMatch[1])
				if err != nil {
					return "", err
				}
				bagMap[bagMatch[1]] = append(bagMap[bagMatch[1]], iBag{subMatch[2], count})
			}
		}
	}

	return strconv.Itoa(countContains(bagMap, "shiny gold") - 1), nil // Return with the answer
}

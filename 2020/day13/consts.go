// Package day13 2020
package day13

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "203"
const testAnswerTwo = "905694340256752"

var testFilename = "../test/private/" + tools.GetPackageName()

package tools

import "testing"

func TestAbs(t *testing.T) {
	if Abs(5) != 5 || Abs(-5) != 5 {
		t.Fatal("Abs error")
	}
}

func TestGcd(t *testing.T) {
	if Gcd(144, 132) != 12 {
		t.Fatal("Gcd error")
	}
}

func TestLcm(t *testing.T) {
	if Lcm(15, 20) != 60 {
		t.Fatal("Lcm error")
	}
}

func TestMin(t *testing.T) {
	if Min(15, 20) != 15 {
		t.Fatal("Min error")
	}
	if Min(30, 4) != 4 {
		t.Fatal("Min error")
	}
}

func TestMax(t *testing.T) {
	if Max(15, 20) != 20 {
		t.Fatal("Max error")
	}
	if Max(30, 4) != 30 {
		t.Fatal("Max error")
	}
}

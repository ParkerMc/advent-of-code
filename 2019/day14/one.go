package day14

import (
	"errors"
	"math"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

type element struct {
	name   string
	amount int64
}

type reaction struct {
	inputs []element
	output element
}

func parseReactions(lines []string) (map[string]reaction, error) {
	reactions := make(map[string]reaction)

	for _, line := range lines {
		r := reaction{inputs: make([]element, 0)}
		split := strings.Split(line, " => ")
		rInputs := strings.Split(split[0], ", ")
		for _, in := range rInputs {
			e := element{}
			split2 := strings.Split(in, " ")
			e.name = split2[1]
			amt, err := strconv.Atoi(split2[0])
			if err != nil {
				return nil, err
			}
			e.amount = int64(amt)
			r.inputs = append(r.inputs, e)
		}
		split2 := strings.Split(split[1], " ")
		r.output.name = split2[1]
		amt, err := strconv.Atoi(split2[0])
		if err != nil {
			return nil, err
		}
		r.output.amount = int64(amt)
		reactions[r.output.name] = r
	}
	return reactions, nil
}

func getOreNeeded(reactions map[string]reaction, extra map[string]int64, amt int64) (int64, map[string]int64, error) {
	need := map[string]int64{"FUEL": amt, "ORE": 0}
	if extra == nil {
		extra = make(map[string]int64, 0)
	}
	for len(need) > 1 {
		for item, amount := range need {
			if item != "ORE" {
				r, ok := reactions[item]
				if !ok {
					return -1, nil, errors.New("can not make an item")
				}
				reactionAmt := int64(math.Ceil(float64(amount) / float64(r.output.amount)))
				for _, e := range r.inputs {
					amt := e.amount * reactionAmt
					amt -= extra[e.name]
					if amt < 0 {
						extra[e.name] = -amt
						amt = 0
					} else {
						delete(extra, e.name)
					}
					if amt != 0 {
						need[e.name] += amt
					}
				}
				eamt := r.output.amount*reactionAmt - amount
				if eamt > 0 {
					extra[item] = eamt
				}
				delete(need, item)
			}
		}
	}
	return need["ORE"], nil, nil
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	reactions, err := parseReactions(lines)
	if err != nil {
		return "", err
	}

	amt, _, err := getOreNeeded(reactions, nil, 1)

	return strconv.FormatInt(amt, 10), err // Return with the answer
}

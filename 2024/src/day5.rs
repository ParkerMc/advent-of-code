use std::{collections::HashMap, time::Duration};

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    let (order, updates) = load_data(input);

    updates
        .iter()
        .filter(|u| {
            for (i, v) in u.iter().enumerate() {
                if let Some(after) = order.get(v) {
                    for v2 in u.iter().take(i) {
                        if after.contains(v2) {
                            return false;
                        }
                    }
                }
            }
            true
        })
        .map(|u| u[u.len() / 2])
        .sum::<u32>()
        .to_string()
}

fn part2(input: &Vec<String>) -> String {
    let (order, updates) = load_data(input);
    updates
        .into_iter()
        .filter_map(|mut u| {
            let mut swap = None;
            let mut skip = 0;
            'main: loop {
                if let Some((i, j)) = swap {
                    (u[i], u[j]) = (u[j], u[i]);
                    skip = j;
                }
                for (i, v) in u.iter().enumerate().skip(skip) {
                    if let Some(after) = order.get(v) {
                        for (j, v2) in u.iter().enumerate().take(i) {
                            if after.contains(v2) {
                                swap = Some((i, j));
                                continue 'main;
                            }
                        }
                    }
                }
                break;
            }
            if swap == None {
                return None;
            }
            Some(u)
        })
        .map(|u| u[u.len() / 2])
        .sum::<u32>()
        .to_string()
}

fn load_data(input: &Vec<String>) -> (HashMap<u32, Vec<u32>>, Vec<Vec<u32>>) {
    let (map, updates) = input
        .split(|l| l.is_empty())
        .collect_tuple()
        .expect("Expected two groups seperated by blank line");
    // let mut forward_map = HashMap::new();
    let mut backward_map = HashMap::new();
    for l in map {
        let (num, before) = l
            .split("|")
            .map(|i| i.parse::<u32>().expect("Expected number"))
            .collect_tuple()
            .expect("Expected two numbers seperated by |");
        // forward_map.entry(before).or_insert(vec![]).push(num);
        backward_map.entry(num).or_insert(vec![]).push(before);
    }

    let updates = updates
        .into_iter()
        .map(|l| {
            l.split(",")
                .into_iter()
                .map(|n| n.parse().expect("Expected number"))
                .collect_vec()
        })
        .collect_vec();

    (backward_map, updates)
}

package day02

import (
	"errors"
	"math"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// Two the second puzzle
func Two(filename string) (string, error) {
	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	for i, line := range lines { // Loop though the lines
		lineRunes := []rune(line) // Runes for the line

		for _, line2 := range lines[i:] { // Loop though all lines after this one
			line2Runes := []rune(line2) // Runes for this second line
			lenDif := int(math.Abs(float64(len(lineRunes) - len(line2Runes))))
			different := lenDif // Keeps track of the number of runes that are different

			if different > 1 { // If it the difference is more than 1 it isn't what we are looking for
				break
			}

			for j := 0; j < len(lineRunes); j++ { // Loop though both lines
				if lineRunes[j] != line2Runes[j] { // If the runes arn't the same count the difference
					different++
					if different > 1 { // If it the difference is more than 1 it isn't what we are looking for
						break
					}
				}
			}

			if different == 1 { // If there is only one difference it is right so get the answer from the two
				letters := "" // Letters will be the answer

				for j := 0; j < len(lineRunes) && j < len(line2Runes); j++ { // For each of the runes
					if lineRunes[j] == line2Runes[j] { // If the rune is the same in both
						letters += string([]rune{lineRunes[j]}) // Add it to the answer
					}
				}
				return letters, nil // Return the answer
			}

		}
	}

	return "", errors.New("input data is not valid for this puzzle") // Should never be reached
}

.PHONY: help run setup test

help: ## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

run: ## Run the program
	cd 2018; make run
	cd 2019; make run
	cd 2020; make run
	cd 2021; make run

setup: ## Setup everything
	cd 2018; make setup
	cd 2019; make setup
	cd 2020; make setup
	cd 2021; make setup

test: ## Run the tests
	cd 2018; make test
	cd 2019; make test
	cd 2020; make test
	cd 2021; make test

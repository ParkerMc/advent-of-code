package day15

import (
	"sort"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// UnitType stores what type the unit is
type UnitType int

// Set types
const (
	UnitElf    UnitType = 0
	UnitGoblin UnitType = 1
)

// Unit the struct for the unit
type Unit struct {
	X     int
	Y     int
	HP    int
	Type  UnitType
	Alive bool
}

// Move moves the unit
func (unit *Unit) Move(units []*Unit, walls [][]bool, elfAttackPow int) {
	unitCanAttack, _ := unit.CanAttack(units)
	if !unitCanAttack {
		movesChart := genMovesChart(unit.X, unit.Y, walls, units)
		closestMoves := -1
		var closestPos []int
		for _, otherUnit := range units {
			if otherUnit.Type != unit.Type && otherUnit.Alive {
				for _, pos := range [][]int{{otherUnit.X - 1, otherUnit.Y}, {otherUnit.X, otherUnit.Y - 1}, {otherUnit.X, otherUnit.Y + 1}, {otherUnit.X + 1, otherUnit.Y}} {
					if pos[0] > -1 && pos[1] > -1 &&
						len(movesChart) > pos[0] && len(movesChart[pos[0]]) > pos[1] && movesChart[pos[0]][pos[1]] > -1 &&
						(closestMoves == -1 || closestMoves > movesChart[pos[0]][pos[1]]) {
						closestMoves = movesChart[pos[0]][pos[1]]
						closestPos = pos
					}
				}
			}
		}
		if closestPos != nil {
			closestMovePos := []int{-1, -1}
			closestMoveMoves := 1000000000000
			otherMovesChart := genMovesChart(closestPos[0], closestPos[1], walls, units)
			for _, pos := range [][]int{{unit.X - 1, unit.Y}, {unit.X, unit.Y - 1}, {unit.X, unit.Y + 1}, {unit.X + 1, unit.Y}} {
				if closestMoveMoves > otherMovesChart[pos[0]][pos[1]] && otherMovesChart[pos[0]][pos[1]] > -1 {
					closestMovePos = pos
					closestMoveMoves = otherMovesChart[pos[0]][pos[1]]
				}
			}
			if closestMovePos[0] != -1 {
				unit.X = closestMovePos[0]
				unit.Y = closestMovePos[1]
			}
		}
	}
	unitCanAttack, attackUnit := unit.CanAttack(units)
	if unitCanAttack {
		if unit.Type == UnitElf {
			attackUnit.HP -= elfAttackPow
		} else {
			attackUnit.HP -= 3
		}
		if attackUnit.HP <= 0 {
			attackUnit.Alive = false
		}
	}
}

// CanAttack sees if the unit can attack and returns the unit to attack
func (unit *Unit) CanAttack(units []*Unit) (bool, *Unit) {
	var attackUnit *Unit
	for _, otherUnit := range units {
		for _, pos := range [][]int{{unit.X + 1, unit.Y}, {unit.X - 1, unit.Y}, {unit.X, unit.Y + 1}, {unit.X, unit.Y - 1}} {
			if otherUnit.Type != unit.Type && otherUnit.Alive && otherUnit.X == pos[0] && otherUnit.Y == pos[1] && (attackUnit == nil || attackUnit.HP > otherUnit.HP) {
				attackUnit = otherUnit
			}
		}
	}
	return attackUnit != nil, attackUnit
}

// func draw(walls [][]bool, units []*Unit) {
// 	for x, xWalls := range walls {
// 		line := ""
// 		for y, wall := range xWalls {
// 			if wall {
// 				line += "#"
// 			} else {
// 				unitFound := false
// 				for _, unit := range units {
// 					if unit.Alive && unit.X == x && unit.Y == y {
// 						if unit.Type == UnitElf {
// 							line += "E"
// 						} else {
// 							line += "G"
// 						}
// 						unitFound = true
// 						break
// 					}
// 				}
// 				if !unitFound {
// 					line += "."
// 				}
// 			}
// 		}
// 		log.Print(line)
// 	}
// }

func genMovesChart(x int, y int, walls [][]bool, units []*Unit) [][]int {
	chart := make([][]int, 0)
	for x, xWall := range walls {
		chart = append(chart, make([]int, 0))
		for _, wall := range xWall {
			if wall {
				chart[x] = append(chart[x], -2)
			} else {
				chart[x] = append(chart[x], -1)
			}
		}
	}

	for _, unit := range units {
		if unit.Alive {
			chart[unit.X][unit.Y] = -3
		}
	}
	chart[x][y] = 0
	lastPoses := [][]int{{x, y}}

	for moves := 1; len(lastPoses) > 0; moves++ {
		newLast := make([][]int, 0)
		for _, lastPos := range lastPoses {
			for _, pos := range [][]int{{lastPos[0] + 1, lastPos[1]}, {lastPos[0] - 1, lastPos[1]}, {lastPos[0], lastPos[1] + 1}, {lastPos[0], lastPos[1] - 1}} {
				if pos[0] > -1 && pos[1] > -1 &&
					len(chart) > pos[0] && len(chart[pos[0]]) > pos[1] &&
					chart[pos[0]][pos[1]] == -1 {
					newLast = append(newLast, pos)
					chart[pos[0]][pos[1]] = moves
				}
			}
			lastPoses = newLast
		}
	}

	return chart
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	units := make([]*Unit, 0)
	walls := make([][]bool, 0)
	for x, line := range lines {
		walls = append(walls, make([]bool, 0))
		for y, char := range []rune(line) {
			if char == '#' {
				walls[x] = append(walls[x], true)
			} else {
				if char == 'E' {
					units = append(units, &Unit{x, y, 200, UnitElf, true})
				} else if char == 'G' {
					units = append(units, &Unit{x, y, 200, UnitGoblin, true})
				}
				walls[x] = append(walls[x], false)
			}
		}
	}
	//draw(walls, units)
	nextTick := true
	var tick int
	for tick = 0; nextTick; tick++ {
		sort.SliceStable(units, func(i int, j int) bool {
			if units[i].X < units[j].X || (units[i].X == units[j].X && units[i].Y < units[j].Y) {
				return true
			}
			return false
		})
		//log.Printf("Round %d:\n\n", tick+1)
		for _, unit := range units {
			if unit.Alive {
				unit.Move(units, walls, 3)
			}
		}
		//draw(walls, units)
		nextTick = false
		for _, unit := range units {
			if unit.Type == UnitElf && unit.Alive {
				nextTick = true
				break
			}
		}
		if nextTick {
			nextTick = false
			for _, unit := range units {
				if unit.Type == UnitGoblin && unit.Alive {
					nextTick = true
					break
				}
			}
		}
	}
	answer := 0
	for _, unit := range units {
		if unit.Alive {
			answer += unit.HP
		}
	}

	answer *= tick - 1
	return strconv.Itoa(answer), nil // Return with the answer
}

use std::{collections::HashSet, time::Duration};

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

const N: u8 = 0x01;
const S: u8 = 0x02;
const E: u8 = 0x04;
const W: u8 = 0x08;

fn part1(input: &Vec<String>) -> String {
    let (map, counts, last_group) = load(input);
    let flat_map = map.iter().flat_map(|r| r).collect_vec();
    (1..=last_group)
        .map(|group| {
            let group_squares = flat_map
                .iter()
                .filter(|(g, _, _)| *g == group)
                .collect_vec();

            counts[group] * group_squares.into_iter().map(|(_, e, _)| e).sum::<u32>()
        })
        .sum::<u32>()
        .to_string()
}

fn part2(input: &Vec<String>) -> String {
    let (map, counts, last_group) = load(input);
    let flat_map = map
        .iter()
        .enumerate()
        .flat_map(|(y, r)| {
            r.iter()
                .enumerate()
                .map(|(x, (group, _, edges))| ((y, x), *group, *edges))
                .collect_vec()
        })
        .collect_vec();
    (1..=last_group)
        .map(|group| {
            let mut edges = 0;
            let mut group_edges: HashSet<_> = flat_map
                .iter()
                .filter(|(_, g, edges)| *edges > 0 && *g == group)
                .flat_map(|(pos, group, edge_flags)| {
                    let mut out = vec![];
                    if edge_flags & N != 0 {
                        out.push((*pos, *group, N));
                    }
                    if edge_flags & S != 0 {
                        out.push((*pos, *group, S));
                    }
                    if edge_flags & E != 0 {
                        out.push((*pos, *group, E));
                    }
                    if edge_flags & W != 0 {
                        out.push((*pos, *group, W));
                    }
                    out
                })
                .collect();

            while let Some((pos, group, edge)) = group_edges.iter().next().cloned() {
                edges += 1;
                let mut cur_pos = pos;
                if edge == N || edge == S {
                    while group_edges.remove(&(cur_pos, group, edge)) {
                        cur_pos.1 += 1;
                    }
                    cur_pos = pos;
                    if cur_pos.1 == 0 {
                        continue;
                    }
                    cur_pos.1 -= 1;
                    while group_edges.remove(&(cur_pos, group, edge)) {
                        if cur_pos.1 == 0 {
                            break;
                        }
                        cur_pos.1 -= 1;
                    }
                    continue;
                }

                while group_edges.remove(&(cur_pos, group, edge)) {
                    cur_pos.0 += 1;
                }
                cur_pos = pos;
                if cur_pos.0 == 0 {
                    continue;
                }
                cur_pos.0 -= 1;
                while group_edges.remove(&(cur_pos, group, edge)) {
                    if cur_pos.0 == 0 {
                        break;
                    }
                    cur_pos.0 -= 1;
                }
            }
            counts[group] * edges
        })
        .sum::<u32>()
        .to_string()
}

fn load(input: &Vec<String>) -> (Vec<Vec<(usize, u32, u8)>>, Vec<u32>, usize) {
    let raw_map = input.iter().map(|l| l.chars().collect_vec()).collect_vec();
    let y_len = raw_map.len();
    let x_len = raw_map[0].len();
    let mut map = input.iter().map(|_| vec![(0, 0, 0); x_len]).collect_vec();
    let mut counts = vec![0];
    let mut queue = HashSet::new();
    let mut group = 0;

    for y in 0..raw_map.len() {
        for x in 0..raw_map[0].len() {
            if map[y][x].0 != 0 {
                continue;
            }
            let c = raw_map[y][x];
            group += 1;
            queue.insert((y, x));
            let mut count = 0;
            while let Some((y, x)) = queue.iter().next().cloned() {
                queue.remove(&(y, x));
                if map[y][x].0 != 0 {
                    continue;
                }
                count += 1;
                let mut edge_count = 0;
                let mut edges = 0;
                if y > 0 && raw_map[y - 1][x] == c {
                    queue.insert((y - 1, x));
                } else {
                    edges |= N;
                    edge_count += 1;
                }
                if x > 0 && raw_map[y][x - 1] == c {
                    queue.insert((y, x - 1));
                } else {
                    edges |= W;
                    edge_count += 1;
                }

                if y < y_len - 1 && raw_map[y + 1][x] == c {
                    queue.insert((y + 1, x));
                } else {
                    edges |= S;
                    edge_count += 1;
                }
                if x < x_len - 1 && raw_map[y][x + 1] == c {
                    queue.insert((y, x + 1));
                } else {
                    edges |= E;
                    edge_count += 1;
                }
                map[y][x] = (group, edge_count, edges)
            }
            counts.push(count);
        }
    }
    return (map, counts, group);
}

package day01

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	sum := 0
	for _, line := range lines {
		num, err := strconv.Atoi(line)
		if err != nil {
			return "", err
		}

		num = num/3 - 2
		for num > 0 {
			sum += num
			num = num/3 - 2
		}
	}

	return strconv.Itoa(sum), nil // Return with the answer
}

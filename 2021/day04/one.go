package day04

import (
	"errors"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

type bingoCard [][]int

func wasCalled(arr []int, val int) bool {
	for _, cur := range arr {
		if cur == val {
			return true
		}
	}
	return false
}

func wins(card bingoCard, called []int) bool {
	// check horizontal
	for _, row := range card {
		for x, cell := range row {
			if !wasCalled(called, cell) {
				break
			}
			if x == len(row)-1 {
				return true
			}
		}
	}

	// check vertical
	for x := range card[0] {
		for y, row := range card {
			if !wasCalled(called, row[x]) {
				break
			}
			if y == len(card)-1 {
				return true
			}
		}
	}
	return false
}

func load(lines []string) ([]int, []bingoCard, error) {
	var err error
	split := strings.Split(lines[0], ",")
	calls := make([]int, len(split))
	for i, val := range split {
		calls[i], err = strconv.Atoi(val)
		if err != nil {
			return nil, nil, err
		}
	}

	cards := make([]bingoCard, 0)
	card := make(bingoCard, 0)
	for _, line := range lines[2:] {
		if line == "" {
			cards = append(cards, card)
			card = make(bingoCard, 0)
			continue
		}
		split = strings.Split(line, " ")
		cardLine := make([]int, 0)
		for _, val := range split {
			if val == "" {
				continue
			}
			intVal, err := strconv.Atoi(val)
			if err != nil {
				return nil, nil, err
			}
			cardLine = append(cardLine, intVal)
		}
		card = append(card, cardLine)
	}
	cards = append(cards, card)

	return calls, cards, nil
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	calls, cards, err := load(lines)
	if err != nil {
		return "", err
	}

	for i := range calls {
		for _, card := range cards {
			if wins(card, calls[:i+1]) {
				uncalledTotal := 0
				for _, row := range card {
					for _, cell := range row {
						if !wasCalled(calls[:i+1], cell) {
							uncalledTotal += cell
						}
					}
				}

				return strconv.Itoa(uncalledTotal * calls[i]), nil
			}
		}
	}

	return "", errors.New("never got a bingo") // Return with the answer
}

// 1041 (too low)
use std::{cmp::Ordering, collections::BinaryHeap, time::Duration};

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    let y_len = input.len();
    let x_len = input[0].len();
    let map = input
        .iter()
        .map(|l| {
            l.chars()
                .map(|c| c.to_digit(10).expect("Expected num"))
                .collect_vec()
        })
        .collect_vec();
    let mut heap = BinaryHeap::new();

    // Start
    heap.push(Entry {
        pos: (0, 0),
        loss: 0,
        dir: Dir::East,
        dir_steps: 0, // TODO maybe 1
        path: vec![],
    });

    while let Some(e) = heap.pop() {
        if e.pos.0 == y_len - 1 && e.pos.1 == x_len - 1 {
            return e.loss.to_string();
        }
        let next = e.next_entries(y_len, x_len, &map);
        heap.extend(next); // TODO maybe check seen map here
    }

    panic!("NOT FOUND")
}

fn part2(input: &Vec<String>) -> String {
    String::from("")
}

#[derive(Debug, Eq, PartialEq, Clone, Copy)]
enum Dir {
    North,
    South,
    East,
    West,
}

impl Dir {
    fn step(&self, y: usize, x: usize, y_len: usize, x_len: usize) -> Option<(usize, usize)> {
        match self {
            Dir::North if y > 0 => Some((y - 1, x)),
            Dir::South if y + 1 < y_len => Some((y + 1, x)),

            Dir::East if x + 1 < x_len => Some((y, x + 1)),
            Dir::West if x > 0 => Some((y, x - 1)),
            _ => None,
        }
    }

    fn bit(&self) -> u64 {
        match self {
            Dir::North => 0x01,
            Dir::South => 0x02,
            Dir::East => 0x04,
            Dir::West => 0x08,
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
struct Entry {
    pos: (usize, usize),
    loss: u32,
    dir: Dir,
    dir_steps: u32,
    path: Vec<(usize, usize)>, // TODO remove
}

fn clone_append<T: Clone>(input: &Vec<T>, entry: T) -> Vec<T> {
    let mut v = input.to_vec();
    v.push(entry);
    v
}
impl Entry {
    fn next_entries(&self, y_len: usize, x_len: usize, map: &Vec<Vec<u32>>) -> Vec<Entry> {
        let mut poses = vec![];
        if self.dir_steps < 3 {
            poses.push(
                self.dir
                    .step(self.pos.0, self.pos.1, y_len, x_len)
                    .map(|pos| Entry {
                        pos,
                        loss: self.loss + map[pos.0][pos.1],
                        dir: self.dir,
                        dir_steps: self.dir_steps + 1,
                        path: clone_append(&self.path, pos),
                    }),
            );
        }
        match self.dir {
            Dir::North | Dir::South => {
                poses.push(
                    Dir::East
                        .step(self.pos.0, self.pos.1, y_len, x_len)
                        .map(|pos| Entry {
                            pos,
                            loss: self.loss + map[pos.0][pos.1],
                            dir: Dir::East,
                            dir_steps: 0,
                            path: clone_append(&self.path, pos),
                        }),
                );
                poses.push(
                    Dir::West
                        .step(self.pos.0, self.pos.1, y_len, x_len)
                        .map(|pos| Entry {
                            pos,
                            loss: self.loss + map[pos.0][pos.1],
                            dir: Dir::West,
                            dir_steps: 0,
                            path: clone_append(&self.path, pos),
                        }),
                );
            }
            Dir::East | Dir::West => {
                poses.push(
                    Dir::North
                        .step(self.pos.0, self.pos.1, y_len, x_len)
                        .map(|pos| Entry {
                            pos,
                            loss: self.loss + map[pos.0][pos.1],
                            dir: Dir::North,
                            dir_steps: 0,
                            path: clone_append(&self.path, pos),
                        }),
                );
                poses.push(
                    Dir::South
                        .step(self.pos.0, self.pos.1, y_len, x_len)
                        .map(|pos| Entry {
                            pos,
                            loss: self.loss + map[pos.0][pos.1],
                            dir: Dir::South,
                            dir_steps: 0,
                            path: clone_append(&self.path, pos),
                        }),
                );
            }
        }

        poses
            .into_iter()
            .filter_map(|x| x)
            .filter(|x| !self.path.contains(&x.pos))
            .collect_vec()
    }
}

impl Ord for Entry {
    fn cmp(&self, other: &Self) -> Ordering {
        other.loss.cmp(&self.loss)
    }
}

impl PartialOrd for Entry {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

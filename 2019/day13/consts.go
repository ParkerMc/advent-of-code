// Package day13 2019
package day13

import "gitlab.com/parkermc/advent-of-code/2019/tools"

const testAnswerOne = "258"
const testAnswerTwo = "12765"

var testFilename = "../test/private/" + tools.GetPackageName()

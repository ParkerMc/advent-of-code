package day16

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

func praseInput(line string) ([]int, error) {
	msg := make([]int, 0)
	for _, c := range line {
		val, err := strconv.Atoi(string(c))
		if err != nil {
			return nil, err
		}
		msg = append(msg, val)
	}
	return msg, nil
}
func runFTT(msg []int) []int {
	basePattern := []int{0, 1, 0, -1}
	patterns := make([][]int, 0)
	for i := range msg {
		pattern := []int{}
		first := true
		for len(pattern) < len(msg) {
			for _, val := range basePattern {
				for k := -1; k < i && len(pattern) < len(msg); k++ {
					if first {
						first = false
					} else {
						pattern = append(pattern, val)
					}
				}
			}
		}
		patterns = append(patterns, pattern)
	}

	for k := 0; k < 100; k++ {
		nmsg := make([]int, len(msg))
		for i := range msg {
			num := 0
			for j := i; j < len(msg); j++ {
				num += msg[j] * patterns[i][j]
			}
			nmsg[i] = tools.Abs(num % 10)
		}
		msg = nmsg

	}
	return msg
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	msg, err := praseInput(lines[0])
	if err != nil {
		return "", err
	}

	msg = runFTT(msg)

	out := 0
	for i := 0; i < 8; i++ {
		out *= 10
		out += msg[i]
	}

	return strconv.Itoa(out), nil // Return with the answer
}

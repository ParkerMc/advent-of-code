// Package day17 2018
package day17

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "7503"
const testAnswerTwo = "3229"

var testFilename = "../test/private/" + tools.GetPackageName()

use std::time::Duration;

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    let chars = input.iter().map(|l| l.chars().collect_vec()).collect_vec();
    chars
        .iter()
        .enumerate()
        .flat_map(|(i, l)| {
            let mut nums = vec![];
            let mut next_symbol = false;
            let mut working_num = false;
            let mut num = 0;
            for (j, c) in l.iter().enumerate() {
                if !c.is_ascii_digit() {
                    if working_num
                        && (next_symbol
                            || c != &'.'
                            || (i > 0 && !chars[i - 1][j].is_numeric() && chars[i - 1][j] != '.')
                            || (i < chars.len() - 1
                                && !chars[i + 1][j].is_numeric()
                                && chars[i + 1][j] != '.'))
                    {
                        nums.push(num);
                    }
                    num = 0;
                    next_symbol = false;
                    working_num = false;
                    continue;
                }
                if !working_num {
                    working_num = true;
                    if j > 0
                        && (l[j - 1] != '.'
                            || (i > 0
                                && !chars[i - 1][j - 1].is_numeric()
                                && chars[i - 1][j - 1] != '.')
                            || (i < chars.len() - 1
                                && !chars[i + 1][j - 1].is_numeric()
                                && chars[i + 1][j - 1] != '.'))
                    {
                        next_symbol = true;
                    }
                }
                num = num * 10 + c.to_digit(10).expect("Has to be digit");
                if !next_symbol
                    && ((i > 0 && !chars[i - 1][j].is_numeric() && chars[i - 1][j] != '.')
                        || (i < chars.len() - 1
                            && !chars[i + 1][j].is_numeric()
                            && chars[i + 1][j] != '.'))
                {
                    next_symbol = true
                }
            }

            if working_num && next_symbol {
                nums.push(num);
            }

            nums
        })
        .sum::<u32>()
        .to_string()
}

fn part2(input: &Vec<String>) -> String {
    let chars = input.iter().map(|l| l.chars().collect_vec()).collect_vec();
    chars
        .iter()
        .enumerate()
        .flat_map(|(i, l)| {
            l.iter()
                .enumerate()
                .map(|(j, c)| {
                    if c != &'*' {
                        return 0;
                    }

                    let mut i_opts: Vec<i64> = vec![0];
                    if i > 0 {
                        i_opts.push(-1);
                    }
                    if i < chars.len() - 1 {
                        i_opts.push(1)
                    }

                    let mut j_opts: Vec<i64> = vec![0];
                    if j > 0 {
                        j_opts.push(-1);
                    }
                    if j < chars[0].len() - 1 {
                        j_opts.push(1)
                    }

                    let nums = i_opts
                        .into_iter()
                        .flat_map(|i_opt| {
                            j_opts
                                .iter()
                                .filter(|j_opt| !(i_opt == 0 && **j_opt == 0))
                                .map(|j_opt| {
                                    (
                                        usize::try_from(i as i64 + i_opt).expect("Invalid usize"),
                                        usize::try_from(j as i64 + j_opt).expect("Invalid usize"),
                                    )
                                })
                                .collect_vec()
                        })
                        .filter_map(|(i, j)| {
                            let line = &chars[i];
                            if !line[j].is_numeric() {
                                return None;
                            }
                            let mut start = j;
                            let mut end = j;

                            while start > 0 && line[start - 1].is_ascii_digit() {
                                start -= 1;
                            }

                            let line_len = line.len();

                            while end < line_len - 1 && line[end + 1].is_ascii_digit() {
                                end += 1;
                            }

                            Some(
                                line[start..=end]
                                    .iter()
                                    .collect::<String>()
                                    .parse::<u64>()
                                    .expect("Should have been number"),
                            )
                        })
                        .unique()
                        .collect_vec();
                    if nums.len() != 2 {
                        return 0;
                    }
                    nums[0] * nums[1]
                })
                .collect_vec()
        })
        .sum::<u64>()
        .to_string()
}

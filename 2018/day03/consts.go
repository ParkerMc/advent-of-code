// Package day03 2018
package day03

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "104126"
const testAnswerTwo = "695"

var testFilename = "../test/private/" + tools.GetPackageName()

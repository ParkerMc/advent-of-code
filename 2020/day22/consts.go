// Package day22 2020
package day22

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "33925"
const testAnswerTwo = "33441"

var testFilename = "../test/private/" + tools.GetPackageName()

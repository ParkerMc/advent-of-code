package day02

import (
	"regexp"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	infoReg, err := regexp.Compile(`(\d+)-(\d+) (.+): (.+)`)
	if err != nil {
		return "", err
	}

	valid := 0
	for _, line := range lines {
		match := infoReg.FindStringSubmatch(line)
		polyNum1, err := strconv.Atoi(match[1])
		if err != nil {
			return "", err
		}
		polyNum2, err := strconv.Atoi(match[2])
		if err != nil {
			return "", err
		}
		poly := []rune(match[3])[0]
		password := []rune(match[4])

		letter1 := password[polyNum1-1]
		letter2 := password[polyNum2-1]
		if letter1 == poly {
			if letter2 != poly {
				valid++
			}
		} else if letter2 == poly {
			valid++
		}
	}

	return strconv.Itoa(valid), nil // Return with the answer
}

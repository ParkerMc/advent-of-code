use std::{
    fs::File,
    io::{BufRead, BufReader},
};

pub fn read_input(file_path: &str) -> Vec<String> {
    let file =
        File::open(file_path).unwrap_or_else(|_| panic!("Failed to open file {}", file_path));
    let buf = BufReader::new(file);
    let mut lines: Vec<String> = buf
        .lines()
        .map(|l| l.expect("Could not parse line in input file"))
        .skip_while(|l| l.is_empty())
        .collect();

    if let Some(i) = lines.iter().rposition(|x| !x.is_empty()) {
        let new_len = i + 1;
        lines.truncate(new_len);
    }

    lines
}

#[cfg(test)]
mod tests {
    use super::read_input;

    #[test]
    fn read_input_reads_content() {
        println!(module_path!());
        let res = read_input(&String::from(
            "resources/test/utils/io_read_input_reads_content",
        ));
        assert_eq!(res.len(), 2);
        assert_eq!(res[0], "test");
        assert_eq!(res[1], "test2");
    }

    #[test]
    fn read_input_removes_start_newlines() {
        println!(module_path!());
        let res = read_input(&String::from(
            "resources/test/utils/io_read_input_removes_start_newlines",
        ));
        assert_eq!(res.len(), 2);
        assert_eq!(res[0], "test3");
        assert_eq!(res[1], "test4");
    }

    #[test]
    fn read_input_removes_end_newlines() {
        println!(module_path!());
        let res = read_input(&String::from(
            "resources/test/utils/io_read_input_removes_end_newlines",
        ));
        assert_eq!(res.len(), 2);
        assert_eq!(res[0], "test5");
        assert_eq!(res[1], "test6");
    }
}

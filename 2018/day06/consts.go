// Package day06 2018
package day06

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "3907"
const testAnswerTwo = "42036"

var testFilename = "../test/private/" + tools.GetPackageName()

package day20

import (
	"errors"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

var (
	northObj = &regexObj{} // Really should be const but I'm lazy
	southObj = &regexObj{}
	eastObj  = &regexObj{}
	westObj  = &regexObj{}
)

type regexObj struct {
	val     []*regexObj
	options []*regexObj
	parent  *regexObj // NOT ALWAYS SET
}

type cellObj struct {
	north bool
	west  bool
}

func readInput(input string) (*regexObj, int, error) {
	root := &regexObj{}
	object := root
	i := 0
	for ; i < len(input); i++ {
		switch input[i] {
		case 'N':
			object.val = append(object.val, northObj)
		case 'S':
			object.val = append(object.val, southObj)
		case 'E':
			object.val = append(object.val, eastObj)
		case 'W':
			object.val = append(object.val, westObj)
		case '(':
			next, j, err := readInput(input[i+1:])
			if err != nil {
				return nil, 0, err
			}
			i += j + 1
			object.val = append(object.val, next)
		case '$':
			fallthrough
		case ')':
			return root, i, nil
		case '|':
			tmp := regexObj{}
			if object.parent != nil {
				object = object.parent
			} else {
				tmp2 := regexObj{options: []*regexObj{root}}
				root = &tmp2
				object = root
			}
			tmp.parent = object
			object.options = append(object.options, &tmp)
			object = &tmp
		default:
			return nil, 0, errors.New("idk: " + string(input[i]))
		}
	}
	return nil, 0, errors.New("should never get here")
}

func gridSetIfNil(area map[int]map[int]*cellObj, pos tools.Pos2D, val *cellObj) {
	if subMap, ok := area[pos.X]; !ok {
		area[pos.X] = map[int]*cellObj{pos.Y: val}
	} else if subMap[pos.Y] == nil {
		area[pos.X][pos.Y] = val
	}
}

func drawGrid(grid map[int]map[int]*cellObj, pos tools.Pos2D, obj *regexObj) {
	if len(obj.options) == 0 {

		for _, move := range obj.val {
			switch move {
			case northObj:
				grid[pos.X][pos.Y].north = true
				pos.Y--
				gridSetIfNil(grid, pos, &cellObj{})
			case southObj:
				pos.Y++
				gridSetIfNil(grid, pos, &cellObj{})
				grid[pos.X][pos.Y].north = true
			case eastObj:
				pos.X++
				gridSetIfNil(grid, pos, &cellObj{})
				grid[pos.X][pos.Y].west = true
			case westObj:
				grid[pos.X][pos.Y].west = true
				pos.X--
				gridSetIfNil(grid, pos, &cellObj{})
			default:
				drawGrid(grid, pos, move)
			}
		}
	} else {
		for _, o := range obj.options {
			drawGrid(grid, pos, o)
		}
	}
}

// func gridMinMax(area map[int]map[int]*cellObj) (int, int, int, int) {
// 	minX := 10000000000
// 	maxX := -10000000000
// 	minY := 10000000000
// 	maxY := -10000000000
// 	for x, line := range area {
// 		if x < minX {
// 			minX = x
// 		}
// 		if x > maxX {
// 			maxX = x
// 		}
// 		for y := range line {
// 			if y < minY {
// 				minY = y
// 			}
// 			if y > maxY {
// 				maxY = y
// 			}
// 		}
// 	}
// 	return minX, maxX, minY, maxY
// }

// func gridPrint(area map[int]map[int]*cellObj) {
// 	minX, maxX, minY, maxY := gridMinMax(area)
// 	for y := minY; y <= maxY; y++ {
// 		lineAbove := ""
// 		line := ""
// 		for x := minX; x <= maxX; x++ {
// 			val := area[x][y]
// 			if val != nil {
// 				if val.north {
// 					lineAbove += "#-"
// 				} else {
// 					lineAbove += "##"
// 				}
// 				if val.west {
// 					if x == 0 && y == 0 {
// 						line += "|X"
// 					} else {
// 						line += "|."
// 					}
// 				} else {
// 					if x == 0 && y == 0 {
// 						line += "#X"
// 					} else {
// 						line += "#."
// 					}
// 				}
// 			} else {
// 				if area[x][y-1] != nil {
// 					lineAbove += "##"
// 				} else {
// 					if area[x-1][y-1] != nil || area[x-1][y] != nil {
// 						lineAbove += "# "
// 					} else {
// 						lineAbove += "  "
// 					}
// 				}
// 				if area[x-1][y] != nil {
// 					line += "# "
// 				} else {
// 					line += "  "
// 				}
// 			}
// 		}
// 		if area[maxX][y-1] != nil || area[maxX][y] != nil {
// 			lineAbove += "#"
// 		}
// 		if area[maxX][y] != nil {
// 			line += "#"
// 		}
// 		log.Print(lineAbove)
// 		log.Print(line)
// 	}
// 	line := ""
// 	for x := minX; x <= maxX; x++ {
// 		if area[x][maxY] != nil {
// 			line += "##"
// 			if x == maxX {
// 				line += "#"
// 			}
// 		} else if area[x-1][maxY] != nil {
// 			line += "# "
// 		} else {
// 			line += "  "
// 		}
// 	}
// 	log.Print(line)
// }

func generateCountMap(grid map[int]map[int]*cellObj) map[int]map[int]int {
	countMap := make(map[int]map[int]int, len(grid))
	queue := []tools.Pos2D{{X: 0, Y: 0}}

	for len(queue) > 0 {
		pos := queue[0]
		queue = queue[1:]
		val := countMap[pos.X][pos.Y]
		cell := grid[pos.X][pos.Y]
		if cell.north {
			newPos := pos.Add(tools.Pos2D{X: 0, Y: -1})
			if countMap[newPos.X][newPos.Y] == 0 {
				tools.GridSet(countMap, newPos, val+1)
				queue = append(queue, newPos)
			}
		}
		if cell.west {
			newPos := pos.Add(tools.Pos2D{X: -1, Y: 0})
			if countMap[newPos.X][newPos.Y] == 0 {
				tools.GridSet(countMap, newPos, val+1)
				queue = append(queue, newPos)
			}
		}
		if col, ok := grid[pos.X+1]; ok && col != nil && col[pos.Y].west {
			newPos := pos.Add(tools.Pos2D{X: 1, Y: 0})
			if countMap[newPos.X][newPos.Y] == 0 {
				tools.GridSet(countMap, newPos, val+1)
				queue = append(queue, newPos)
			}
		}
		if col, ok := grid[pos.X]; ok && col[pos.Y+1] != nil && col[pos.Y+1].north {
			newPos := pos.Add(tools.Pos2D{X: 0, Y: 1})
			if countMap[newPos.X][newPos.Y] == 0 {
				tools.GridSet(countMap, newPos, val+1)
				queue = append(queue, newPos)
			}
		}
	}

	return countMap
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	root, _, err := readInput(lines[0][1:])
	if err != nil {
		return "", err
	}

	grid := make(map[int]map[int]*cellObj)
	grid[0] = map[int]*cellObj{0: {}}

	drawGrid(grid, tools.Pos2D{}, root)
	// gridPrint(grid)

	countMap := generateCountMap(grid)

	largest := 0
	for _, col := range countMap {
		for _, val := range col {
			if val > largest {
				largest = val
			}
		}
	}

	return strconv.Itoa(largest), nil // Return with the answer
}

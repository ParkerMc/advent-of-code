package day20

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

type dirT int

const (
	dirUp    dirT = 0
	dirDown  dirT = 1
	dirLeft  dirT = 2
	dirRight dirT = 3
)

type tileRotT struct {
	tile int
	rot  int
}

type tileT [][]bool
type tileGroupT []tileT

const tileSize = 10
const tileSizeM1 = tileSize - 1

// func (t tileT) print() {
// 	size := len(t)
// 	log.Print()
// 	for y := 0; y < size; y++ {
// 		line := ""
// 		for x := 0; x < size; x++ {
// 			if t[x][y] {
// 				line += "#"
// 			} else {
// 				line += "."
// 			}
// 		}
// 		log.Print(line)
// 	}
// }

func (t tileT) rotate90() tileT {
	size := len(t)
	newTile := make(tileT, size)
	for x := 0; x < size; x++ {
		newTile[x] = make([]bool, size)
		for y := 0; y < size; y++ {
			newTile[x][y] = t[size-y-1][x]
		}
	}
	return newTile
}

func (t tileT) flip() tileT {
	size := len(t)
	newTile := make(tileT, size)
	for x := 0; x < size; x++ {
		newTile[x] = make([]bool, size)
		for y := 0; y < size; y++ {
			newTile[x][y] = t[x][size-y-1]
		}
	}
	return newTile
}

func loadTiles(lines []string) (map[int]tileGroupT, []int, error) {
	var err error
	tileNum := 0
	startI := 0
	tiles := make(map[int]tileGroupT, 0)
	for i, line := range lines {
		if strings.HasPrefix(line, "Tile ") {
			tileNum, err = strconv.Atoi(line[5 : len(line)-1])
			if err != nil {
				return nil, nil, err
			}

			startI = i + 1
			tiles[tileNum] = make(tileGroupT, 8)
			tiles[tileNum][0] = make(tileT, tileSize)
			for i := range tiles[tileNum][0] {
				tiles[tileNum][0][i] = make([]bool, tileSize)
			}
			_, _ = tileNum, tiles
		} else if line != "" {
			y := i - startI
			for x, c := range line {
				if c == '#' {
					tiles[tileNum][0][x][y] = true
				}
			}
		}
	}

	tileNumbers := make([]int, 0)
	for tileNum := range tiles {
		tileNumbers = append(tileNumbers, tileNum)
		tiles[tileNum][1] = tiles[tileNum][0].rotate90()
		tiles[tileNum][2] = tiles[tileNum][1].rotate90()
		tiles[tileNum][3] = tiles[tileNum][2].rotate90()

		tiles[tileNum][4] = tiles[tileNum][0].flip()
		tiles[tileNum][5] = tiles[tileNum][4].rotate90()
		tiles[tileNum][6] = tiles[tileNum][5].rotate90()
		tiles[tileNum][7] = tiles[tileNum][6].rotate90()
	}

	return tiles, tileNumbers, nil
}

func getPossibleTiles(tiles map[int]tileGroupT, tileNumbers []int) map[int][][][]tileRotT { // [tile num][tile rot][dir] = []possible combos
	possibleTilesPerTile := make(map[int][][][]tileRotT, 0) // [tile num][tile rot][dir] = []possible combos
	for _, tileNum := range tileNumbers {
		possibleTilesPerTile[tileNum] = make([][][]tileRotT, 8)
		for i := 0; i < 8; i++ {
			possibleTilesPerTile[tileNum][i] = make([][]tileRotT, 4)
			for j := 0; j < 4; j++ {
				possibleTilesPerTile[tileNum][i][j] = make([]tileRotT, 0)
			}
		}
	}

	for i, tileNum := range tileNumbers {
		for rotNum, tile := range tiles[tileNum] {
			for _, tileNum2 := range tileNumbers[i+1:] {
				for rotNum2, tile2 := range tiles[tileNum2] {
					top, bottom, left, right := true, true, true, true
					for i := 0; i < tileSize; i++ {
						if top && tile[i][0] != tile2[i][tileSizeM1] {
							top = false
						}
						if bottom && tile[i][tileSizeM1] != tile2[i][0] {
							bottom = false
						}
						if left && tile[0][i] != tile2[tileSizeM1][i] {
							left = false
						}
						if right && tile[tileSizeM1][i] != tile2[0][i] {
							right = false
						}
					}

					if top {
						possibleTilesPerTile[tileNum][rotNum][dirUp] = append(possibleTilesPerTile[tileNum][rotNum][dirUp], tileRotT{tile: tileNum2, rot: rotNum2})
						possibleTilesPerTile[tileNum2][rotNum2][dirDown] = append(possibleTilesPerTile[tileNum2][rotNum2][dirDown], tileRotT{tile: tileNum, rot: rotNum})
					}
					if bottom {
						possibleTilesPerTile[tileNum][rotNum][dirDown] = append(possibleTilesPerTile[tileNum][rotNum][dirDown], tileRotT{tile: tileNum2, rot: rotNum2})
						possibleTilesPerTile[tileNum2][rotNum2][dirUp] = append(possibleTilesPerTile[tileNum2][rotNum2][dirUp], tileRotT{tile: tileNum, rot: rotNum})
					}
					if right {
						possibleTilesPerTile[tileNum][rotNum][dirRight] = append(possibleTilesPerTile[tileNum][rotNum][dirRight], tileRotT{tile: tileNum2, rot: rotNum2})
						possibleTilesPerTile[tileNum2][rotNum2][dirLeft] = append(possibleTilesPerTile[tileNum2][rotNum2][dirLeft], tileRotT{tile: tileNum, rot: rotNum})
					}
					if left {
						possibleTilesPerTile[tileNum][rotNum][dirLeft] = append(possibleTilesPerTile[tileNum][rotNum][dirLeft], tileRotT{tile: tileNum2, rot: rotNum2})
						possibleTilesPerTile[tileNum2][rotNum2][dirRight] = append(possibleTilesPerTile[tileNum2][rotNum2][dirRight], tileRotT{tile: tileNum, rot: rotNum})
					}
				}
			}
		}
	}
	return possibleTilesPerTile
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	tiles, tileNumbers, err := loadTiles(lines)
	if err != nil {
		return "", err
	}

	possibleTilesPerTile := getPossibleTiles(tiles, tileNumbers)

	// possibleTilesPerTile: [tile num][tile rot][dir] = []possible combos
	out := 1
	for tileNum, rots := range possibleTilesPerTile {
		count := 0
		for _, posCombos := range rots[0] {
			if len(posCombos) > 0 {
				count++
			}
		}
		if count == 2 {
			out *= tileNum
		}
	}

	return strconv.Itoa(out), nil // Return with the answer
}

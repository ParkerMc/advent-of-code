// Package day21 2018
package day21

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "713328"
const testAnswerTwo = "92399285032143"

var testFilename = "../test/private/" + tools.GetPackageName()

package day14

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// func draw(recipes []int, elfOne int, elfTwo int) {
// 	text := ""
// 	for i, recipe := range recipes {
// 		if i == elfOne {
// 			text += "(" + strconv.Itoa(recipe) + ")"
// 		} else if i == elfTwo {
// 			text += "[" + strconv.Itoa(recipe) + "]"
// 		} else {
// 			text += " " + strconv.Itoa(recipe) + " "
// 		}
// 	}
// 	log.Print(text)
// }

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	input, err := strconv.Atoi(lines[0])
	if err != nil {
		return "", err
	}
	recipes := make([]int, 0)
	recipes = append(recipes, 3, 7)
	elfOne := 0
	elfTwo := 1

	for len(recipes) < input+10 {
		for _, recipeChar := range strconv.Itoa(recipes[elfOne] + recipes[elfTwo]) {
			recipe, err := strconv.Atoi(string(recipeChar))
			if err != nil {
				return "", err
			}
			recipes = append(recipes, recipe)
		}
		elfOne += 1 + recipes[elfOne]
		if elfOne >= len(recipes) {
			elfOne %= len(recipes)
		}
		elfTwo += 1 + recipes[elfTwo]
		if elfTwo >= len(recipes) {
			elfTwo %= len(recipes)
		}
	}
	answer := ""
	for i := input; i < input+10; i++ {
		answer += strconv.Itoa(recipes[i])
	}
	return answer, nil // Return with the answer
}

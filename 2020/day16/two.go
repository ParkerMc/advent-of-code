package day16

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	validNumsLargest := 0
	fields, myTicket, tickets, err := loadData(lines)
	if err != nil {
		return "", err
	}

	for _, field := range fields {
		if field.max1 > validNumsLargest {
			validNumsLargest = field.max1
		}
		if field.max2 > validNumsLargest {
			validNumsLargest = field.max2
		}
	}
	validNums := make([]bool, validNumsLargest+1)
	for _, field := range fields {
		for _, bounds := range [][]int{{field.min1, field.max1}, {field.min2, field.max2}} {
			for i := bounds[0]; i <= bounds[1]; i++ {
				validNums[i] = true
			}
		}
	}

	posFields := make([][]bool, len(fields)) // [Index of field type][Field#]
	for i := range posFields {
		posFields[i] = make([]bool, len(fields))
		for j := range posFields[i] {
			posFields[i][j] = true
		}
	}

	for _, ticket := range append(tickets, myTicket) {
		valid := true
		for _, val := range ticket {
			if val > validNumsLargest || !validNums[val] {
				valid = false
				break
			}
		}
		if valid {
			for i, val := range ticket {
				for j, rule := range fields {
					if posFields[j][i] {
						if !((val >= rule.min1 && val <= rule.max1) || (val >= rule.min2 && val <= rule.max2)) {
							posFields[j][i] = false
						}
					}
				}

			}
		}
	}

	assignments := make([]int, len(fields)) // [Index of field type] = Field#
	for i := range assignments {
		assignments[i] = -1
	}

	used := make([]bool, len(posFields[0]))
	needFind := len(fields)

	for needFind > 0 { // [Index of field type][Field#]
		for fieldType, fieldMap := range posFields {
			if assignments[fieldType] == -1 {
				i := -1
				for field, ok := range fieldMap {
					if ok && !used[field] {
						if i == -1 {
							i = field
						} else {
							i = -1
							break
						}
					}
				}
				if i != -1 {
					assignments[fieldType] = i
					used[i] = true
					needFind--
				}
			}
		}
	}

	out := 1
	for i, field := range fields {
		if len(field.name) > 9 && field.name[:9] == "departure" {
			out *= myTicket[assignments[i]]
		}
	}

	return strconv.Itoa(out), nil // Return with the answer
}

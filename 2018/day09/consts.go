// Package day09 2018
package day09

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "386151"
const testAnswerTwo = "3211264152"

var testFilename = "../test/private/" + tools.GetPackageName()

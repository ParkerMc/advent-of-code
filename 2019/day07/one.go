package day07

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	nums, err := tools.CreateIntcodeData(lines[0])
	if err != nil {
		return "", err
	}

	most := 0
	for ap := 0; ap < 5; ap++ {
		for bp := 0; bp < 5; bp++ {
			if ap == bp {
				continue
			}
			for cp := 0; cp < 5; cp++ {
				if ap == cp || bp == cp {
					continue
				}
				for dp := 0; dp < 5; dp++ {
					if ap == dp || bp == dp || cp == dp {
						continue
					}
					for ep := 0; ep < 5; ep++ {
						if ap == ep || bp == ep || cp == ep || dp == ep {
							continue
						}
						in := 0
						for _, i := range []int{ap, bp, cp, dp, ep} {
							p := tools.NewProgramFromData(nums)
							p.AddInput(i)
							p.AddInput(in)
							err = p.Run()
							if err != nil {
								return "", err
							}
							in, err = p.RemoveOutput()
							if err != nil {
								return "", err
							}
						}
						if in > most {
							most = in
						}
					}
				}
			}
		}
	}

	return strconv.Itoa(most), nil // Return with the answer
}

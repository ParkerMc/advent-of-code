package day13

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	canLeaveAfter, err := strconv.Atoi(lines[0])
	if err != nil {
		return "", err
	}

	busses := make([]int, 0)
	split := strings.Split(lines[1], ",")
	for _, strBus := range split {
		if strBus != "x" {
			bus, err := strconv.Atoi(strBus)
			if err != nil {
				return "", err
			}
			busses = append(busses, bus)
		}
	}

	soonestBusI := 0
	leaveTimes := make([]int, len(busses))
	for i, bus := range busses {
		time := bus
		for time < canLeaveAfter {
			time += bus
		}
		leaveTimes[i] = time
		if time < leaveTimes[soonestBusI] {
			soonestBusI = i
		}
	}

	return strconv.Itoa(busses[soonestBusI] * (leaveTimes[soonestBusI] - canLeaveAfter)), nil // Return with the answer
}

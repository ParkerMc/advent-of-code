package day17

import (
	"errors"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

func getImage(line string) (string, error) {
	p, err := tools.NewProgram(line)
	if err != nil {
		return "", err
	}
	err = p.Run()
	if err != nil {
		return "", err
	}
	image := ""
	dat, err := p.RemoveOutput()
	for err == nil {
		image += string(rune(dat))
		dat, err = p.RemoveOutput()
	}
	if !p.GetStopped() {
		return "", errors.New("program isn't done")
	}
	return image[:len(image)-2], nil
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	image, err := getImage(lines[0])
	if err != nil {
		return "", err
	}
	imgLines := strings.Split(image, "\n")

	out := 0
	for y, row := range imgLines {
		for x, val := range row {
			if val == '#' && (x > 0 && row[x-1] == '#') && (x+1 < len(row) && row[x+1] == '#') && (y > 0 && imgLines[y-1][x] == '#') && (y+1 < len(imgLines) && imgLines[y+1][x] == '#') {
				out += x * y
			}
		}
	}

	return strconv.Itoa(out), nil // Return with the answer
}

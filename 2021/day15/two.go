package day15

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

func loopAround(val int) int {
	if val > 9 {
		return val - 9
	}
	return val
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {
	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	orgWidth := len(lines[0])
	orgHeigth := len(lines)
	grid := make([][]int, orgWidth*5)

	for x := range grid {
		grid[x] = make([]int, orgHeigth*5)
	}
	for y, row := range lines {
		for x, val := range row {
			nextX := x
			nextVal := int(val) - 48
			for i := 0; i < 5; i++ {
				grid[nextX][y] = nextVal
				nextX += orgWidth
				nextVal = loopAround(nextVal + 1)
			}
		}
	}
	for _, col := range grid {
		for y := 0; y < orgHeigth; y++ {
			nextY := y
			nextVal := col[y]
			for i := 1; i < 5; i++ {
				nextY += orgHeigth
				nextVal = loopAround(nextVal + 1)
				col[nextY] = nextVal
			}
		}
	}
	goal := tools.Pos2D{X: len(grid) - 1, Y: len(grid[0]) - 1}

	answer, err := solve(grid, goal)
	if err != nil {
		return "", err
	}

	return strconv.Itoa(answer), nil // return answer
}

package day24

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {
	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	armies, err := loadData(lines)
	if err != nil {
		return "", err
	}

	// It's now efficent enough to just brute force
	for boost := 0; ; boost++ {
		winner, res := runSim(armies, boost)
		if winner == immune {
			return strconv.Itoa(res), nil
		}
	}
}

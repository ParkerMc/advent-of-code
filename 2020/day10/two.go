package day10

import (
	"math"
	"sort"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	length := len(lines) + 2
	nums := make([]int, length)
	for i, strNum := range lines {
		nums[i], err = strconv.Atoi(strNum)
		if err != nil {
			return "", err
		}
	}
	nums[length-2] = 0
	nums[length-1] = math.MaxInt32
	sort.Sort(sort.Reverse(sort.IntSlice(nums)))
	nums[0] = nums[1] + 3

	ways := make(map[int]int, length)
	for i, jolts := range nums {
		if i == 0 {
			ways[jolts] = 1
			continue
		}
		curWays := 0
		if val, ok := ways[jolts+1]; ok {
			curWays += val
		}
		if val, ok := ways[jolts+2]; ok {
			curWays += val
		}
		if val, ok := ways[jolts+3]; ok {
			curWays += val
		}
		ways[jolts] = curWays
	}

	return strconv.Itoa(ways[0]), nil // Return with the answer
}

// Package day03 2020
package day03

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "262"
const testAnswerTwo = "2698900776"

var testFilename = "../test/private/" + tools.GetPackageName()

package day07

import (
	"math"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

var costMap = make([]int, 1)

func getCost(dist int) int {
	if dist == 0 {
		return 0
	}

	if len(costMap) <= dist {
		calcTil(dist)
	}

	return costMap[dist]
}

func calcTil(dist int) {
	last := costMap[len(costMap)-1]
	for i := len(costMap); i <= dist; i++ {
		last = last + i
		costMap = append(costMap, last)
	}
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	split := strings.Split(lines[0], ",")
	crabs := make([]int, len(split))
	for i, crab := range split {
		crabs[i], err = strconv.Atoi(crab)
		if err != nil {
			return "", err
		}
	}

	minV := math.MaxInt
	maxV := 0
	for _, crab := range crabs {
		if crab < minV {
			minV = crab
		}
		if crab > maxV {
			maxV = crab
		}
	}

	answerMap := make(map[int]int, maxV-minV)
	for _, crab := range crabs {
		for i := minV; i <= maxV; i++ {
			answerMap[i] += getCost(tools.Abs(crab - i))
		}
	}

	answer := math.MaxInt
	for _, val := range answerMap {
		if val < answer {
			answer = val
		}
	}

	return strconv.Itoa(answer), nil // Return with the answer
}

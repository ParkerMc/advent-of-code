use std::{
    collections::{hash_map::Entry, HashMap},
    time::Duration,
    usize,
};

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    input
        .join("")
        .split(",")
        .map(|p| p.chars().fold(0, |acc, c| ((acc + (c as u64)) * 17) % 256))
        .sum::<u64>()
        .to_string()
}

fn part2(input: &Vec<String>) -> String {
    let mut map: HashMap<String, (usize, usize)> = HashMap::new();

    for (i, str) in input.join("").split(",").enumerate() {
        let split = str.split("=").collect_vec();
        if split.len() > 1 {
            match map.entry(split[0].to_owned()) {
                Entry::Occupied(mut v) => v.get_mut().1 = split[1].parse().expect("Expected num"),
                Entry::Vacant(v) => {
                    v.insert((i, split[1].parse().expect("Expected num")));
                }
            }
        }
        if str.ends_with('-') {
            map.remove(&str[0..str.len() - 1].to_string());
        }
    }

    let boxes = map.iter().into_group_map_by(|(key, _)| {
        key.chars()
            .fold(0, |acc, c| ((acc + (c as usize)) * 17) % 256)
    });

    boxes
        .iter()
        .flat_map(|(box_i, v)| {
            v.iter()
                .sorted_by_key(|(_, (i, _))| i)
                .enumerate()
                .map(move |(i, (_, (_, f)))| (box_i + 1) * (i + 1) * f)
        })
        .sum::<usize>()
        .to_string()
}

// Package day24 2018
package day24

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "28976"
const testAnswerTwo = "3534"

var testFilename = "../test/private/" + tools.GetPackageName()

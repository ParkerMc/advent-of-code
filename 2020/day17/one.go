package day17

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

var posesAround = getPosesAround()

func getPosesAround() []tools.Pos3D {
	list := make([]tools.Pos3D, 26)
	for i, x := range []int{-1, 1, 0} {
		for j, y := range []int{-1, 1, 0} {
			for k, z := range []int{-1, 1, 0} {
				if i == 2 && j == 2 && k == 2 {
					break
				}
				list[i*9+j*3+k] = tools.Pos3D{X: x, Y: y, Z: z}
			}
		}
	}
	return list
}

// func print(area map[int]map[int]map[int]bool, min, max tools.Pos3D) {
// 	for z := min.Z; z <= max.Z; z++ {
// 		log.Printf("z=%d", z)
// 		for y := min.Y; y <= max.Y; y++ {
// 			line := ""
// 			for x := min.X; x <= max.X; x++ {
// 				if area[x][y][z] {
// 					line += "#"
// 				} else {
// 					line += "."
// 				}
// 			}
// 			log.Print(line)
// 		}
// 		log.Print()
// 	}

// }

func set(area map[int]map[int]map[int]bool, x, y, z int, active bool) {
	if _, ok := area[x]; !ok {
		area[x] = make(map[int]map[int]bool, 0)
	}
	if _, ok := area[x][y]; !ok {
		area[x][y] = make(map[int]bool, 0)
	}
	area[x][y][z] = active
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	min := tools.Pos3D{X: 0, Y: 0, Z: 0}
	max := tools.Pos3D{X: len(lines[0]) - 1, Y: len(lines) - 1, Z: 0}
	area := make(map[int]map[int]map[int]bool, 0)
	for y, row := range lines {
		for x, c := range row {
			if c == '#' {
				set(area, x, y, 0, true)
			}
		}
	}

	for i := 0; i < 6; i++ {
		toSwap := make([]tools.Pos3D, 0)
		for x := min.X - 1; x <= max.X+1; x++ {
			for y := min.Y - 1; y <= max.Y+1; y++ {
				for z := min.Z - 1; z <= max.Z+1; z++ {
					numAround := 0
					for _, pos := range posesAround {
						if area[x+pos.X][y+pos.Y][z+pos.Z] {
							numAround++
							if numAround > 3 {
								break
							}
						}
					}
					if area[x][y][z] { // Active
						if numAround != 2 && numAround != 3 {
							toSwap = append(toSwap, tools.Pos3D{X: x, Y: y, Z: z})
						}
					} else { // not active
						if numAround == 3 {
							toSwap = append(toSwap, tools.Pos3D{X: x, Y: y, Z: z})
						}
					}
				}
			}
		}

		for _, swap := range toSwap {
			newVal := !area[swap.X][swap.Y][swap.Z]
			if newVal {
				if swap.X < min.X {
					min.X = swap.X
				}
				if swap.Y < min.Y {
					min.Y = swap.Y
				}
				if swap.Z < min.Z {
					min.Z = swap.Z
				}
				if swap.X > max.X {
					max.X = swap.X
				}
				if swap.Y > max.Y {
					max.Y = swap.Y
				}
				if swap.Z > max.Z {
					max.Z = swap.Z
				}
			}
			set(area, swap.X, swap.Y, swap.Z, newVal)
		}
	}

	actives := 0
	for _, val := range area {
		for _, val2 := range val {
			for _, val3 := range val2 {
				if val3 {
					actives++
				}
			}
		}
	}

	return strconv.Itoa(actives), nil // Return with the answer
}

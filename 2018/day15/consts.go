// Package day15 2018
package day15

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "214731"
const testAnswerTwo = "53222"

var testFilename = "../test/private/" + tools.GetPackageName()

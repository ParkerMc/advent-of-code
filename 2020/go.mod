module gitlab.com/parkermc/advent-of-code/2020

go 1.17

require github.com/golang-collections/go-datastructures v0.0.0-20150211160725-59788d5eb259

require github.com/stretchr/testify v1.7.0 // indirect

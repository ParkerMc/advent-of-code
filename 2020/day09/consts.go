// Package day09 2020
package day09

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "675280050"
const testAnswerTwo = "96081673"

var testFilename = "../test/private/" + tools.GetPackageName()

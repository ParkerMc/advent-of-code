use std::time::Duration;

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    solve(input, false)
}

fn part2(input: &Vec<String>) -> String {
    solve(input, true)
}

fn solve(input: &Vec<String>, allow_one_error: bool) -> String {
    input
        .iter()
        .filter_map(|l| {
            let nums = l
                .split_whitespace()
                .map(|n| n.parse::<i64>().expect("Expected number"))
                .collect_vec();
            let mut attempts = vec![nums.clone()];
            if allow_one_error {
                for i in 0..(nums.len()) {
                    let mut clone = nums.clone();
                    clone.remove(i);
                    attempts.push(clone);
                }
            }
            let any = attempts.into_iter().any(|v| is_valid(&v).0);
            if any {
                return Some(());
            }
            return None;
        })
        .count()
        .to_string()
}

fn is_valid(nums: &Vec<i64>) -> (bool, usize) {
    let mut num = nums.get(0).expect("Expected first");
    let positive = num < nums.get(1).expect("Expected second");
    for (i, next) in nums.iter().enumerate().skip(1) {
        let diff = (next - num).abs();
        if diff == 0 || diff > 3 || (positive && num > next) || (!positive && num < next) {
            return (false, i);
        }
        num = next;
    }
    (true, 0)
}

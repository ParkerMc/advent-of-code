// Package day17 2019
package day17

import "gitlab.com/parkermc/advent-of-code/2019/tools"

const testAnswerOne = "7404"
const testAnswerTwo = "929045"

var testFilename = "../test/private/" + tools.GetPackageName()

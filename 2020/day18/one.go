package day18

import (
	"container/list"
	"errors"
	"fmt"
	"strconv"
	"unicode"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

type storeType int
type opType int

const (
	typeNum      storeType = 1
	typeOP       storeType = 2
	typeEquation storeType = 3

	opAdd   opType = 1
	opMulti opType = 2
)

type store struct {
	sType    storeType
	num      int
	op       opType
	equation *list.List
}

func solvePT(firstElement *list.Element, equation *list.List, pt int) error {
	var err error

	firstStore := firstElement.Value.(*store)
	if firstStore.sType == typeEquation {
		if pt == 1 {
			firstStore.num, err = solve(firstStore.equation)
		} else {
			firstStore.num, err = solve2(firstStore.equation)
		}
		if err != nil {
			return err
		}
		firstStore.sType = typeNum
	} else if firstStore.sType != typeNum {
		return fmt.Errorf("store type not number or equation: %d", firstStore.sType)
	}

	opElement := firstElement.Next()
	opStore := opElement.Value.(*store)
	if opStore.sType != typeOP {
		return fmt.Errorf("store type not OP: %d", opStore.sType)
	}

	secondElement := opElement.Next()
	secondStore := secondElement.Value.(*store)
	if secondStore.sType == typeEquation {
		if pt == 1 {
			secondStore.num, err = solve(secondStore.equation)
		} else {
			secondStore.num, err = solve2(secondStore.equation)
		}
		if err != nil {
			return err
		}
		secondStore.sType = typeNum
	} else if secondStore.sType != typeNum {
		return fmt.Errorf("store type not number or equation: %d", secondStore.sType)
	}
	switch opStore.op {
	case opMulti:
		firstStore.num *= secondStore.num
	case opAdd:
		firstStore.num += secondStore.num
	default:
		return fmt.Errorf("unknown op: %d", opStore.op)
	}
	equation.Remove(opElement)
	equation.Remove(secondElement)
	return nil
}

func load(line string) (*list.List, error) {
	equationParts := list.New()
	equation := list.New()
	for _, c := range line {
		if unicode.IsDigit(c) {
			equation.PushBack(&store{sType: typeNum, num: int(c - '0')})
			continue
		}
		switch c {
		case '(':
			equationParts.PushBack(equation)
			equation = list.New()
		case ')':
			last := equationParts.Back()
			equationParts.Remove(last)
			newlist := last.Value.(*list.List)
			newlist.PushBack(&store{sType: typeEquation, equation: equation})
			equation = newlist
		case '+':
			equation.PushBack(&store{sType: typeOP, op: opAdd})
		case '*':
			equation.PushBack(&store{sType: typeOP, op: opMulti})
		case ' ':
		default:
			return nil, fmt.Errorf("unknown char: %c", c)
		}
	}

	if equationParts.Len() > 0 {
		return nil, errors.New("unmatched parentheses")
	}
	return equation, nil
}

func solve(equation *list.List) (int, error) {
	for equation.Len() > 1 {
		err := solvePT(equation.Front(), equation, 1)
		if err != nil {
			return 0, err
		}
	}
	return equation.Front().Value.(*store).num, nil
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	out := 0

	for _, line := range lines {
		equation, err := load(line)
		if err != nil {
			return "", err
		}

		val, err := solve(equation)
		if err != nil {
			return "", err
		}
		out += val
	}

	return strconv.Itoa(out), nil // Return with the answer
}

package day04

import (
	"regexp"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

var hclreg = regexp.MustCompile(`^#[0-9a-f]{6}$`)

func (i *id) isValid2() bool {
	byrI, err := strconv.Atoi(i.byr)
	if err != nil {
		return false
	}
	iyrI, err := strconv.Atoi(i.iyr)
	if err != nil {
		return false
	}
	eyrI, err := strconv.Atoi(i.eyr)
	if err != nil {
		return false
	}

	if len(i.hgt) < 2 {
		return false
	}
	hgtI, err := strconv.Atoi(string(i.hgt[:len(i.hgt)-2]))
	if err != nil {
		return false
	}
	return (i.byr != "" && byrI >= 1920 && byrI <= 2002 &&
		i.iyr != "" && iyrI >= 2010 && iyrI <= 2020 &&
		i.eyr != "" && eyrI >= 2020 && eyrI <= 2030 &&
		i.hgt != "" && ((i.hgt[len(i.hgt)-2:] == "cm" && hgtI >= 150 && hgtI <= 193) || (i.hgt[len(i.hgt)-2:] == "in") && hgtI >= 59 && hgtI <= 76) &&
		hclreg.MatchString(i.hcl) &&
		(i.ecl == "amb" || i.ecl == "blu" || i.ecl == "brn" || i.ecl == "gry" || i.ecl == "grn" || i.ecl == "hzl" || i.ecl == "oth") &&
		len(i.pid) == 9)
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	return strconv.Itoa(getValidIDs(lines, false)), nil // Return with the answer
}

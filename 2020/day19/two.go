package day19

import (
	"regexp"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	rules, strs, err := load(lines)
	if err != nil {
		return "", err
	}

	rules[8] = ruleT{isStr: false, nums: [][]int{{42}, {42, 8}}}
	rules[11] = ruleT{isStr: false, nums: [][]int{{42, 31}, {42, 11, 31}}}

	max := 0
	for _, str := range strs {
		legnth := len(str)
		if legnth > max {
			max = legnth
		}
	}

	regstr, _, _ := rules[0].getPart(rules, 0, max)
	regex, err := regexp.Compile("^" + regstr + "$")
	if err != nil {
		return "", err
	}

	matched := 0
	for _, str := range strs {
		if regex.MatchString(str) {
			matched++
		}
	}
	return strconv.Itoa(matched), nil // Return with the answer
}

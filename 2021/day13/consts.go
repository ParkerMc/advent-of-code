// Package day13 2018
package day13

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "618"
const testAnswerTwo = ` ##  #    ###  #### #  # #### #  # #  #
#  # #    #  # #    # #  #    # #  #  #
#  # #    #  # ###  ##   ###  ##   #  #
#### #    ###  #    # #  #    # #  #  #
#  # #    # #  #    # #  #    # #  #  #
#  # #### #  # #### #  # #    #  #  ## `

var testFilename = "../test/private/" + tools.GetPackageName()

// Package day15 2018
package day15

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "717"
const testAnswerTwo = "2993"

var testFilename = "../test/private/" + tools.GetPackageName()

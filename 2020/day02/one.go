package day02

import (
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	infoReg, err := regexp.Compile(`(\d+)-(\d+) (.+): (.+)`)
	if err != nil {
		return "", err
	}

	valid := 0
	for _, line := range lines {
		match := infoReg.FindStringSubmatch(line)
		polyMin, err := strconv.Atoi(match[1])
		if err != nil {
			return "", err
		}
		polyMax, err := strconv.Atoi(match[2])
		if err != nil {
			return "", err
		}
		poly := match[3]
		password := match[4]

		count := strings.Count(password, poly)
		if count >= polyMin && count <= polyMax {
			valid++
		}
	}

	return strconv.Itoa(valid), nil // Return with the answer
}

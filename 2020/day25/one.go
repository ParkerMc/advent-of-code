package day25

import (
	"log"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	cardPubKey, err := strconv.Atoi(lines[0])
	if err != nil {
		return "", err
	}
	doorPubKey, err := strconv.Atoi(lines[1])
	if err != nil {
		return "", err
	}

	value := 1
	cardLoopNum := 0
	for value != cardPubKey {
		value *= 7
		value %= 20201227
		cardLoopNum++
	}
	log.Print(cardLoopNum)

	encryptionKey := 1
	for i := 0; i < cardLoopNum; i++ {
		encryptionKey *= doorPubKey
		encryptionKey %= 20201227
	}

	return strconv.Itoa(encryptionKey), nil // Return with the answer
}

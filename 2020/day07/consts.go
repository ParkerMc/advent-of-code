// Package day07 2020
package day07

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "213"
const testAnswerTwo = "38426"

var testFilename = "../test/private/" + tools.GetPackageName()

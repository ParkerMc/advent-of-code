// Package day15 2019
package day15

import "gitlab.com/parkermc/advent-of-code/2019/tools"

const testAnswerOne = "252"
const testAnswerTwo = "350"

var testFilename = "../test/private/" + tools.GetPackageName()

package tools

import "testing"

func TestAbs(t *testing.T) {
	if Abs(5) != 5 || Abs(-5) != 5 {
		t.Fatal("Abs error")
	}
}

func TestGcd(t *testing.T) {
	if Gcd(144, 132) != 12 {
		t.Fatal("Gcd error")
	}
}

func TestLcm(t *testing.T) {
	if Lcm(15, 20) != 60 {
		t.Fatal("Lcm error")
	}
}

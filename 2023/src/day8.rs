use std::{collections::HashMap, time::Duration};

use itertools::Itertools;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    let (instructions, nodes) = load_data(input);
    solve(&instructions, &nodes, "AAA", "ZZZ").to_string()
}

fn part2(input: &Vec<String>) -> String {
    let (instructions, nodes) = load_data(input);

    let answers = nodes
        .keys()
        .filter(|n| n.ends_with("A"))
        .map(|n| solve(&instructions, &nodes, n, "Z"))
        .collect_vec();
    answers
        .into_iter()
        .reduce(|a, b| lcm(a, b))
        .expect("Expected reduce")
        .to_string()
}

fn gcd(mut a: u64, mut b: u64) -> u64 {
    if a == b {
        return a;
    }
    if b > a {
        let temp = a;
        a = b;
        b = temp;
    }
    while b > 0 {
        let temp = a;
        a = b;
        b = temp % b;
    }
    a
}

fn lcm(a: u64, b: u64) -> u64 {
    a * (b / gcd(a, b))
}

fn load_data(input: &Vec<String>) -> (Vec<char>, HashMap<&str, (String, String)>) {
    let instructions = input[0].trim().chars().collect_vec();
    let nodes: HashMap<_, _> = input
        .iter()
        .skip(2)
        .map(|l| {
            let split = l.split('=').collect_vec();
            let dests: (String, String) = split[1]
                .replace("(", "")
                .replace(")", "")
                .split(',')
                .map(|p| p.trim().to_string())
                .collect_tuple()
                .expect("Expected 2 tuple");
            (split[0].trim(), dests)
        })
        .collect();
    (instructions, nodes)
}

fn solve(
    instructions: &Vec<char>,
    nodes: &HashMap<&str, (String, String)>,
    starting: &str,
    ending: &str,
) -> u64 {
    let mut instructions_i = 0;
    let mut steps: u64 = 0;
    let mut current = starting;
    while !current.ends_with(ending) {
        current = match instructions[instructions_i] {
            'L' => &nodes.get(current).expect("Failed to find a node").0,
            'R' => &nodes.get(current).expect("Failed to find a node").1,
            _ => panic!("Bad instruction: {}", instructions[instructions_i]),
        };
        steps += 1;
        instructions_i = (instructions_i + 1) % instructions.len();
    }
    steps
}

package day22

import (
	"container/list"
	"log"
	"sort"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

const print2 = false

var game int

func decksEqualPastDeck(list []*list.List, arr [][]*list.List) bool {
	for _, decks := range arr {
		if decksEqualPast(list, decks) {
			return true
		}
	}
	return false
}

func decksEqualPast(list []*list.List, decks []*list.List) bool {
	for i := 0; i < len(decks); i++ {
		if decks[i].Len() != list[i].Len() {
			return false
		}
	}
	for i, deck := range decks {
		current := list[i].Front()
		current2 := deck.Front()
		legnth := deck.Len()
		for j := 0; j < legnth; j++ {
			if current2.Value.(int) != current.Value.(int) {
				return false
			}
			current = current.Next()
			current2 = current2.Next()
		}
	}
	return true
}

func play2(decks []*list.List, palyerCount, totalCardCount int) int {
	game++
	curGame := game
	pastDecks := make([][]*list.List, 0)
	playerWon := -1
	if print2 {
		log.Printf("=== Game %d ===", curGame)
	}
	for i := 1; playerWon == -1; i++ {
		if print2 {
			log.Printf("-- Round %d (Game %d) --", i, curGame)
			printDecks(decks)
		}

		if decksEqualPastDeck(decks, pastDecks) {
			playerWon = 0
			break
		}

		decksCopy := make([]*list.List, len(decks))
		for i, deck := range decks {
			decksCopy[i] = list.New()
			decksCopy[i].PushBackList(deck)
		}
		pastDecks = append(pastDecks, decksCopy)

		currentCards := make([]int, palyerCount)
		for player, deck := range decks {
			cardE := deck.Front()
			if cardE != nil {
				deck.Remove(cardE)
				currentCards[player] = cardE.Value.(int)
				if print2 {
					log.Printf("Player %d plays: %d", player+1, cardE.Value.(int))
				}
			}
		}

		normal := false
		for player, card := range currentCards {
			if card > decks[player].Len() {
				normal = true
				break
			}
		}
		winningPlayer := 0
		if normal {
			highestVal := 0
			for player, card := range currentCards {
				if card > highestVal {
					highestVal = card
					winningPlayer = player
				}
			}
			sort.Sort(sort.Reverse(sort.IntSlice(currentCards)))
		} else {
			newDecks := make([]*list.List, len(decks))
			newCardCount := 0
			for player, deck := range decks {
				newCardCount += currentCards[player]
				newDecks[player] = list.New()
				currentCard := deck.Front()
				for j := 0; j < currentCards[player]; j++ {
					newDecks[player].PushBack(currentCard.Value)
					currentCard = currentCard.Next()
				}
			}
			winningPlayer = play2(newDecks, palyerCount, newCardCount)

			if winningPlayer == 1 {
				tmp := currentCards[0]
				currentCards[0] = currentCards[1]
				currentCards[1] = tmp
			}
		}

		if print2 {
			log.Printf("Player %d wins the Round!", winningPlayer+1)
		}

		for _, card := range currentCards {
			if card != 0 {
				decks[winningPlayer].PushBack(card)
			}
		}

		for player, deck := range decks {
			if deck.Len() == totalCardCount {
				playerWon = player
			}
		}
	}

	if print2 {
		log.Print("== Post-game results ==")
		printDecks(decks)
	}

	return playerWon
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	decks, palyerCount, totalCardCount, err := load(lines)
	if err != nil {
		return "", err
	}

	game = 0
	playerWon := play2(decks, palyerCount, totalCardCount)

	return strconv.Itoa(countWinner(decks, playerWon)), nil // Return with the answer
}

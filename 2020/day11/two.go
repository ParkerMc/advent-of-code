package day11

import (
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	xCount := len(lines[0])
	yCount := len(lines)
	seats := make([][]GridPos, xCount)
	for i := 0; i < xCount; i++ {
		seats[i] = make([]GridPos, yCount)
	}

	for y, line := range lines {
		for x, c := range line {
			switch c {
			case 'L':
				seats[x][y] = GridPosSeatEmpty
			case '.':
				seats[x][y] = GridPosFloor
			default:
				return "", fmt.Errorf("unknown char in data: %c", c)
			}
		}
	}

	for {
		toSwap := make([]tools.Pos2D, 0)
		for x, cols := range seats {
			for y, val := range cols {
				occupied := 0
				for change := 1; y-change >= 0; change++ {
					val := cols[y-change]
					if val != GridPosFloor {
						if val == GridPosSeatOccupied {
							occupied++
						}
						break
					}
				}
				for change := 1; y+change < yCount; change++ {
					val := cols[y+change]
					if val != GridPosFloor {
						if val == GridPosSeatOccupied {
							occupied++
						}
						break
					}
				}
				for change := 1; x-change > -1; change++ {
					val := seats[x-change][y]
					if val != GridPosFloor {
						if val == GridPosSeatOccupied {
							occupied++
						}
						break
					}
				}
				for change := 1; x-change > -1 && y-change > -1; change++ {
					val := seats[x-change][y-change]
					if val != GridPosFloor {
						if val == GridPosSeatOccupied {
							occupied++
						}
						break
					}
				}
				for change := 1; x-change > -1 && y+change < yCount; change++ {
					val := seats[x-change][y+change]
					if val != GridPosFloor {
						if val == GridPosSeatOccupied {
							occupied++
						}
						break
					}
				}

				for change := 1; x+change < xCount; change++ {
					val := seats[x+change][y]
					if val != GridPosFloor {
						if val == GridPosSeatOccupied {
							occupied++
						}
						break
					}
				}
				for change := 1; x+change < xCount && y-change > -1; change++ {
					val := seats[x+change][y-change]
					if val != GridPosFloor {
						if val == GridPosSeatOccupied {
							occupied++
						}
						break
					}
				}
				for change := 1; x+change < xCount && y+change < yCount; change++ {
					val := seats[x+change][y+change]
					if val != GridPosFloor {
						if val == GridPosSeatOccupied {
							occupied++
						}
						break
					}
				}

				if val == GridPosSeatEmpty && occupied == 0 {
					toSwap = append(toSwap, tools.Pos2D{X: x, Y: y})
				} else if val == GridPosSeatOccupied && occupied >= 5 {
					toSwap = append(toSwap, tools.Pos2D{X: x, Y: y})
				}
			}
		}

		for _, swap := range toSwap {
			switch seats[swap.X][swap.Y] {
			case GridPosSeatEmpty:
				seats[swap.X][swap.Y] = GridPosSeatOccupied
			case GridPosSeatOccupied:
				seats[swap.X][swap.Y] = GridPosSeatEmpty
			default:
				return "", errors.New("invalid swap pos")
			}
		}
		if len(toSwap) == 0 {
			count := 0
			for _, cols := range seats {
				for _, val := range cols {
					if val == GridPosSeatOccupied {
						count++
					}
				}
			}
			return strconv.Itoa(count), nil
		}
	}
}

package day11

import (
	"strconv"

	"github.com/golang-collections/collections/set"
	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

const (
	black = 0
	white = 1

	left  = 0
	right = 1
	up    = 2
	down  = 3
)

type robot struct {
	pos tools.Pos2D
	dir int
}

func paint(area map[int]map[int]int, pos tools.Pos2D, color int) {
	if _, ok := area[pos.X]; !ok {
		if color == black {
			return
		}
		area[pos.X] = make(map[int]int, 0)
	}
	area[pos.X][pos.Y] = color
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	program, err := tools.NewProgram(lines[0])
	if err != nil {
		return "", err
	}

	painted := set.New()
	area := make(map[int]map[int]int, 0)
	robot := robot{pos: tools.Pos2D{X: 0, Y: 0}, dir: up}

	first := true
	err = program.Run()
	if err != nil {
		return "", err
	}
	for !program.GetStopped() {
		if !first {
			color, err := program.RemoveOutput()
			if err != nil {
				return "", err
			}
			move, err := program.RemoveOutput()
			if err != nil {
				return "", err
			}
			paint(area, robot.pos, color)
			painted.Insert(tools.Pos2D{X: robot.pos.X, Y: robot.pos.Y})
			if (robot.dir == up && move == right) || (robot.dir == down && move == left) {
				robot.dir = right
			} else if (robot.dir == down && move == right) || (robot.dir == up && move == left) {
				robot.dir = left
			} else if (robot.dir == left && move == left) || (robot.dir == right && move == right) {
				robot.dir = down
			} else if (robot.dir == right && move == left) || (robot.dir == left && move == right) {
				robot.dir = up
			}
			switch robot.dir {
			case up:
				robot.pos.Y++
			case down:
				robot.pos.Y--
			case left:
				robot.pos.X--
			case right:
				robot.pos.X++
			}
		} else {
			first = false
		}
		color := 0
		if val, ok := area[robot.pos.X]; ok && val[robot.pos.Y] == 1 {
			color = 1
		}
		program.AddInput(color)
		err = program.Run()
		if err != nil {
			return "", err
		}
	}

	return strconv.Itoa(painted.Len()), nil // Return with the answer
}

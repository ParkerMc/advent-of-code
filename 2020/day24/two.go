package day24

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2020/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	isBlack, _, _, err := load(lines)
	if err != nil {
		return "", err
	}

	for i := 0; i < 100; i++ {
		count := make(map[tools.Pos2D]int, 0) // TODO can change to make it run faster
		for pos, val := range isBlack {
			if val {
				count[pos] += 0
				for _, addPos := range []tools.Pos2D{{X: -1, Y: 1}, {X: 1, Y: 1}, {X: -2, Y: 0}, {X: 2, Y: 0}, {X: -1, Y: -1}, {X: 1, Y: -1}} {
					count[pos.Add(addPos)]++
				}
			}
		}

		toFlip := make([]tools.Pos2D, 0)
		for pos, count := range count {
			if isBlack[pos] {
				if (count == 0) || (count > 2) {
					toFlip = append(toFlip, pos)
				}
			} else if count == 2 {
				toFlip = append(toFlip, pos)
			}
		}

		for _, pos := range toFlip {
			isBlack[pos] = !isBlack[pos]
		}
	}

	out := 0
	for _, val := range isBlack {
		if val {
			out++
		}
	}

	return strconv.Itoa(out), nil // Return with the answer
}

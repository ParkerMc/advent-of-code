package day09

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	grid := make([][]int, len(lines[0]))
	for x := 0; x < len(lines[0]); x++ {
		grid[x] = make([]int, len(lines))
	}

	for y, line := range lines {
		for x, val := range line {
			grid[x][y] = int(val - 48)
		}
	}

	answer := 0
	for x, col := range grid {
		for y, val := range col {
			if (y-1 < 0 || grid[x][y-1] > val) &&
				(y+1 >= len(grid[0]) || grid[x][y+1] > val) &&
				(x-1 < 0 || grid[x-1][y] > val) &&
				(x+1 >= len(grid) || grid[x+1][y] > val) {
				answer += val + 1
			}
		}
	}

	return strconv.Itoa(answer), nil // Return with the answer
}

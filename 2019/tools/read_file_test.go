package tools

import (
	"testing"
)

const testFilename = "test_read"
const testNoFilename = "test_no_read"

const testReadFileAnswer = "testing\nthe\nreading\nfile\nthingy\n"

var testReadFileLinesAnswer = []string{"testing", "the", "reading", "file", "thingy", ""}
var testReadFileLinesRemoveLastAnswer = []string{"testing", "the", "reading", "file", "thingy"}

func TestReadFile(t *testing.T) {
	text, err := ReadFile(testFilename)
	if err != nil {
		t.Fatal(err)
	}

	if text != testReadFileAnswer {
		t.Fatal("the text read is incorrect")
	}

	_, err = ReadFile(testNoFilename)
	if err == nil {
		t.Fatal("read file that shouldn't exist")
	}
}

func TestReadFileLines(t *testing.T) {
	lines, _ := ReadFileLines(testFilename)

	for i, line := range lines {
		if len(testReadFileLinesAnswer) <= i || line != testReadFileLinesAnswer[i] {
			t.Fatal("the text read is incorrect")
		}
	}
}

func TestReadFileLinesRemoveLast(t *testing.T) {
	lines, _ := ReadFileLinesRemoveLast(testFilename)

	for i, line := range lines {
		if len(testReadFileLinesRemoveLastAnswer) <= i || line != testReadFileLinesRemoveLastAnswer[i] {
			t.Fatal("the text read is incorrect")
		}
	}
}

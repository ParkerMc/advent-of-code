// Package day20 2018
package day20

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "3755"
const testAnswerTwo = "8627"

var testFilename = "../test/private/" + tools.GetPackageName()

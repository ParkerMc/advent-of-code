// Package day05 2019
package day05

import "gitlab.com/parkermc/advent-of-code/2019/tools"

const testAnswerOne = "16434972"
const testAnswerTwo = "16694270"

var testFilename = "../test/private/" + tools.GetPackageName()

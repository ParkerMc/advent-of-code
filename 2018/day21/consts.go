// Package day21 2018
package day21

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "16311888"
const testAnswerTwo = "1413889"

var testFilename = "../test/private/" + tools.GetPackageName()

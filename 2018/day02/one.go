package day02

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// One the first puzzle
func One(filename string) (string, error) {
	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	numWithTwo := 0
	numWithThree := 0
	for _, line := range lines {
		countedRunes := make([]rune, 0) // Runes that are already counted
		lineRunes := []rune(line)       // Runes for the line

		two := false // If the line has two or three of one rune(char)
		three := false
		for i, letterRune := range lineRunes { // Loop through the runes of the line
			alreadyCounted := false // Check if the rune has already been counted
			for _, countedRune := range countedRunes {
				if countedRune == letterRune {
					alreadyCounted = true
					break
				}
			}

			if !alreadyCounted { // If the rune has not already been counted count it
				count := 1
				for j := i + 1; j < len(lineRunes); j++ { // Loop through and add one to count for evey rune that's the same
					if lineRunes[j] == letterRune {
						count++
					}
				}

				if count == 2 { // If there are two of the rune set that the line has two of at lest one rune
					two = true
				} else if count == 3 { // If there are three of the rune set that the line has three of at lest one rune
					three = true
				}
			}
			countedRunes = append(countedRunes, letterRune)
		}

		if two { // If the line had two of at least one rune add to num with two
			numWithTwo++
		}
		if three { // If the line had three of at least one rune add to num with three
			numWithThree++
		}
	}
	return strconv.Itoa(numWithTwo * numWithThree), nil // Get the answer by multiplying and return
}

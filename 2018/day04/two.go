package day04

import (
	"sort"
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	sort.Strings(lines)
	guards := make([]Guard, 0)
	guardNum := -1
	sleepAmt := 0
	shift := make([]bool, 60)
	sleeps := 0
	for _, line := range lines {
		minV, err := strconv.Atoi(strings.Split(strings.Split(strings.Split(line, " ")[1], ":")[1], "]")[0])
		if err != nil {
			return "", err
		}
		action := strings.Split(line, " ")[2]
		if action == "Guard" {
			num, err := strconv.Atoi(strings.Split(strings.Split(line, " ")[3], "#")[1])

			if err != nil {
				return "", err
			}
			if guardNum != -1 {
				index := -1
				for i, lookGuard := range guards {
					if lookGuard.Number == guardNum {
						index = i
					}
				}
				if index > -1 {
					guards[index].ShiftSleep = append(guards[index].ShiftSleep, shift)
					guards[index].SleepAmount += sleepAmt
				} else {
					guards = append(guards, Guard{guardNum, [][]bool{shift}, sleepAmt})
				}

			}
			guardNum = num
			sleepAmt = 0
			shift = make([]bool, 60)
			sleeps = 0
		} else if action == "falls" {
			sleeps = minV
		} else if action == "wakes" {
			for i := 0; i < 60; i++ {
				if i >= sleeps && i < minV {
					shift[i] = true
				}
			}
			sleepAmt += minV - sleeps
		}
	}
	if guardNum != -1 {
		index := -1
		for i, lookGuard := range guards {
			if lookGuard.Number == guardNum {
				index = i
			}
		}
		if index > -1 {
			guards[index].ShiftSleep = append(guards[index].ShiftSleep, shift)
			guards[index].SleepAmount += sleepAmt
		} else {
			guards = append(guards, Guard{guardNum, [][]bool{shift}, sleepAmt})
		}
	}
	mostAsleepGuard := guards[0]
	mostAsleepTime := 0
	mostAsleepTimeAmt := 0
	for _, guard := range guards {
		sleepTimes := make([]int, 60)
		for _, shift := range guard.ShiftSleep {
			for i := 0; i < 60; i++ {
				if shift[i] {
					sleepTimes[i]++
				}
			}
		}
		for i := 0; i < 60; i++ {
			if sleepTimes[i] > mostAsleepTimeAmt {
				mostAsleepGuard = guard
				mostAsleepTime = i
				mostAsleepTimeAmt = sleepTimes[i]
			}
		}
	}
	return strconv.Itoa(mostAsleepGuard.Number * mostAsleepTime), nil // Return with the answer
}

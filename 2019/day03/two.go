package day03

import (
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2019/tools"
)

func placeWireCount(area map[int]map[int]int, line []item) error {
	x := 0
	y := 0
	z := 0
	for _, part := range line {
		for i := 1; i <= part.amount; i++ {
			switch part.dir {
			case "U":
				y++
			case "D":
				y--
			case "R":
				x++
			case "L":
				x--
			}
			if val, ok := area[x]; !ok || val == nil {
				area[x] = make(map[int]int, 0)
			}
			z++
			area[x][y] = z
		}
	}
	return nil
}

// Two the second puzzle for the day
func Two(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	area := make(map[int]map[int]int, 0)
	line1, err := prepLines(lines[0])
	if err != nil {
		return "", err
	}
	line2, err := prepLines(lines[1])
	if err != nil {
		return "", err
	}
	_, err = placeWire(area, line1, 1)
	if err != nil {
		return "", err
	}

	poses, err := placeWire(area, line2, 2)
	if err != nil {
		return "", err
	}

	distMap1 := make(map[int]map[int]int)
	distMap2 := make(map[int]map[int]int)
	placeWireCount(distMap1, line1)
	placeWireCount(distMap2, line2)

	closest := 100000000
	for _, pos := range poses {
		if distMap1[pos.X][pos.Y]+distMap2[pos.X][pos.Y] < closest {
			closest = distMap1[pos.X][pos.Y] + distMap2[pos.X][pos.Y]
		}
	}

	return strconv.Itoa(closest), nil // Will never get here
}

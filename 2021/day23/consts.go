// Package day23 2021
package day23

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "19059"
const testAnswerTwo = "48541"

var testFilename = "../test/private/" + tools.GetPackageName()

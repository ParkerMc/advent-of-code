use std::time::Duration;

use crate::utils;

pub fn run(part: Option<u8>, input: &Vec<String>) -> Duration {
    utils::day::run_day(part, module_path!(), part1, part2, input)
}

#[cfg(test)]
mod tests {
    use super::{part1, part2};
    use crate::utils;

    #[test]
    fn test_part1() {
        utils::unit_test::test_day(module_path!(), "1", part1);
    }
    #[test]
    fn test_part2() {
        utils::unit_test::test_day(module_path!(), "2", part2);
    }
}

fn part1(input: &Vec<String>) -> String {
    input
        .iter()
        .map(|l| {
            let nums: Vec<char> = l.chars().filter(|c| c.is_ascii_digit()).collect();
            if nums.len() == 0 {
                return 0;
            }
            nums[0].to_digit(10).expect("Expected digit") * 10
                + nums[nums.len() - 1].to_digit(10).expect("Expected digit")
        })
        .sum::<u32>()
        .to_string()
}

fn part2(input: &Vec<String>) -> String {
    input
        .iter()
        .map(|l| {
            let chars: Vec<char> = l.chars().collect();
            let first = (0..chars.len())
                .find_map(|i| {
                    if chars[i].is_ascii_digit() {
                        return Some(chars[i].to_digit(10).expect("Should've been digit"));
                    }
                    let subset = &l[i..chars.len()];
                    if subset.starts_with("one") {
                        return Some(1);
                    }
                    if subset.starts_with("two") {
                        return Some(2);
                    }
                    if subset.starts_with("three") {
                        return Some(3);
                    }
                    if subset.starts_with("four") {
                        return Some(4);
                    }
                    if subset.starts_with("five") {
                        return Some(5);
                    }
                    if subset.starts_with("six") {
                        return Some(6);
                    }
                    if subset.starts_with("seven") {
                        return Some(7);
                    }
                    if subset.starts_with("eight") {
                        return Some(8);
                    }
                    if subset.starts_with("nine") {
                        return Some(9);
                    }
                    None
                })
                .expect("First digit not found");
            let second = (0..chars.len())
                .rev()
                .find_map(|i| {
                    if chars[i].is_ascii_digit() {
                        return Some(chars[i].to_digit(10).expect("Should've been digit"));
                    }
                    let subset = &l[0..=i];
                    if subset.ends_with("one") {
                        return Some(1);
                    }
                    if subset.ends_with("two") {
                        return Some(2);
                    }
                    if subset.ends_with("three") {
                        return Some(3);
                    }
                    if subset.ends_with("four") {
                        return Some(4);
                    }
                    if subset.ends_with("five") {
                        return Some(5);
                    }
                    if subset.ends_with("six") {
                        return Some(6);
                    }
                    if subset.ends_with("seven") {
                        return Some(7);
                    }
                    if subset.ends_with("eight") {
                        return Some(8);
                    }
                    if subset.ends_with("nine") {
                        return Some(9);
                    }
                    None
                })
                .expect("Second digit not found");
            first * 10 + second
        })
        .sum::<u32>()
        .to_string()
}

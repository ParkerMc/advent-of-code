// Package day17 2020
package day17

import "gitlab.com/parkermc/advent-of-code/2020/tools"

const testAnswerOne = "304"
const testAnswerTwo = "1868"

var testFilename = "../test/private/" + tools.GetPackageName()

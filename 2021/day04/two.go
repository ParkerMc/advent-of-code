package day04

import (
	"errors"
	"strconv"

	"gitlab.com/parkermc/advent-of-code/2021/tools"
)

// Two the second puzzle for the day
func Two(filename string) (string, error) {
	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}

	calls, cards, err := load(lines)
	if err != nil {
		return "", err
	}

	for i := range calls {
		tmpCards := make([]bingoCard, 0)
		for _, card := range cards {
			if !wins(card, calls[:i+1]) {
				tmpCards = append(tmpCards, card)
			}
		}

		if len(cards) == 1 && len(tmpCards) == 0 {
			uncalledTotal := 0
			for _, row := range cards[0] {
				for _, cell := range row {
					if !wasCalled(calls[:i+1], cell) {
						uncalledTotal += cell
					}
				}
			}

			return strconv.Itoa(uncalledTotal * calls[i]), nil
		}
		cards = tmpCards
	}

	return "", errors.New("never got all but one bingo") // Return with the answer
}

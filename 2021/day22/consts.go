// Package day22 2018
package day22

import "gitlab.com/parkermc/advent-of-code/2021/tools"

const testAnswerOne = "583636"
const testAnswerTwo = "1294137045134837"

var testFilename = "../test/private/" + tools.GetPackageName()

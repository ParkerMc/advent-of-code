// Package day01 2018
package day01

import "gitlab.com/parkermc/advent-of-code/2018/tools"

const testAnswerOne = "500"
const testAnswerTwo = "709"

var testFilename = "../test/private/" + tools.GetPackageName()

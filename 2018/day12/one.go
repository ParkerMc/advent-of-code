package day12

import (
	"strconv"
	"strings"

	"gitlab.com/parkermc/advent-of-code/2018/tools"
)

func genIndex(arr map[int]bool, mid int) string {
	out := ""
	for x := mid - 2; x <= mid+2; x++ {
		if arr[x] {
			out += "#"
		} else {
			out += "."
		}
	}
	return out
}

func getSum(state map[int]bool) int {
	answer := 0
	for i, plant := range state {
		if plant {
			answer += i
		}
	}
	return answer
}

// One the first puzzle for the day
func One(filename string) (string, error) {

	// Read the file
	lines, err := tools.ReadFileLinesRemoveLast(filename)
	if err != nil {
		return "", err
	}
	start := -4
	end := len(strings.Split(lines[0], " ")[2]) + 3
	state := make(map[int]bool)
	state[-4] = false
	state[-3] = false
	state[-2] = false
	state[-1] = false
	for i, char := range []rune(strings.Split(lines[0], " ")[2]) {
		if char == '#' {
			state[i] = true
		} else {
			state[i] = false
		}
	}
	state[end-3] = false
	state[end-2] = false
	state[end-1] = false
	state[end] = false

	rules := make(map[string]bool, 0)
	for _, line := range lines[2:] {
		if strings.Split(line, " ")[2] == "#" {
			rules[strings.Split(line, " ")[0]] = true
		} else {
			rules[strings.Split(line, " ")[0]] = false
		}
	}

	for gen := 1; gen <= 20; gen++ {
		swap := make([]int, 0)
		for x := start; x <= end; x++ {
			if rules[genIndex(state, x)] != state[x] {
				swap = append(swap, x)
			}
		}
		for _, i := range swap {
			state[i] = !state[i]
			if start == i-2 {
				start--
				state[start] = false
				start--
				state[start] = false
			} else if start == i-3 {
				start--
				state[start] = false
			} else if end == i+2 {
				end++
				state[end] = false
				end++
				state[end] = false
			} else if end == i+3 {
				end++
				state[end] = false
			}
		}
	}

	return strconv.Itoa(getSum(state)), nil // Return with the answer
}

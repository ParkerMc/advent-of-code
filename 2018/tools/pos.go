package tools

// Pos2D a 2D pos
type Pos2D struct {
	X int
	Y int
}

// Pos3D a 4D pos
type Pos3D struct {
	X int
	Y int
	Z int
}

// Pos4D a 4D pos
type Pos4D struct {
	W int
	X int
	Y int
	Z int
}

// Add adds two points
func (pos *Pos2D) Add(pos2 Pos2D) Pos2D {
	return Pos2D{X: pos.X + pos2.X, Y: pos.Y + pos2.Y}
}

// Add adds two points
func (pos *Pos3D) Add(pos2 Pos3D) Pos3D {
	return Pos3D{X: pos.X + pos2.X, Y: pos.Y + pos2.Y, Z: pos.Z + pos2.Z}
}

// Subtract subtracts two points
func (pos *Pos3D) Subtract(pos2 Pos3D) Pos3D {
	return Pos3D{X: pos.X - pos2.X, Y: pos.Y - pos2.Y, Z: pos.Z - pos2.Z}
}

// Add adds two points
func (pos *Pos4D) Add(pos2 Pos4D) Pos4D {
	return Pos4D{W: pos.W + pos2.W, X: pos.X + pos2.X, Y: pos.Y + pos2.Y, Z: pos.Z + pos2.Z}
}

// MDist calculates the manhattan distance between two points
func (pos *Pos2D) MDist(pos2 *Pos2D) int {
	return Abs(pos.X-pos2.X) + Abs(pos.Y-pos2.Y)
}

// MDist calculates the manhattan distance between two points
func (pos *Pos3D) MDist(pos2 *Pos3D) int {
	return Abs(pos.X-pos2.X) + Abs(pos.Y-pos2.Y) + Abs(pos.Z-pos2.Z)
}

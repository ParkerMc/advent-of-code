package tools

import (
	"runtime"
	"strings"
)

// GetPackageName Gets the package name for getting test file name
func GetPackageName() string {
	pc, _, _, _ := runtime.Caller(1)
	split := strings.Split(runtime.FuncForPC(pc).Name(), "/")
	return strings.Split(split[len(split)-1], ".")[0]
}
